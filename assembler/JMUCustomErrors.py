#  from JMUArchitecture import Arch


class CustomException(Exception):
    def __init__(self, line, level):
        self.line = line
        self.level = level


class Error(CustomException):
    def __init__(self, line):
        super(Error, self).__init__(line, level="ERROR: ")


class Advisory(CustomException):
    def __init__(self, line):
        super(Advisory, self).__init__(line, level="ADVISORY: ")


class GeneralSyntaxError(Error):

    def __init__(self, line):
        super(GeneralSyntaxError, self).__init__(line)

    def __str__(self):
        return self.level + "syntax error " + \
               " on line " + str(self.line)


class SymbolAlreadyDefinedError(Error):

    def __init__(self, line, name):
        super(SymbolAlreadyDefinedError, self).__init__(line)
        self.symbol_name = name

    def __str__(self):
        return self.level + "symbol " + self.symbol_name + \
               " is previously defined on line " + str(self.line)


class ImpossibleShift(Error):

    def __init__(self, line, value):
        super(ImpossibleShift, self).__init__(line)
        self.value = value

    def __str__(self):
        msg = self.level + "shift of " + str(self.value) + \
               " is out of range on line " + str(self.line)
        return msg


class SymbolNotDefinedError(Error):

    def __init__(self, line, symbol_name):
        super(SymbolNotDefinedError, self).__init__(line)
        self.symbol_name = symbol_name

    def __str__(self):
        return self.level + "symbol " + self.symbol_name + \
               " mentioned on line " + str(self.line) + " is not defined."


class RelativeJumpTooFar(Error):

    def __init__(self, line, offset):
        super(RelativeJumpTooFar, self).__init__(line)
        self.offset = offset

    def __str__(self):
        return self.level + 'line ' + str(self.line) + \
               ', relative offset of ' + str(self.offset) + ' is too large.'


class IntegerOutOfRange(Error):

    def __init__(self, line, value):
        super(IntegerOutOfRange, self).__init__(line)
        self.value = value

    def __str__(self):
        return self.level + 'line ' + str(self.line) + \
               ', integer value of ' + str(self.value) + ' is out of range.'


class TinyValueOutOfRange(Error):

    def __init__(self, line, offset):
        super(TinyValueOutOfRange, self).__init__(line)
        self.offset = offset

    def __str__(self):
        return self.level + 'line ' + str(self.line) + \
               ', unsigned value of ' + str(self.offset) + \
               ' is too large or negative.'


class NotYetImpplementedError(Error):

    def __init__(self, line):
        super(NotYetImpplementedError, self).__init__(line)

    def __str__(self):
        return self.level + 'line ' + str(self.line) + \
               ', instruction not yet implemented.'


class EmptyInstructionError(Error):
    def __init__(self, line):
        super(EmptyInstructionError, self).__init__(line)

    def __str__(self):
        return self.level + 'line ' + str(self.line) + \
               ', instruction is not encoded.'


class AdvisoryUseImmediate(Advisory):

    def __init__(self, line, t):
        super(AdvisoryUseImmediate, self).__init__(line)
        self.t = t

    def __str__(self):
        return self.level + 'line ' + str(self.line) + \
               ', unsigned byte value of ' + str(self.t) + \
               ' is small enough for an immediate instruction.'


class AdvisoryFarCall(Advisory):

    def __init__(self, line, offset):
        super(AdvisoryFarCall, self).__init__(line)
        self.offset = offset

    def __str__(self):
        return self.level + 'line ' + str(self.line) + \
               ', using far call for offset of ' + str(self.offset) + \
               ' because too far or forward.'


class AdvisoryForwardCall(Advisory):

    def __init__(self, line):
        super(AdvisoryForwardCall, self).__init__(line)

    def __str__(self):
        return self.level + 'line ' + str(self.line) + \
               ', using far call because it\'s a forward call.'


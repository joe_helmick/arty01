// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
// alu_muldiv.sv

// Unsigned division algorithm from "Divider Background.pdf" in /reference:
// from http://bwrcs.eecs.berkeley.edu/Classes/icdesign/ee141_s04/Project/Divider%20Background.pdf

// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
`timescale 1ns/1ps
`include "arch_defines.vh"

module alu_muldiv (
	input wire clock,
	input wire [17:0] a,
	input wire [17:0] b,
	input wire muldiv, // 1 for mul, 0 for div
	input wire start,
	output wire [17:0] result,
	output wire [17:0] remainder,
	output wire divdone
);

// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
// These are all for using wires in the output instead of registers.
reg [17:0] resultR;
reg [17:0] remainderR;
reg divdoneR;
assign result = resultR;
assign remainder = remainderR;
assign divdone = divdoneR;
// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪

reg [4:0] iteration;
reg [35:0] div_remainder; // double-width per algorithm
reg [17:0] divisor;
reg [17:0] quotient;
localparam  STEP1 = 0, STEP2 = 1, STEP3 = 2;
reg [1:0] stepR;

always @ (posedge clock)
begin
	if (start)
	begin
		if (muldiv == 1) // multiply
		begin
			resultR <= a * b;
			remainderR <= 0;
		end
		else // divide
		begin
			iteration <= 0;
			divdoneR <= 0;
			quotient <= 0;
			divisor <= b;
			div_remainder <= a;
			stepR <= STEP1;
		end
	end
	else
	// not starting, but divider actually running
	if (muldiv == 0)
	begin 
		if (iteration < 18)
		begin
			case (stepR)
				STEP1: do_step1();
				STEP2: do_step2();
				STEP3: do_step3();
				default : ;
			endcase
		end
		else
		begin
			resultR <= quotient;
			remainderR <= div_remainder[35:18];
			divdoneR <= 1;
		end		
	end
end

// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
task do_step1();
begin
	div_remainder <= div_remainder << 1;
	stepR <= STEP2;
end endtask

// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
task do_step2();
begin
	div_remainder[35:18] <= div_remainder[35:18] - divisor;
	stepR <= STEP3;
end endtask

// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
task do_step3();
begin
	if (div_remainder[35] == 1) // negative
		begin
		div_remainder[35:18] <= div_remainder[35:18] + divisor;
		quotient <= quotient << 1;
		end
	else
		begin
		quotient <= quotient << 1;
		quotient[0] <= 1'b1;
		end

	stepR <= STEP1;
	iteration <= iteration + 1;
end endtask;
// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪

	



/* ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
BELOW IS THE ORIGINAL COMBINATIONAL ALGORITHM, WITH 
STEPS CLEARLY DOCUMENTED
 ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪*/


// module alu_muldiv (
// 	input wire [17:0] a,
// 	input wire [17:0] b,
// 	input wire muldiv, // 1 for mul, 0 for div
// 	input wire start,
// 	output reg [17:0] result,
// 	output reg [17:0] remainder,
// 	output reg divdone
// );

// integer iteration;
// reg [35:0] div_remainder; // double-width per algorithm
// reg [17:0] divisor;
// reg [17:0] quotient;


// always @ (posedge start)
// begin
// //	if (start) begin
// 		if (muldiv) // 1 means multiply, 0 means divide
// 		begin
// 			result = a * b;  // regular inferred combinational multiply, let run over several clocks, check timing
// 			remainder = 0;
// 		end
// 		else // divide
// 		begin
// 			divdone = 0;

// 			// Unsigned division algorithm from "Divider Background.pdf" in /reference:
// 			// from http://bwrcs.eecs.berkeley.edu/Classes/icdesign/ee141_s04/Project/Divider%20Background.pdf

// 			// Load the number being divider (dividend) into the double-width remainder register.
// 			div_remainder = a;
// 			// Load the divisor from b operand
// 			divisor = b;
// 			// Initialize the quotient register.
// 			quotient = 0;
// 			// Do one iteration per output bit.
// 			for (iteration = 1; iteration < 19; iteration = iteration + 1)
// 			begin
// 				// Step 1. "Shift the remainder register left 1 bit."
// 				div_remainder = div_remainder << 1;

// 				// Step 2. "Subtract the divisor register from the LEFT HALF of the remainder
// 				// register, and place the result into the LEFT HALF of the remainder register.
// 				div_remainder[35:18] = div_remainder[35:18] - divisor;

// 				// Step 3. "Test the remainder.
// 				//   If negative (msg == 1), then
// 				// 		3.b Restore the original value by adding the divisor register to the
// 				//			LEFT HALF of the remainder register, and place the sum from this
// 				//			addition into the LEFT HALF of the remainder register.  Also, 
// 				//			shift the quotient register to the left, setting its new least
// 				//			significant bit to zero.
// 				//  Otherwise (remainder is >= 0), then
// 				//		3.a Shift the quotient register to the left and set the new
// 				//			least significant (rightmost) bit to one.
// 				if (div_remainder[35] == 1) // negative
// 					begin
// 					div_remainder[35:18] = div_remainder[35:18] + divisor;
// 					quotient = quotient << 1;
// 					end
// 				else
// 					begin
// 					quotient = quotient << 1;
// 					quotient[0] = 1'b1;
// 					end
// 			end
// 			// After all iterations complete, assign the final values for the quotient
// 			// and remainder back to the output registers of the module.
// 			result = quotient;
// 			remainder = div_remainder[35:18];
// 			divdone = 1;
// 		end
// //	end
// //	else
// //		;
// end

endmodule

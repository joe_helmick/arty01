from enum import IntEnum


class SymbolType(IntEnum):
    equate = 1
    pclabel = 2
    alias = 3
    datastruct = 4
    variable = 5
    sizeof = 6


class Symbol:
    name = None
    value = None
    line_defined_on = None
    registernum = None

    def __init__(self, name: str, value: int, line_defined_on: int, symtype: SymbolType):
        self.name = name
        self.value = value
        self.line_defined_on = line_defined_on
        self.symtype = symtype

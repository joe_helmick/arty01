import tkinter as tk
from tkSourceLine import *
from tkKonstants import tkkuistandards as tkk
from tkMemoryWindow import MemoryWindow

from vm import VirtualMachine
from Decoder import Decoder, InstructionDecode


class ui_main_window:
    tkk = None
    root = None
    cnvlst = None
    cnvreg = None
    curline = None

    LST_WID = 1200
    LST_HEI = 1034
    REG_WID = 278

    BTN_WID = 110
    BTN_HEI = 28
    
    photoRun = None
    photoStop = None
    photoStep = None
    photoRunToBp = None
    photoMemory = None
    photoGotoCur = None
    
    PADDING = 6
    LST_VIZ_TOP = 1

    # These get computed once, later, in __init__
    LST_VIZ_LINES = 0
    LST_VIZ_BOT = 0
    SCROLL_LINES = 0

    srclines = []
    src_len = None
    pc = 0
    vm = None
    win_ram = None
    win_stack = None
    isRunning = False
    line_dict = {}
    isBrowsing = True
    decodes = {}  # PC is key, decoded stuff is stored in tuple


    def compute_decodes(self):
        d = Decoder()

        for line in self.srclines:
            if line.word_count == 0:
                continue
            # If we get here we know there's an iw1, so decode it.
            d.decodeIw1(line.iw1)
            a = d.a
            b = d.b
            s = d.s
            t = d.t
            flagid = d.flagid
            flagsc = d.flagsc
            opt5 = d.opt5
            opt10 = d.opt10
            mode = d.mode
            opcode = d.opcode

            c = None
            if line.word_count == 2:
                d.decodeIw2(line.iw2)
                c = d.c

            instr = InstructionDecode(line.iw1, line.iw2, a, b, c, s, t,
                                      flagid, flagsc,
                                      opt5, opt10,
                                      mode, opcode)
            self.decodes[line.pc] = instr
        return


    def map_lines(self):
        for line in self.srclines:
            # if line.iw1 is None:
            if line.word_count == 0:
                continue
            self.line_dict[line.pc] = line.number
        return


    def __init__(self, root: tk, lines: list, memory: list):
        self.tkk = tkk()
        self.vm = VirtualMachine(memory.copy())

        # Assign parameters to instance variables.
        self.root = root
        self.srclines = lines

        # Map PC value to a line number.
        self.map_lines()

        # Decode every instruction and store them in decodes,
        # key is PC and value is an InstructionDecode object.
        self.compute_decodes()

        # Save this value, as it never changes and we use it a lot.
        self.src_len = len(self.srclines)

        # Layout the top bar of buttons.
        xpos = self.PADDING
        wid = self.BTN_WID
        hei = self.BTN_HEI
        ypos = hei + self.PADDING
        btnRun = tk.Button(root, text='  Run')
        self.photoRun = tk.PhotoImage(file=r'D:\dev-attic\microprocessors\Arty01\assembler\media_play_green.png')
        btnRun.config(image=self.photoRun, compound=tk.LEFT)
        btnRun.place(x=xpos, y=self.PADDING, width=wid, height=hei)
        btnRun.bind('<Button-1>', self.vm_run)

        xpos = xpos + wid + self.PADDING
        btnStop = tk.Button(root, text='  Stop')
        self.photoStop = tk.PhotoImage(file=r'D:\dev-attic\microprocessors\Arty01\assembler\media_stop_red.png')
        btnStop.config(image=self.photoStop, compound=tk.LEFT)
        btnStop.place(x=xpos, y=self.PADDING, width=wid, height=hei)
        btnStop.bind('<Button-1>', self.vm_stop)

        xpos = xpos + wid + self.PADDING
        btnStep = tk.Button(root, text='  Step')
        self.photoStep = tk.PhotoImage(file=r'D:\dev-attic\microprocessors\Arty01\assembler\media_step_forward.png')
        btnStep.config(image=self.photoStep, compound=tk.LEFT)
        btnStep.place(x=xpos, y=self.PADDING, width=wid, height=hei)
        btnStep.bind("<Button-1>", self.vm_step)

        xpos = xpos + wid + self.PADDING
        btnRunToBP = tk.Button(root, text='  Run To BP')
        self.photoRunToBp = tk.PhotoImage(file=r'D:\dev-attic\microprocessors\Arty01\assembler\media_fast_forward.png')
        btnRunToBP.config(image=self.photoRunToBp, compound=tk.LEFT)
        btnRunToBP.place(x=xpos, y=self.PADDING, width=wid, height=hei)

        xpos = xpos + wid + self.PADDING
        btnGotoCur = tk.Button(root, text='  Goto CL')
        self.photoGotoCur = tk.PhotoImage(file=r'D:\dev-attic\microprocessors\Arty01\assembler\arrow_right_green.png')
        btnGotoCur.config(image=self.photoGotoCur, compound=tk.LEFT)
        btnGotoCur.place(x=xpos, y=self.PADDING, width=wid, height=hei)
        btnGotoCur.bind('<Button-1>', self.callback_goto_current_line)

        xpos = xpos + wid + self.PADDING
        btnMemoryStart = tk.Button(root, text='  RAM')
        self.photoMemory = tk.PhotoImage(file=r'D:\dev-attic\microprocessors\Arty01\assembler\text_binary.png')
        btnMemoryStart.config(image=self.photoMemory, compound=tk.LEFT)
        btnMemoryStart.place(x=xpos, y=self.PADDING, width=wid, height=hei)
        btnMemoryStart.bind('<Button-1>', self.callback_launch_memory_bottom)

        xpos = xpos + wid + self.PADDING
        btnMemoryStack = tk.Button(root, text='  Stack')
        btnMemoryStack.config(image=self.photoMemory, compound=tk.LEFT)
        btnMemoryStack.place(x=xpos, y=self.PADDING, width=wid, height=hei)
        btnMemoryStack.bind('<Button-1>', self.callback_launch_memory_stack)

        # Create the listing canvas.
        ypos = ypos + self.PADDING
        xpos = self.PADDING
        wid = self.LST_WID
        hei = self.LST_HEI

        # These next three "constants" are related to font size.  The UI is adaptable to
        # different font sizes, within reason.  Too large and horizontal spacing can't handle it.
        # 16 point seems about the limit.  TODO: COULD FIX THIS WITH TOTAL DYNAMIC X POSITIONING IN ALL PAINT ROUTINES
        self.LST_VIZ_LINES = self.LST_HEI // tkk.HEI_LINE
        self.LST_VIZ_BOT = self.LST_VIZ_TOP + self.LST_VIZ_LINES - 1
        self.SCROLL_LINES = self.LST_VIZ_LINES // 3

        self.cnvlst = tk.Canvas(root, bg=tkk.COLOR_BG_DEFAULT, bd=1, highlightthickness=1)
        self.cnvlst.place(x=xpos, y=ypos, width=wid, height=hei)
        self.cnvlst.bind('<ButtonRelease-1>', self.callback_listing_click)
        self.cnvlst.bind('<MouseWheel>', self.callback_listing_wheel)
        self.cnvlst.bind('<Up>', self.callback_listing_keypress)
        self.cnvlst.bind('<Down>', self.callback_listing_keypress)
        self.cnvlst.bind('<Prior>', self.callback_listing_keypress)
        self.cnvlst.bind('<Next>', self.callback_listing_keypress)
        self.cnvlst.bind('<Enter>', self.canvas_focus)
        self.cnvlst.bind('<ButtonRelease-3>', self.callback_listing_rclick)

        # Create the register/status canvas
        xpos = xpos + self.PADDING + self.LST_WID
        wid = self.REG_WID
        hei = self.LST_HEI
        self.cnvreg = tk.Canvas(root, bg=tkk.COLOR_BG_DEFAULT, bd=1, highlightthickness=1)
        self.cnvreg.place(x=xpos, y=ypos, width=wid, height=hei)

        # Find starting current line, where PC = 0 and start painting.
        self.curline = self.line_dict[0]
        self.update_listing()
        return


    def vm_stop(self, event):
        self.vm.halt = True
        self.isBrowsing = True
        self.curline = self.line_dict[self.vm.PC]
        self.callback_goto_current_line(None)
        self.update_listing()
        if self.win_ram is not None:
            self.win_ram.paint()
        self.isBrowsing = True
        return


    def vm_step(self, event=None):
        self.vm.halt = False
        self.isBrowsing = False
        # self.vm.run_next_instruction()
        self.vm.run_instruction(self.vm.PC, self.decodes[self.vm.PC])
        self.update_listing()
        # print('vm.instructionCount', self.vm.instructionCount)
        if self.win_ram is not None:
            self.win_ram.paint()
        self.isBrowsing = True


    def vm_run(self, event=None):

        # How often to check tk event loop.
        # On dev machine, this method runs > 100,000 times per second, so a large value here
        # keeps updates down and the speed up.
        TK_CHECK_INTERVAL = 5000

        self.vm.halt = False
        self.isBrowsing = False
        running = True

        while running:

            # Check tkinter event loop every so often
            if (self.vm.instructionCount % TK_CHECK_INTERVAL) == 0:
                try:
                    self.root.update()
                except tk.TclError:
                    # This simply allows clean shutdown of the main window.  By the time we get here,
                    # main window can be destroyed, so just handle any tcl error, stop the virtual
                    # machine, and return before trying to do any screen/tcl updates.
                    self.vm.halt = True
                    return

#            self.vm.run_instruction(self.vm.PC, self.decodes[self.vm.PC])
            self.vm.run_instruction(self.decodes[self.vm.PC])

            self.curline = self.line_dict[self.vm.PC]

            # Handle screen update marker.
            if self.srclines[self.curline].upd == BreakPoint.SET:
                print('upd vm.instructionCount', self.vm.instructionCount)
                self.isBrowsing = False
                self.update_listing()
                # self.root.update()
                self.isBrowsing = True

            # Handle breakpoint.
            if self.srclines[self.curline].bp == BreakPoint.SET:
                print('bp vm.instructionCount', self.vm.instructionCount)
                self.isBrowsing = False
                self.update_listing(pc_bp=self.srclines[self.curline].bp)
                # self.root.update()
                running = False


        # self.update_listing()
        return


    def canvas_focus(self, event=None):
        self.cnvlst.focus_set()
        return


    def drawbp(self, ypos: int, kind: BreakPoint):
        FACTOR = 0.3
        x1 = 5
        x2 = x1 + tkk.HEI_LINE - (FACTOR * tkk.HEI_LINE)
        y1 = ypos + (FACTOR * tkk.HEI_LINE)
        y2 = y1 + tkk.HEI_LINE - (FACTOR * tkk.HEI_LINE)
        if kind == BreakPoint.SET:
            self.cnvlst.create_oval(x1, y1, x2, y2, fill=tkk.COLOR_BP_MARKER, outline=tkk.COLOR_BP_MARKER, width=2.0)
        if kind == BreakPoint.DISABLED:
            self.cnvlst.create_oval(x1, y1, x2, y2, fill=None, outline=tkk.COLOR_BP_MARKER, width=2.0)
        if kind == BreakPoint.CLEAR:
            self.cnvlst.create_oval(x1, y1, x2, y2, fill=tkk.COLOR_BG_DEFAULT, outline=tkk.COLOR_BG_DEFAULT, width=2.0)
        return


    def drawupd(self, ypos: int, kind: BreakPoint):
        FACTOR = 0.3
        x1 = 32
        x2 = x1 + tkk.HEI_LINE - (FACTOR * tkk.HEI_LINE)
        y1 = ypos + (FACTOR * tkk.HEI_LINE)
        y2 = y1 + tkk.HEI_LINE - (FACTOR * tkk.HEI_LINE)
        if kind == BreakPoint.SET:
            self.cnvlst.create_rectangle(x1, y1, x2, y2, fill=tkk.COLOR_UPD_MARKER, outline=tkk.COLOR_UPD_MARKER, width=2.0)
        if kind == BreakPoint.DISABLED:
            self.cnvlst.create_rectangle(x1, y1, x2, y2, fill=None, outline=tkk.COLOR_UPD_MARKER, width=2.0)
        if kind == BreakPoint.CLEAR:
            self.cnvlst.create_rectangle(x1, y1, x2, y2, fill=tkk.COLOR_BG_DEFAULT, outline=tkk.COLOR_BG_DEFAULT, width=2.0)
        return


    def callback_listing_click(self, event):
        # Left-click toggles breakpoint for lines with an instruction.
        LEFT_LIMIT = 330
        if event.x > LEFT_LIMIT:
            return
        line_clicked = (event.y // tkk.HEI_LINE) + 1
        self.curline = line_clicked + self.LST_VIZ_TOP - 1
        # click on left implies breakpoint toggle
        if self.srclines[self.curline].pc is not None and self.srclines[self.curline].iw1 is not None:
            self.srclines[self.curline].bp_cycle()
            # print('bp src=', self.srclines[self.curline].text)
            self.update_listing()
        return


    def callback_listing_rclick(self, event):
        # Right-click toggles screen updating for lines with an instruction.
        LEFT_LIMIT = 330
        if event.x > LEFT_LIMIT:
            return
        line_clicked = (event.y // tkk.HEI_LINE) + 1
        self.curline = line_clicked + self.LST_VIZ_TOP - 1
        # click on left implies breakpoint toggle
        if self.srclines[self.curline].pc is not None and self.srclines[self.curline].iw1 is not None:
            self.srclines[self.curline].upd_cycle()
            # print('upd src=', self.srclines[self.curline].text)
            self.update_listing()
        return


    def callback_listing_wheel(self, event):
        MAGIC_WHEEL_DELTA = 120
        # wheel up
        if event.delta == MAGIC_WHEEL_DELTA:
            self.LST_VIZ_TOP -= self.SCROLL_LINES
            self.LST_VIZ_BOT -= self.SCROLL_LINES
        # wheel down
        if event.delta == -MAGIC_WHEEL_DELTA:
            self.LST_VIZ_TOP += self.SCROLL_LINES
            self.LST_VIZ_BOT += self.SCROLL_LINES
        self.isBrowsing = True
        self.update_listing(pc_bp=self.srclines[self.curline].bp)
        return


    def callback_listing_keypress(self, event):
        if event.keysym == 'Up':
            self.LST_VIZ_TOP -= 1
            self.LST_VIZ_BOT -= 1
        if event.keysym == 'Down':
            self.LST_VIZ_TOP += 1
            self.LST_VIZ_BOT += 1
        if event.keysym == 'Prior':
            self.LST_VIZ_TOP -= self.LST_VIZ_LINES
            self.LST_VIZ_BOT -= self.LST_VIZ_LINES
        if event.keysym == 'Next':
            self.LST_VIZ_TOP += self.LST_VIZ_LINES
            self.LST_VIZ_BOT += self.LST_VIZ_LINES
        self.isBrowsing = True
        self.update_listing(pc_bp=self.srclines[self.curline].bp)
        return

    # noinspection PyUnusedLocal
    def callback_goto_current_line(self, event):
        MARGIN = 5
        # Do nothing if already visible.
        if self.LST_VIZ_TOP <= self.curline <= self.LST_VIZ_BOT:
            return
        # Set visible window and repaint with current line near the top.
        self.LST_VIZ_TOP = self.curline - MARGIN
        self.LST_VIZ_BOT = self.LST_VIZ_TOP + self.LST_VIZ_LINES
        self.update_listing(pc_bp=self.srclines[self.curline].bp)
        return

    # noinspection PyUnusedLocal
    def callback_launch_memory_bottom(self, event):
        if self.win_ram is None:
            self.win_ram = MemoryWindow(root=self, title="RAM", addr=0, rows=16, cols=16, vm=self.vm, mode=tkk.FROM_ZERO)
        else:
            self.win_ram.focus_set()
            self.win_ram.deiconify()
        return

    # noinspection PyUnusedLocal
    def callback_launch_memory_stack(self, event):
        if self.win_stack is None:
            self.win_stack = MemoryWindow(root=self, title="STACK", addr=0, rows=16, cols=4, vm=self.vm, mode=tkk.FROM_RAMTOP)
        else:
            self.win_stack.focus_set()
            self.win_stack.deiconify()
        return


    def update_listing(self, pc_bp=None):
        if not self.isBrowsing:
            self.curline = self.line_dict[self.vm.PC]
            self.callback_goto_current_line(None)
        self.constrain_listing()
        self.paint_listing(pc_bp=pc_bp)
        self.paint_registers()
        if self.win_ram is not None:
            self.win_ram.paint()
        self.root.update()
        return


    def drawbox(self, x1, y1, color):
        x1 = x1 - 4
        x2 = x1 + 62
        y2 = y1 + self.tkk.HEI_LINE
        self.cnvlst.create_line(x1, y1,
                                x2, y1,
                                x2, y2,
                                x1, y2,
                                x1, y1,
                                fill=color, width=3, joinstyle=tk.ROUND)
        return


    def paint_listing(self, pc_bp=None):
        # Delete everything from canvas to avoid memory leaks.
        self.cnvlst.delete("all")

        # Initialize constants.
        count = 0
        X_LINE = 24 + 24
        X_PC = 90 + 24
        X_IW = 164 + 24
        X_SRC = 310 + 24

        # Redraw the appropriate lines
        for n in range(self.LST_VIZ_TOP, self.LST_VIZ_BOT + 1):

            # Get this line for reference
            line = self.srclines[n]

            # Determine y position of drawing for this iteration.
            ypos = count * tkk.HEI_LINE

            # Extract pieces of listing line.
            line_number = line.text[0:6]
            pcstr = line.text[6:11]
            iwstr = line.text[12:24]

            # Handle "empty" source code lines and get the right part of non-empty lines.
            if len(line.text.rstrip()) > 25:
                src = line.text[-(len(line.text) - 26):]  # -24 to get the vertical bar, 26 for raw start of line
            else:
                src = ''

            # Paint line number, pc, instruction words
            self.cnvlst.create_text(X_LINE, ypos, text=line_number, font=self.tkk.font1,
                                    anchor=tk.NW, fill=tkk.COLOR_LINENO)
            self.cnvlst.create_text(X_PC, ypos, text=pcstr, font=self.tkk.font1,
                                    anchor=tk.NW, fill=tkk.COLOR_PC)
            self.cnvlst.create_text(X_IW, ypos, text=iwstr, font=self.tkk.font1,
                                    anchor=tk.NW, fill=tkk.COLOR_IW)

            # Paint background and determine source color.
            if self.curline == line.number:

                if line.bp == BreakPoint.SET:
                    self.cnvlst.create_rectangle(X_SRC, ypos, self.LST_WID, ypos + tkk.HEI_LINE,
                                                 fill=tkk.COLOR_BP_BG, outline=tkk.COLOR_BP_BG)
                    src_color = tkk.COLOR_TXT_CURRENT
                else:
                    self.cnvlst.create_rectangle(X_SRC, ypos, self.LST_WID, ypos + tkk.HEI_LINE,
                                                 fill=tkk.COLOR_BG_CURLINE, outline=tkk.COLOR_BG_CURLINE)
                    src_color = tkk.COLOR_TXT_CURRENT
            else:
                self.cnvlst.create_rectangle(X_SRC, ypos, self.LST_WID, ypos + tkk.HEI_LINE,
                                             fill=tkk.COLOR_BG_DEFAULT, outline=tkk.COLOR_BG_DEFAULT)
                src_color = tkk.COLOR_TXT_DEFAULT

            # Paint the source line
            self.cnvlst.create_text(X_SRC, ypos, text=src, font=self.tkk.font1, anchor=tk.NW, fill=src_color)

            # Now draw breakpoint markers if set or disabled.
            if line.bp == BreakPoint.SET:
                self.drawbp(ypos, BreakPoint.SET)
            if line.bp == BreakPoint.DISABLED:
                self.drawbp(ypos, BreakPoint.DISABLED)

            # Now draw GUI update indicator markers if set or disabled.
            if line.upd == BreakPoint.SET:
                self.drawupd(ypos, BreakPoint.SET)
            if line.upd == BreakPoint.DISABLED:
                self.drawupd(ypos, BreakPoint.DISABLED)


            # Finally, draw indicator for breakpoint current program counter.
            if line.bp == BreakPoint.SET and pc_bp == line.bp:
                self.drawbox(X_PC, ypos, tkk.COLOR_TXT_CHANGED)

            count += 1
        return


    @staticmethod
    def changedColor(cur, prev):
        if cur == prev:
            return tkk.COLOR_TXT_DEFAULT
        return tkk.COLOR_TXT_CHANGED


    def paint_registers(self):
        self.cnvreg.delete('all')

        # X position same for everything on this canvas
        XPOS = 5

        ypos = 0
        for n in range(0, 32):
            regval = self.vm.get_gpr(n)
            changed = self.vm.get_gpr_prev(n)
            label = 'r{0:02} '.format(n)
            hexstr = '${0:05x} '.format(regval)
            decstr = '{0:6}  '.format(regval)
            asciivalue = '\u2219'
            if 32 <= regval <= 127:
                asciivalue = chr(regval)
            text = label + hexstr + decstr + asciivalue
            # xpos = 5
            if regval != changed:
                color = tkk.COLOR_TXT_CHANGED
            else:
                color = tkk.COLOR_TXT_DEFAULT
            self.cnvreg.create_text(XPOS, ypos, text=text, font=self.tkk.font1, anchor=tk.NW, fill=color)
            ypos += tkk.HEI_LINE


        # PC always changes.
        ypos += tkk.HEI_LINE
        text = 'PC ${0:05X}  '.format(self.vm.PC)
        self.cnvreg.create_text(XPOS, ypos, text=text, font=self.tkk.font1, anchor=tk.NW, fill=tkk.COLOR_TXT_CHANGED)

        # SP might change
        ypos += tkk.HEI_LINE
        color = self.changedColor(self.vm.SP, self.vm.SP_prev)
        text = 'SP ${0:05X}  '.format(self.vm.SP)
        self.cnvreg.create_text(XPOS, ypos, text=text, font=self.tkk.font1, anchor=tk.NW, fill=color)

        # Z
        ypos += tkk.HEI_LINE
        color = self.changedColor(self.vm.flagZ, self.vm.flagZ_prev)
        text = ' Z {0}  '.format(self.vm.flagZ)
        self.cnvreg.create_text(XPOS, ypos, text=text, font=self.tkk.font1, anchor=tk.NW, fill=color)

        # S
        ypos += tkk.HEI_LINE
        color = self.changedColor(self.vm.flagS, self.vm.flagS_prev)
        text = ' S {0}  '.format(self.vm.flagS)
        self.cnvreg.create_text(XPOS, ypos, text=text, font=self.tkk.font1, anchor=tk.NW, fill=color)
        return


    def constrain_listing(self):

        if self.curline < 1:
            self.curline = 1

        if self.curline >= self.src_len:
            self.curline = self.src_len - 1

        if self.LST_VIZ_TOP < 1:
            self.LST_VIZ_TOP = 1
            self.LST_VIZ_BOT = self.LST_VIZ_LINES
            if self.LST_VIZ_BOT >= self.src_len:
                self.LST_VIZ_BOT = self.src_len - 1
            return

        if self.LST_VIZ_BOT >= self.src_len:
            self.LST_VIZ_BOT = self.src_len - 1
            self.LST_VIZ_TOP = self.LST_VIZ_BOT - self.LST_VIZ_LINES + 1
            if self.LST_VIZ_TOP < 1:
                self.LST_VIZ_TOP = 1
            return
        return

import antlr4
from antlr4 import *
from JMULexer import JMULexer
from JMUArchitecture import Arch
from JMUInstruction import Instruction
from JMUCustomErrors import EmptyInstructionError
from bitstring import BitArray
from Decoder import Decoder
from Decoder import InstructionMode


def fmtseg2(ops: str, src: str):
    return ops.ljust(24, ' ') + ' ┊ ' + src.rstrip()


global_line_number = 1


class AsmLine:
    global global_line_number
    tokens = []

    def words_empty(self):
        # If there are no words, then obviously empty.
        if len(self.words) == 0:
            return True
        # Return true if any word is empty, otherwise
        # line is filled, so return false.
        return None in self.words

    def __init__(self, tokens: list):
        self.tokens = tokens
        self.program_counter = None
        self.words = []
        self.source_code = ''
        self.line_number = 0
        self.global_line_number = global_line_number
        self.instruction = Instruction()

        # Concatenate the tokens into a new source code line.
        for t in self.tokens:
            self.source_code += t.text

        # Handle the case where (for include files) the new line
        # need a CRLF added to the end.
        if not self.source_code.endswith('\n'):
            self.source_code += '\n'

        # Store the value of the line number on which the token
        # originally appears (in its own source file).
        if len(tokens) > 0:
            self.line_number = tokens[0].line

        # Store the "global" line number, taking into account
        # include files which may have altered the original
        # line number.
        self.global_line_number = global_line_number

        return

    def get_tokens(self):
        return self.tokens

    def __str__(self):
        fillchar = ' '
        emptychar = '\u2588'
        str_line_number = str(self.global_line_number).rjust(5) + ' '

        if self.program_counter is None:
            str_program_counter = (fillchar * 6)
        else:
            str_program_counter = '{:05x} '.format(self.program_counter)

        str_words = ''
        if not self.words_empty():

            if len(self.words) == 0:
                str_words = (fillchar * 11)

            if len(self.words) == 1:
                str_words = '{:05x}'.format(self.words[0]) + (fillchar * 6)

            if len(self.words) == 2:
                str_words = '{:05x} '.format(self.words[0]) + '{:05x}'.format(self.words[1])

        else:
            if len(self.words) != 0:
                str_words = (emptychar * 11)
            else:
                str_words = (fillchar * 11)

        s = str_line_number + str_program_counter + str_words + ' ' + '\u250A' + ' ' + self.source_code
        return s


class AsmSourceFile:
    global global_line_number
    asm_lines = []
    special_tokens = [
        JMULexer.NL,
        JMULexer.INCLUDE,
        JMULexer.FILENAME,
        antlr4.Token.EOF
    ]

    def __init__(self, errors: list,  filename: str, ismain: bool):
        self.asm_lines = []
        self.filename = filename
        self.ismain = ismain
        self.errors = errors

    def get_listing(self):
        # After the file is loaded, join all the string representations
        # of the lines and return that as the "listing" for the file.
        listing = ''.join([str(line) for line in self.asm_lines]).rstrip()
        return listing

    def tokenize_file(self):
        input_stream = FileStream(self.filename, encoding='utf-8')
        lexer = JMULexer(self.errors, input_stream)
        token_stream = CommonTokenStream(lexer)
        token_stream.fill()
        return token_stream.tokens

    def load(self):
        global global_line_number

        file_tokens = self.tokenize_file()
        line_tokens = []
        line_no = 1
        include_pending = False

        for ct in file_tokens:

            if ct.type not in self.special_tokens:
                line_tokens.append(ct)
                continue

            if ct.type == JMULexer.NL:
                line_tokens.append(ct)
                asm_line = AsmLine(line_tokens.copy())
                asm_line.line_number = line_no
                asm_line.global_line_number = global_line_number
                self.asm_lines.append(asm_line)
                global_line_number += 1
                line_no += 1
                line_tokens.clear()

                if include_pending:
                    incfile = AsmSourceFile(self.errors, filename, ismain=False)
                    loaded = incfile.load()
                    for inc_asm_line in loaded.asm_lines:
                        self.asm_lines.append(inc_asm_line)
                    include_pending = False

            if ct.type == JMULexer.INCLUDE:
                # ct.text = ';' + ct.text + ' (included next)'
                line_tokens.append(ct)

            if ct.type == JMULexer.FILENAME:
                filename = ct.text.replace('"', '')
                line_tokens.append(ct)
                include_pending = True

            if ct.type == antlr4.Token.EOF:
                asm_line = AsmLine(line_tokens.copy())
                asm_line.line_number = line_no
                asm_line.global_line_number = global_line_number
                self.asm_lines.append(asm_line)
                global_line_number += 1
                line_no += 1

        # Return this file object to the project caller.
        return self


class AsmProject:
    final_filename = ''
    unhandledCount = 0

    def __init__(self, errors: list, final_filename: str):
        self.main_file = None
        self.final_filename = final_filename
        self.errors = errors

    def load(self, main_filename: str, ismain: bool):
        # Create a new AsmSourceFile object and add it to the list.
        file = AsmSourceFile(self.errors, main_filename, ismain)
        # Recursively load it up
        self.main_file = file.load()
        # Save it to the designated output file name for assembly.
        output = open(self.final_filename, 'w', newline='\n', encoding='utf-8')
        for line in self.main_file.asm_lines:
            output.write(line.source_code)
        output.close()
        return

    def check_unencoded_instructions(self):
        for asm_line in self.main_file.asm_lines:
            w1 = len(asm_line.instruction.words)  # how many words allocated in pass 1
            w2 = len(asm_line.words)  # how many words encoded
            if w1 != w2:
                self.errors.append(EmptyInstructionError(asm_line.line_number))

    def show_binary_breakdown(self):
        # UI stuff
        h = '\u2508'
        width = 100
        line1 = (h * 5) + ' INSTRUCTION LOCATION AND DECODING '

        print()
        print(line1.ljust(width, h))
        print('  line  addr   fmt op   args                     ┊ source')
        print(h * width)

        mask_at_msb    = 0b100000000000000000
        mask_at_opcode = 0b011110000000000000
        mask_opcode    = 0b011111100000000000
        mask_a         = 0b000000000000011111
        mask_b         = 0b000000001111100000
        mask_flagid    = 0b000000000000001110
        mask_flagsc    = 0b000000000000000001
        mask_s         = 0b000000011111111111
        mask_t         = 0b000001111111100000
        mask_opt5      = 0b100000000000100000
        mask_c         = 0b000000000000011111
        mask_opt10     = 0b000000010000000000

        MODE_AT = 1
        MODE_REGULAR = 0

        self.unhandledCount = 0
        for asm_line in self.main_file.asm_lines:

            # Bail out here if nothing to show for this assembly line.
            if asm_line.words is None or asm_line.program_counter is None:
                continue

            # Get the instruction from the asm_line object.
            i = asm_line.instruction

            # Bail out if instruction has no words, it's not executable.
            if len(i.words) == 0:
                continue

            # Assign the instruction words.
            iw1 = i.words[0]
            iw2 = None
            if len(i.words) > 1:
                iw2 = i.words[1]

            # Instantiate a decoder and decode the instruction words.
            handled = False

            idec = Decoder()
            idec.decodeIw1(iw1)
            if iw2 is not None:
                idec.decodeIw2(iw2)

            a = idec.a
            b = idec.b
            c = idec.c
            s = idec.s
            t = idec.t
            flagid = idec.flagid
            flagsc = idec.flagsc
            opt5 = idec.opt5
            opt10 = idec.opt10
            opcode = idec.opcode

            # Decide what type of "marker" to show in decoder listing.
            if idec.mode == InstructionMode.ModeAAT:
                fmt = '\u2593'
            else:
                fmt = '\u2591'

            # Format the start of the line the same for all.
            seg1 = '{0: 6} '.format(asm_line.global_line_number) + \
                   '${:05x}   '.format(asm_line.program_counter) + \
                   fmt + \
                   ' {0: 3}   '.format(opcode)
            seg2 = '?????'

            # Now just handle each opcode...

            if opcode == Arch.OPC_LD_GPR_EXPR:
                seg2 = 'ldw r{0:02}, ${1:05x}:{1}'.format(a, iw2)
                handled = True

            if opcode == Arch.OPC_LD_GPR_GPTR:
                seg2 = 'ld r{0:02}, [r{1:02}]'.format(a, b)
                handled = True

            # if opcode == Arch.OPC_LD_GPR_SPTR:
            #     seg2 = 'ld r{0:02}, [${1:05x}:{1}]'.format(a, iw2)
            #     handled = True

            if opcode == Arch.OPC_LD_SP:
                seg2 = 'sp = ${0:05x}:{0}'.format(iw2)
                handled = True

            if opcode == Arch.OPC_SV_EXPR_GPR:
                seg2 = 'sv ${0:05x}:{0}, r{1:02}'.format(iw2, a)
                handled = True

            if opcode == Arch.OPC_SV_GPTR_GPR:
                seg2 = 'sv [r{0:02}], r{1:02}'.format(a, b)
                handled = True

            # if opcode == Arch.OPC_SV_SPTR_GPR:
            #     seg2 = 'sv [{0:05x}:{0}], r{1:02}'.format(iw2, a)
            #     handled = True

            # if opcode == Arch.OPC_SV_GPTR_EXPR:
            #     seg2 = 'sv [r{0:02}], r{1:05x}:{1}'.format(a, iw2)
            #     handled = True

            if opcode == Arch.OPC_CALL_FAR:
                seg2 = 'call ${0:05x}:{0}'.format(iw2)
                handled = True

            if opcode == Arch.OPC_CALL_NEAR:
                seg2 = 'call {0}'.format(s)
                handled = True

            if opcode == Arch.OPC_JA:  # ja
                if flagid == 0:
                    seg2 = 'ja ${0:05x}:{0}'.format(iw2)
                elif flagid == 1:
                    if flagsc:
                        seg2 = 'ja.z ${0:05x}:{0}'.format(iw2)
                    else:
                        seg2 = 'ja.nz ${0:05x}:{0}'.format(iw2)
                handled = True

            if opcode == Arch.OPC_RET:
                if flagid == 0:
                    seg2 = 'ret'
                elif flagid == 1:
                    if flagsc:
                        seg2 = 'ret.z'
                    else:
                        seg2 = 'ret.nz'
                handled = True

            if opcode == Arch.OPC_PUSH:
                if opt5 == 1:
                    seg2 = 'push flags'
                else:
                    seg2 = 'push r{0:02}'.format(a)
                handled = True

            if opcode == Arch.OPC_IO_GPR:
                if opt5:
                    seg2 = 'io r{0:02}'.format(a)
                else:
                    seg2 = 'io- r{0:02}'.format(a)
                handled = True

            if opcode == Arch.OPC_POP:
                if opt5 == 1:
                    seg2 = 'pop flags'
                else:
                    seg2 = 'pop r{0:02}'.format(a)
                handled = True

            if opcode in [
                Arch.OPC_ADD_AB,
                Arch.OPC_SUB_AB,
                Arch.OPC_CMP_AB,
                Arch.OPC_MUL_AB,
                Arch.OPC_AND_AB,
                Arch.OPC_OR_AB,
                Arch.OPC_XOR_AB,
                Arch.OPC_COPY_AB
            ]:
                seg2 = 'r{0:02}, r{1:02}'.format(a, b)
                handled = True

            if opcode == Arch.OPC_DIV_ABC:
                seg2 = 'div r{0:02}, r{1:02}, r{2:02}'.format(a, b, c)
                handled = True

            if opcode == Arch.OPC_JR:
                seg2 = 'jr {0}'.format(s)
                handled = True

            if opcode == Arch.OPC_JRZ:
                seg2 = 'jr.z {0}'.format(s)
                handled = True

            if opcode == Arch.OPC_JRNZ:
                seg2 = 'jr.nz {0}'.format(s)
                handled = True

            if opcode == Arch.OPC_JRS:
                seg2 = 'jr.s {0}'.format(s)
                handled = True

            if opcode == Arch.OPC_JRNS:
                seg2 = 'jr.ns {0}'.format(s)
                handled = True

            # if opcode == Arch.OPC_TX_GPR:
            #     if opt5 == 1:
            #         seg2 = 'tx r{0:02}'.format(a)
            #     else:
            #         seg2 = 'rx r{0:02}'.format(a)
            #     handled = True

            if opcode == Arch.OPC_TX_GPR:
                seg2 = 'tx r{0:02}'.format(a)
                handled = True

            if opcode == Arch.OPC_RX_GPR:
                seg2 = 'rx r{0:02}'.format(a)
                handled = True

            if opcode == Arch.OPC_IO_GPR:
                if opt5 == 1:
                    seg2 = 'io r{0:02}'.format(a)
                else:
                    seg2 = 'io off'.format(a)
                handled = True

            # if opcode == Arch.OPC_SHIFT:
            #     if opt10 == 1:
            #         seg2 = 'shl r{0:02}, {1}'.format(a, b)
            #     else:
            #         seg2 = 'shr r{0:02}, {1}'.format(a, b)
            #     handled = True

            if opcode in [
                Arch.OPC_LD_A_T,
                Arch.OPC_ADD_A_T,
                Arch.OPC_SUB_A_T,
                Arch.OPC_CMP_A_T,
                Arch.OPC_MUL_A_T,
                Arch.OPC_AND_A_T,
                Arch.OPC_OR_A_T,
                Arch.OPC_XOR_A_T
            ]:
                seg2 = 'r{0:02}, {1:03}:${1:02x}'.format(a, t)
                handled = True

            if opcode == Arch.OPC_TX_T:
                seg2 = 'tx {0:03}:${0:02x}'.format(t)
                handled = True

            if opcode == Arch.OPC_IO_T:
                seg2 = 'io {0:03}:${0:02x}'.format(t)
                handled = True

            # If not handled before this, there's a problem.  So display message.
            if not handled:
                seg2 = 'ERROR: opcode {0} unhandled'.format(opcode)
                self.unhandledCount += 1

            # Print final result for this line.
            print(seg1 + fmtseg2(seg2, asm_line.source_code))

        # Print a footer when done with entire program and show result of decoding.
        if self.unhandledCount > 0:
            print('ERROR: some instructions unhandled')
        else:
            print('SUCCESS: all instructions handled')

        # Show a border and return.
        print(h * width)
        return


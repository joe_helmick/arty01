-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (win64) Build 2708876 Wed Nov  6 21:40:23 MST 2019
-- Date        : Sun Jun 14 09:09:58 2020
-- Host        : DESKTOP-THNB40S running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               D:/dev-attic/microprocessors/Arty01/revA/revA.srcs/sources_1/ip/sp_addsub/sp_addsub_stub.vhdl
-- Design      : sp_addsub
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a100tcsg324-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity sp_addsub is
  Port ( 
    A : in STD_LOGIC_VECTOR ( 17 downto 0 );
    ADD : in STD_LOGIC;
    CE : in STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 17 downto 0 )
  );

end sp_addsub;

architecture stub of sp_addsub is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "A[17:0],ADD,CE,S[17:0]";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "c_addsub_v12_0_14,Vivado 2019.2";
begin
end;

// alu.sv
// ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
`timescale 1ns/1ps
`include "arch_defines.vh"

module alu(
	input wire clock,
	input wire reset,
	input wire enable,
	input wire [3:0] operation,
	input wire [17:0] a,
	input wire [17:0] b,
	output reg [17:0] result
);

	always @ ( posedge clock )
	begin
		if (reset)
		begin
			result <= 0;
		end
		else
		begin 
			if (enable)
				case ( operation )

					`OP_ADD: result <= a + b;

					`OP_SUB,
					`OP_CMP: result <= a - b;

					// `OP_AND: result <= a & b;

					// `OP_OR: result <= a | b;

					// `OP_XOR: result <= a ^ b;

					default: 
						result <= 0;

				endcase
		end
	end

endmodule

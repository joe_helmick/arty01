; Conway's game of life by Joe Helmick (c) 2020
; designed for custom CPU by Joe Helmick (c) 2020
;
; This version of the game creates an "infinite" board, where the top and
; bottom rows wrap around and the left and right columns wrap around also.
; This allows for long-lived patterns like gliders to just keep going
; until they interact with other cells.  It's not so good for patterns like
; Gosper guns that spawn moving patterns, like gliders, as these gliders
; can wrap around and "attack" the established pattern, ruining it.
;
;==================================================================================================
.lstdecoding            ; show post-assembly decoding
.lstsymtable            ; show symbol dictionary after assembly
.lstprogress            ; show progress (for debugging of assembler only)

.equ    wwidth  	60		        ; width of board
.equ    wheight		30		        ; height of board
.equ    charalive	233		        ; denotes filled cell on code page cp437
;.equ    charalive	254		        ; denotes filled cell on code page cp437
.equ    chardead	250		        ; denotes empty cell on code page cp437
;.equ    charalive	219		        ; denotes filled cell on code page cp437
;.equ    chardead	176		        ; denotes empty cell on code page cp437
.equ    brdbufsize	(wwidth * wheight)      ; size of the board buffer
.equ    cr	        13		        ; carriage return character
.equ    lf		10	        	; line feed character
.equ    neg1            $3ffff                  ; what -1 would look like in signed math
.equ    nullchar        $00
.equ    spacechar       $20
.equ    escapechar      27

.struct $00300 vars_null ; these all get reset to $0
{
	generation      w 1
	population      w 1
	numberbuffer    w 7
}
.struct next vars_chardead ; these get reset to the "chardead" value
{
	brd1            w brdbufsize
	brd2            w brdbufsize
}

.string generationmsg 13, 10, "generation: ", 0
.string populationmsg "population: ", 0

;------------------------------------------------------------------------------
.org 0                          ; start here, jump to main routine
	ja      main
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
memfill:
;------------------------------------------------------------------------------
.alias  r30     @count                  ; number of words to fill
.alias  r31     @addr                   ; starting address
.alias  r29     @fillvalue              ; the value to fill with

	cmp     @count, 0
	ret.z
	sv      [@addr], @fillvalue
	add     @addr, 1
	sub     @count, 1
	jr      memfill

;------------------------------------------------------------------------------
copyboard:
;------------------------------------------------------------------------------
; Copy one block of memory (board) to another
; non-overlapping block of same size.
.alias  r31     @srcaddr
.alias  r30     @dstaddr
.alias  r29     @length

	ld      r28, [@srcaddr]
	sv      [@dstaddr], r28
	add     @srcaddr, 1
	add     @dstaddr, 1
	sub     @length, 1
	cmp     @length, 0
	jr.nz   copyboard
	ret

;------------------------------------------------------------------------------
showboard:
;------------------------------------------------------------------------------
; Display the board one character at a time.

.alias r31 @brdaddr
.alias r30 @row
.alias r29 @col
.alias r28 @char
.alias r27 @lastchar

	ld      @lastchar, 0                    ; initialize to zero so first comparison will fail
	ld      @row, 0                         ; start at row 0
nextrow2:
	ld      @col, 0                         ; start at column 0
nextpos2:
	ld      @char, [@brdaddr]               ; load the character from the board address
	cmp     @lastchar, @char                ; compare to last char
	jr.z    show_char                       ; if the same, show same as last, no ansi codes sent

show_different:
	tx      escapechar                      ; beginning of escape sequence same for both
	tx      '['
	tx      '3'
	tx      '8'
	tx      ';'
	tx      '5'
	tx      ';'

	cmp     @char, charalive
	jr.nz   show_different_dead

show_different_alive:
	tx      '2'
	tx      '0'
	tx      '9'
	tx      'm'
	jr      show_char

show_different_dead:
	tx      '2'
	tx      '4'
	tx      '2'
	tx      'm'

show_char:
	tx      @char
	jr      showboard_next

showboard_next:
	copy    @lastchar, @char                ; save this char for next iteration

	; move to next character
	add     @brdaddr, 1
	add     @col, 1
	cmp     @col, wwidth
	jr.nz   nextpos2

	; end of line, so do a clear EOL and a CRLF
	tx      27
	tx      '['
	tx      'K'
	tx      cr
	tx      lf

	; go to next row and bail if done
	add     @row, 1
	cmp     @row, wheight
	jr.nz   nextrow2
	ret

;------------------------------------------------------------------------------
getcurcharboard:
;------------------------------------------------------------------------------
.alias r31 @baseaddr
.alias r30 @row
.alias r29 @col
.alias r28 @offset
; re-use r29 to return contents at computed address

	ld      @offset, wwidth
	mul     @offset, @row
	add     @offset, @col
	add     @baseaddr, @offset
	ld      r29, [@baseaddr]
	ret

;------------------------------------------------------------------------------
loadpattern:
;------------------------------------------------------------------------------
; no side effects
; two gliders and two togglers in a 60 x 30 board
; starts off a nice 1000+ generation run
;             11111111112222222222333333333344444444445555555555
;   012345678901234567890123456789012345678901234567890123456789
;000............................................................
;060...x........................................................
;120....xx......................................................
;180...xx.................x.....................................
;240......................x.....................................
;300......................x.................................x...
;360.............................xxx......................xx....
;420.......................................................xx...
	push    r31
	ld      r31, charalive
	sv      (brd1 + 63), r31
	sv      (brd1 + 124), r31
	sv      (brd1 + 125), r31
	sv      (brd1 + 183), r31
	sv      (brd1 + 184), r31

	sv      (brd1 + 202), r31
	sv      (brd1 + 262), r31
	sv      (brd1 + 322), r31

	sv      (brd1 + 389), r31
	sv      (brd1 + 390), r31
	sv      (brd1 + 391), r31

	sv      (brd1 + 356), r31
	sv      (brd1 + 414), r31
	sv      (brd1 + 415), r31
	sv      (brd1 + 475), r31
	sv      (brd1 + 476), r31

	pop     r31
	ret

;------------------------------------------------------------------------------
print:
;------------------------------------------------------------------------------
.alias r31 @baseaddr
.alias r30 @printchar

	ld      @printchar, [@baseaddr]
	cmp     @printchar, nullchar
	ret.z
	tx      @printchar
	add     @baseaddr, 1
	jr      print

;------------------------------------------------------------------------------
clearnumbuffer:
;------------------------------------------------------------------------------
; Prepare number buffer to display with all spaces and a null terminator.
; No side effects.
	ld      r00, spacechar          ; load a space character
	sv      (numberbuffer + 0), r00 ; fill
	sv      (numberbuffer + 1), r00 ; fill
	sv      (numberbuffer + 2), r00 ; fill
	sv      (numberbuffer + 3), r00 ; fill
	sv      (numberbuffer + 4), r00 ; fill
	sv      (numberbuffer + 5), r00 ; fill
	ld      r00, nullchar           ; load a null character to end string
	sv      (numberbuffer + 6), r00 ; fill
	ret

;------------------------------------------------------------------------------
makedecnumber:
;------------------------------------------------------------------------------
.alias r31 @number
.alias r30 @digitpointer
.alias r29 @divisor
.alias r28 @remainder

	call    clearnumbuffer
	ldw     @digitpointer, (numberbuffer + 5)       ; point to 'ones' digit
	ld      @divisor, 10                            ; decimal, divide by 10 each time
divby10:
	div     @number, @divisor, @remainder
	add     @remainder, $30                         ; convert remainder to ASCII digit
	sv      [@digitpointer], @remainder             ; save remainder at current pointer
	sub     @digitpointer, 1                        ; move left one place for next digit
	cmp     @number, 0                              ; are we done yet?
	jr.nz   divby10
	ret

;------------------------------------------------------------------------------
homecursor:
;------------------------------------------------------------------------------
	; send ANSI sequence to home cursor
	tx      27      ; escape
	tx      '['
	tx      '0'
	tx      ';'
	tx      '0'
	tx      'H'
	ret

;------------------------------------------------------------------------------
eraseeol:
;------------------------------------------------------------------------------
	; send ANSI sequence to clear to end of line
	tx      27      ;escape
	tx      '['
	tx      'K'
	ret

;------------------------------------------------------------------------------
printcrlf:
;------------------------------------------------------------------------------
	tx      cr
	tx      lf
	ret

;------------------------------------------------------------------------------
do_generation:
;------------------------------------------------------------------------------

.alias r00 @r_cur
.alias r01 @r_above
.alias r02 @r_below
.alias r03 @c_cur
.alias r04 @c_left
.alias r05 @c_right
.alias r06 @neighbor_count
.alias r07 @population
.alias r08 @charalive
.alias r09 @chardead
.alias r10 @neg1


; set some constants
	ld      @charalive, charalive
	ld      @chardead, chardead
	ldw     @neg1, neg1
;reset the population count
	ld      @population, 0
;start at top left
	ld      @r_cur, 0
	ld      @c_cur, 0
next_position:

; Based on current row and current column, determine row above, row below,
; column to the left, column to the right.  Roll over from any edge to the
; other if out of bounds.
	copy    @r_above, @r_cur
	sub     @r_above, 1
	cmp     @r_above, @neg1
	jr.nz   dog_row_below
	ld      @r_above, (wheight - 1)

dog_row_below:
	copy    @r_below, @r_cur
	add     @r_below, 1
	cmp     @r_below, wheight
	jr.nz   dog_col_left
	ld      @r_below, 0

dog_col_left:
	copy    @c_left, @c_cur
	sub     @c_left, 1
	cmp     @c_left, @neg1
	jr.nz   dog_col_right
	ld      @c_left, (wwidth - 1)

dog_col_right:
	copy    @c_right, @c_cur
	add     @c_right, 1
	cmp     @c_right, wwidth
	jr.nz   dog_boundaries_done
	ld      @c_right, 0

dog_boundaries_done:

; Reset the neigbor count for this cell, about to accumulate a new count.
	ld      @neighbor_count, 0

dog_n_count1:
; . ? .
; . X .  row above, current column
; . . .
	ldw     r31, brd1               ; set base address
	copy    r30, @r_above            ; row above
	copy    r29, @c_cur              ; current column
	call    getcurcharboard
	cmp     r29, charalive
	jr.nz   dog_n_count2
	add     @neighbor_count, 1      ; neighbor alive, so inc count

dog_n_count2:
; . . ?
; . X .  row above, column to the right
; . . .
	ldw     r31, brd1               ; set base address
	copy    r30, @r_above            ; row above
	copy    r29, @c_right            ; column to the right
	call    getcurcharboard
	cmp     r29, charalive
	jr.nz   dog_n_count3
	add     @neighbor_count, 1      ; neighbor alive, so inc count

dog_n_count3:
; . . .
; . X ?  current row, column to the right
; . . .
	ldw     r31, brd1               ; set base address
	copy    r30, @r_cur              ; current row
	copy    r29, @c_right            ; column to the right
	call    getcurcharboard
	cmp     r29, charalive
	jr.nz   dog_n_count4
	add     @neighbor_count, 1      ; neighbor alive, so inc count

dog_n_count4:
; . . .
; . X .  row below, column to the right
; . . ?
	ldw     r31, brd1               ; set base address
	copy    r30, @r_below           ; row below
	copy    r29, @c_right           ; column to the right
	call    getcurcharboard
	cmp     r29, charalive
	jr.nz   dog_n_count5
	add     @neighbor_count, 1      ; neighbor alive, so inc count

dog_n_count5:
; . . .
; . X .  row below, current column
; . ? .
	ldw     r31, brd1               ; set base address
	copy    r30, @r_below           ; row below
	copy    r29, @c_cur             ; current column
	call    getcurcharboard
	cmp     r29, charalive
	jr.nz   dog_n_count6
	add     @neighbor_count, 1      ; neighbor alive, so inc count

dog_n_count6:
; . . .
; . X .  row below, column to the left
; ? . .
	ldw     r31, brd1               ; set base address
	copy    r30, @r_below            ; row below
	copy    r29, @c_left             ; column to the left
	call    getcurcharboard
	cmp     r29, charalive
	jr.nz   dog_n_count7
	add     @neighbor_count, 1      ; neighbor alive, so inc count

dog_n_count7:
; . . .
; ? X .  same row, column to the left
; . . .
	ldw     r31, brd1               ; set base address
	copy    r30, @r_cur              ; current row
	copy    r29, @c_left             ; column to the left
	call    getcurcharboard
	cmp     r29, charalive
	jr.nz   dog_n_count8
	add     @neighbor_count, 1      ; neighbor alive, so inc count

dog_n_count8:
; ? . .
; . X .  row above, column to the left
; . . .
	ldw     r31, brd1               ; set base address
	copy    r30, @r_above           ; row above
	copy    r29, @c_left            ; column to the left
	call    getcurcharboard
	cmp     r29, charalive
	jr.nz   dog_check_cur_cell
	add     @neighbor_count, 1      ; neighbor alive, so inc count

dog_check_cur_cell:

; . . .
; . ? .  current cell
; . . .
	ldw     r31, brd1               ; set base address
	copy    r30, @r_cur             ; current row
	copy    r29, @c_cur             ; current column
	call    getcurcharboard
	cmp     r29, charalive
	jr.nz   dog_cell_dead

; if we get here, current cell is alive, so test neighbor count to determine its fate
; r06 has current neighbor count

	cmp     @neighbor_count, 0
	jr.z    mark_board_2_dead       ; alive but lonely with 0 neighbors

	cmp     @neighbor_count, 1
	jr.z    mark_board_2_dead       ; alive but lonely with 1 neighbor

	cmp     @neighbor_count, 2
	jr.z    mark_board_2_alive      ; alive and survives with 2 neighbors

dog_tst_3_neighbors:
	cmp     @neighbor_count, 3
	jr.nz   mark_board_2_dead       ; not 3, so must be 4+; cell is overcrowded, so dies
	jr      mark_board_2_alive      ; alive and survives with 3 neighbors

dog_cell_dead:
	cmp     @neighbor_count, 3
	jr.z    mark_board_2_alive      ; dead cell with 3 neighbors spawns new live cell
	jr      mark_board_2_dead       ; dead cell with not 3 neighbors stays dead

mark_board_2_alive:
	ldw     r31, brd2               ; working with board 2 now
	ld      r28, wwidth             ; r28 initialized with width of board
	mul     r28, @r_cur             ; row offset = row# * width
	add     r28, @c_cur             ; now add column offset
	add     r31, r28                ; add offset to base address
	sv      [r31], @charalive       ; mark this cell alive
	add     @population, 1          ; alive, so add one to population
	jr      dog_next_position

mark_board_2_dead:
	ldw     r31, brd2               ; working with board 2 now
	ld      r28, wwidth             ; r28 initialized with width of board
	mul     r28, @r_cur             ; row offset = row# * width
	add     r28, @c_cur             ; now add column offset
	add     r31, r28                ; add offset to base address
	sv      [r31], @chardead        ; mark this cell dead

dog_next_position:
	add     @c_cur, 1               ; move to next column
	cmp     @c_cur, wwidth          ; check if we've moved beyond the right edge
	jr.z    rolloff_right           ; column rollover

dog_next_row:
	cmp     @r_cur, wheight         ; did we just roll off bottom?
	jr.nz   next_position           ; if not proceed

dog_done:
	ldw     r31, population         ; save population and return
	sv      [r31], @population
	ret

rolloff_right:
	ld      @c_cur, 0               ; reset to left column
	add     @r_cur, 1               ; ... of next row
	jr      dog_next_row




;;------------------------------------------------------------------------------
;delayminor:
;;------------------------------------------------------------------------------
;	ldw     r30, $00040
;delayminorrepeat:
;	cmp     r30, 0
;	ret.z
;	sub     r30, 1
;	jr      delayminorrepeat

;------------------------------------------------------------------------------
delay:
;------------------------------------------------------------------------------
	ldw     r31, 40000
delayloop:
	cmp     r31, 0
	ret.z
;	call    delayminor
	sub     r31, 1
	jr      delayloop


;------------------------------------------------------------------------------
wait_for_space:
	ld      r14, 0
	rx      r14
	cmp     r14, 32
	jr.nz    wait_for_space
	ret
;------------------------------------------------------------------------------


;------------------------------------------------------------------------------
main:
;------------------------------------------------------------------------------
	; reset the values that need to be null, as a group
	ldw     r31, vars_null
	ldw     r30, sizeof_vars_null
	ld      r29, $00
	call    memfill

	; reset the "boards" with "chardead" characters
	ldw     r31, vars_chardead
	ldw     r30, sizeof_vars_chardead
	ld      r29, chardead
	call    memfill

	; load the starting pattern of live cells
	call    loadpattern

mainloop:
	call    homecursor

	ldw     r31, brd1               ; show the current board
	call    showboard

	call    do_generation

	ldw     r31, brd2               ; copy board 2 to board 1
	ldw     r30, brd1
	ldw     r29, brdbufsize
	call    copyboard

	tx      escapechar
	tx      '['
	tx      '3'
	tx      '8'
	tx      ';'
	tx      '5'
	tx      ';'
	tx      '2'
	tx      '5'
	tx      '5'
	tx      'm'

	ldw     r31, generationmsg      ; load message and print it
	call    print

	ldw     r30, generation         ; load generation
	ld      r31, [r30]
	call    makedecnumber
	ldw     r31, numberbuffer       ; load number and print it
	call    print
	call    eraseeol
	call    printcrlf

	ldw     r31, populationmsg      ; load message and print it
	call    print

	ldw     r30, population         ; load population
	ld      r31, [r30]
	call    makedecnumber
	ldw     r31, numberbuffer       ; load number and print it
	call    print
	call    eraseeol
	call    printcrlf

	ldw     r31, generation         ; load generation, increment, save
	ld      r30, [r31]
	add     r30, 1
	sv      [r31], r30

	;call    delay

	;call    wait_for_space

	ja      mainloop                ; loop

.end

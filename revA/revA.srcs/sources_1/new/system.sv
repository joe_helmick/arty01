`include "arch_defines.vh"  // SET "SIMULATION" IN THIS FILE

module system(
	// Clocks and reset, actual physical system inputs.
		input wire brd_rst,
		input wire brd_clk,
		// Actual physical system outputs.
		output wire [7:0] io_byte,
		output wire io_strobe,
		output wire tx,
		output wire txled

		`ifdef SIMULATION 
		, 
		// Testing-only outputs
		output wire t_clk_sys,
		output wire t_sys_rst,
		output wire [6:0] t_state,
		output wire [17:0] t_iw1,
		output wire [17:0] t_iw2,
		output wire [2:0] t_flags, // each flag has its own position, none is position zero
		output wire [6:0] t_opcode,
		output wire [4:0] t_a,
		output wire [4:0] t_b,
		output wire [4:0] t_c,
		output wire [17:0] t_dina,
		output wire [17:0] t_dinc,
		output wire [17:0] t_douta,
		output wire [17:0] t_doutb,
		output wire [17:0] t_doutc,
		output wire t_wea,
		output wire t_wec,
		output wire signed [10:0] t_s,
		output wire [7:0] t_t,
		output wire t_opt5,
		output wire [1:0] t_flagid, // binary encoded
		output wire t_flagsc,
		output wire [17:0] t_pc,
		output wire signed [10:0] t_pc_delta,
		output wire t_pc_alu_en,
		output wire t_alu_en,
		output wire [17:0] t_alu_a,
		output wire [17:0] t_alu_b,
		output wire [17:0] t_alu_result,
		output wire [17:0] t_alu_md_result,
		output wire [17:0] t_alu_md_remainder,

	 	output wire t_muldiv_start,
		output wire t_muldiv_done,

		output wire [17:0] t_sp,
		output wire t_sp_alu_en,
		output wire t_sp_add,
		output wire t_fault,
		output wire [17:0] t_ram_data_addr,
		output wire [17:0] t_ram_data_dout,
		output wire [17:0] t_ram_code_dout,

		output wire t_txbusy
		
		`endif	
);
// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
// Decoded values
	wire [4:0] rega, regb, regc;
	wire [7:0] t;
	wire signed [10:0] s;

// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
// Clock-generator declarations.

	wire clk_sys;
	wire clk_locked;
	// This next line seems to be the key to coming out of reset properly
	// once the clock generator is "locked on." Signal brd_reset is ACTIVE
	// LOW, so system is "ready" when this signal is HIGH and clk_locked
	// is HIGH.  Therefore, system is not ready (i.e., in reset) when the
	// opposite is true.
	wire sys_ready = clk_locked & brd_rst;
	wire sys_rst = ~sys_ready ;

	clk_wiz_0 CLKGEN (
		.clk_in1(brd_clk),
		.resetn(brd_rst),
		.locked(clk_locked),
		.clk_out1(clk_sys)
	);

// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
// GPIO declarations
	reg [7:0] io_byteR;
	reg io_strobeR;

	OBUF gpio_0 ( .I(io_byteR[0]), .O(io_byte[0]) );
	OBUF gpio_1 ( .I(io_byteR[1]), .O(io_byte[1]) );
	OBUF gpio_2 ( .I(io_byteR[2]), .O(io_byte[2]) );
	OBUF gpio_3 ( .I(io_byteR[3]), .O(io_byte[3]) );
	OBUF gpio_4 ( .I(io_byteR[4]), .O(io_byte[4]) );
	OBUF gpio_5 ( .I(io_byteR[5]), .O(io_byte[5]) );
	OBUF gpio_6 ( .I(io_byteR[6]), .O(io_byte[6]) );
	OBUF gpio_7 ( .I(io_byteR[7]), .O(io_byte[7]) );
	OBUF gpio_strobe ( .I(io_strobeR), .O(io_strobe) );

// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
// PROGRAM COUNTER declarations.
	reg pc_enable;
	reg pc_action_step;
	reg pc_action_jr;
	reg pc_action_ja;
	reg [17:0] pc_ja_addr;
	reg [17:0] pc;

	pc_module PCMOD (
		.clock(clk_sys),
		.reset(sys_rst),
		.enable(pc_enable),
		.step(pc_action_step),
		.jr(pc_action_jr),
		.offset(s),
		.ja(pc_action_ja),
		.addr(pc_ja_addr),
		.pc(pc)
	);

// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
// RAM-related declarations.
	wire [17:0] ram_code_dout;

	reg [0:0] ram_data_we;
	reg [17:0] ram_data_addr;
	reg [17:0] ram_data_din;
	wire [17:0] ram_data_dout;

	BlockRAMDual256 RAM (
		.clka(clk_sys),
		.wea(1'b0),
		.addra(pc),
		.dina(18'b0),
		.douta(ram_code_dout),

		.clkb(clk_sys),
		.web(ram_data_we),
		.addrb(ram_data_addr),
		.dinb(ram_data_din),
		.doutb(ram_data_dout)
	);

// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
// ALU instantiations and declarations.
	reg which_alu;
	reg alu_en;
	reg [3:0] alu_op;
	reg [17:0] alu_a;
	reg [17:0] alu_b;

	reg [17:0] alu_result;
	alu ALU(
		.clock(clk_sys),
		.reset(sys_rst),
		.enable(alu_en),
		.operation(alu_op),
		.a(alu_a),
		.b(alu_b),
		.result(alu_result)
	);

	reg [17:0] alu_md_result;
	reg [17:0] alu_md_remainder;
	wire muldiv_start;
	wire muldiv_action;
	reg divdone;
	alu_muldiv DIVIDER (
		.clock(clk_sys),
		.a(alu_a),
		.b(alu_b),
		.muldiv(muldiv_action), // 1 to multiply, 0 to divide
		.start(muldiv_start),
		.result(alu_md_result),
		.remainder(alu_md_remainder),
		.divdone(divdone)
	);
	
	wire S = (which_alu == `USE_ALU_GENERAL) ? (alu_result[17] == 1'b1) : (alu_md_result[17] == 1'b1);
	wire Z = (which_alu == `USE_ALU_GENERAL) ? (alu_result == 18'b0) : (alu_md_result == 18'b0);

// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
// STACK POINTER ALU instantiation and declarations.
	reg [17:0] sp;
	reg sp_alu_en;      // 1 to activate, 0 to deactivate
	wire [17:0] sp_new; // output of the stack pointer alu
	reg sp_add;         // 1 for add, 0 for subtract
	sp_addsub STACK_ALU (
		.A(sp),
		.CE(sp_alu_en),
		.ADD(sp_add),
		.S(sp_new)
	);

// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
// REGISTER FILE declarations
	reg rf_wea, rf_wec;
	reg [17:0] rf_dina, rf_dinc;
	reg [17:0] rf_douta, rf_doutb, rf_doutc;
	regfile REGFILE(
		.clock(clk_sys),
		.wea(rf_wea),
		.wec(rf_wec),
		.addra(rega),
		.addrb(regb),
		.addrc(regc),
		.dina(rf_dina),
		.dinc(rf_dinc),
		.douta(rf_douta),
		.doutb(rf_doutb),
		.doutc(rf_doutc)
	);

// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
// RS-232 TX module declarations
	reg [7:0] txdata;
	reg txwrite;
	initial txwrite = 0;
	wire txbusy; // RS232 module busy indicator
	wire txport; // net between 232 module and IO buffer
	OBUF bufledtx ( .I(txbusy), .O(txled) );  // LED driver
	OBUF buftx ( .I(txport), .O(tx) );			// top-level IO port driver

	// CLOCKS_PER_BAUD : 60 Mhz / 115200 => 520.833 => 521  works
	//                 : 60 Mhz / 921600 >=  65.104 => 65   works
	//                 : 60 Mhz / 460800 >=  130.208 => 130  works

	//                 : 64 MHz / 460800 -> 138.888 -> 139 not tested
	//                 : 64 MHz / 921600 ->  69.444 ->  69 not tested
	
	//                 : 62 MHz / 460800 -> 134.548 -> 135 not tested
	//                 : 62 MHz / 921600 ->  67.274 ->  67 not tested

	//                 : 55.296 Mhz / 460800 -> 120 not tested
	//                 : 55.296 Mhz / 921600 ->  60 not tested
	


	txuartlite #(.TIMING_BITS(24), .CLOCKS_PER_BAUD(69) )
		TX232LITE (
		.i_clk(clk_sys),
		.i_wr(txwrite),
		.i_data(txdata),
		.o_uart_tx(txport),
		.o_busy(txbusy)
	);
	
// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
// FSM declarations
	reg done; // instructions indicate when they're done

	reg [6:0] fsm_state; 
	fsm FSM(
		.clock(clk_sys),
		.reset(sys_rst),
		.done(done),
		.fsm_state(fsm_state),
		.waiting(txbusy)
	);

// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
// PROGRAM EXECUTOR declarations

executor EXECUTOR(
	// inputs to executor
	.sys_rst(sys_rst),
	.clk_sys(clk_sys),
	.fsm_state(fsm_state),
	.ram_data_dout(ram_data_dout),
	.ram_code_dout(ram_code_dout),
	.rf_douta(rf_douta),
	.rf_doutb(rf_doutb),
	.rf_doutc(rf_doutc),
	.sp_new(sp_new),
	.pc(pc),
	.Z(Z),
	.S(S),
	.alu_result(alu_result),
	.alu_md_result(alu_md_result),
	.alu_md_remainder(alu_md_remainder),

	// outputs from executor
	`ifdef SIMULATION
		.iw1(t_iw1),
		.iw2(t_iw2),
		.opcode(t_opcode),
		.flagid(t_flagid),
		.flagsc(t_flagsc),
		.opt5(t_opt5),
		.flags(t_flags),
		.fault(t_fault),
	`endif

	.io_byte(io_byteR),
	.io_strobe(io_strobeR),
	.txdata(txdata),
	.txwrite(txwrite),

	.a(rega),
	.b(regb),
	.c(regc),
	.t(t),

	.pc_enable(pc_enable),
	.pc_action_step(pc_action_step),
	.pc_action_jr(pc_action_jr),
	.s(s),
	.pc_action_ja(pc_action_ja),
	.pc_ja_addr(pc_ja_addr),

	.ram_data_we(ram_data_we),
	.ram_data_addr(ram_data_addr),
	.ram_data_din(ram_data_din),

	.done(done),

	.rf_wea(rf_wea),
	.rf_wec(rf_wec),
	.rf_dina(rf_dina),
	.rf_dinc(rf_dinc),

	.sp_add(sp_add),
	.sp_alu_en(sp_alu_en),
	.sp(sp),

	.alu_en(alu_en),
	.alu_op(alu_op),
	.alu_a(alu_a),
	.alu_b(alu_b),
	.which_alu(which_alu),
	.muldiv_action(muldiv_action),
	.muldiv_start(muldiv_start)

);



// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
// SYSTEM AS MODULE OUTPUTS, DEBUG STUFF
	`ifdef SIMULATION
	assign t_clk_sys = clk_sys;
	assign t_sys_rst = sys_rst;
	assign t_state = fsm_state;

	assign t_a = rega;
	assign t_b = regb;
	assign t_c = regc;
	assign t_dina = rf_dina;

	assign t_dinc = rf_dinc;
	assign t_douta = rf_douta;
	assign t_doutb = rf_doutb;
	assign t_doutc = rf_doutc;
	assign t_wea = rf_wea;
	assign t_wec = rf_wec;
	assign t_s = s;
	assign t_t = t;
	assign t_pc = pc;
	assign t_pc_delta = s;
	assign t_pc_alu_en = pc_enable;
	
	assign t_alu_en = alu_en;
	assign t_alu_a = alu_a;
	assign t_alu_b = alu_b;
	assign t_alu_result = alu_result;
	assign t_alu_md_result = alu_md_result;
	assign t_alu_md_remainder = alu_md_remainder;
	assign t_muldiv_done = divdone;
	assign t_muldiv_start = muldiv_start;

	assign t_sp = sp;
	assign t_sp_alu_en = sp_alu_en;
	assign t_sp_add = sp_add;

	assign t_ram_data_addr = ram_data_addr;
	assign t_ram_data_dout = ram_data_dout;
	assign t_ram_code_dout = ram_code_dout;

	assign t_txbusy = txbusy;

	`endif

endmodule

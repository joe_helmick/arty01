grammar Directives;

directive
    : equate
    | org
    | struct
    | include
    | defstring
    | alias
    | printEachLine
    | printSymbolTable
    | printDecoding
    | bindata
    ;

bindata
    : BINDATA SYMNAME bindataList+
    ;

bindataItem
    : number    # bindataItemNumber
    | CHAR      # bindataItemChar
    ;

bindataList
    : bindataItem (SEP bindataItem)*
    ;

printEachLine
    : LSTPROGRESS
    ;

printSymbolTable
    : LSTSYMTABLE
    ;

printDecoding
    : LSTDECODING
    ;

equate
    : EQUATE SYMNAME expr
    ;

org
    : ORG expr
    ;

alias
    : ALIAS GPR ALIASNAME       # aliasdef
    ;

struct :
    structdecl structorigin structname
    LBRACE
        vardecl+
    RBRACE
    ;

structdecl
    : STRUCT
    ;

structorigin
    : number                      # defvarOriginNumber
    | SYMNAME                     # defvarOriginSymbol
    | ORIGIN_NEXT                 # defvarOriginNext
    ;

structname
    : SYMNAME
    ;

vardecl
    : SYMNAME DECLWORD expr (FILL expr)?
    ;

include
    : INCLUDE FILENAME
    ;

defstring
    : STRINGDECL SYMNAME stringpartcollection
    ;

stringpartcollection
    : stringpart (SEP stringpart)*
    ;

stringpart
    : number         #stringPartExpr
    | StringLiteral  #stringPartString
    | CHAR           # stringPartChar
    ;

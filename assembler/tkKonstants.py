from tkinter.font import Font


class tkkuistandards:

    WID_MAIN_WINDOW = 1496
    HEI_MAIN_WINDOW = 1080

    FONT_MONO_CODE = 'Iosevka Curly Medium'
    SIZ_FONT_SRC = 14
    HEI_LINE = 24

    # SIZ_FONT_SRC = 18
    # HEI_LINE = 28

    COLOR_MAIN_WINDOW = '#101010'  # verydark grey
    COLOR_BG_DEFAULT = '#303030'  # dark grey
    COLOR_TXT_DEFAULT = '#F6D87C'  # amber

    #COLOR_TXT_HEADER = '#FFFFFF'  # white
    COLOR_TXT_HEADER = '#238FBD'  # Pelorous

    COLOR_PC = '#7CF6D8'
    COLOR_IW = '#D7F67C'
    COLOR_LINENO = '#606060'

    # COLOR_BG_CURLINE = '#7CF6D8'
    # COLOR_BG_CURLINE = '#5DB8A2'
    COLOR_BG_CURLINE = '#3F7E6E'

    # COLOR_TXT_CURRENT = '#223C45'
    COLOR_TXT_CURRENT = '#FFFFFF'
    # COLOR_TXT_CURRENT = '#000000'  # black

    # COLOR_TXT_CHANGED = '#7CD7F6'
    # COLOR_TXT_CHANGED = '#F69B7C'
    # COLOR_TXT_CHANGED = '#F9BFAB'
    COLOR_TXT_CHANGED = '#F67CD8'

    COLOR_TXT_MEM_DEFAULT = '#cccccc'

    FROM_ZERO = 1
    FROM_RAMTOP = 0

    COLOR_BP_MARKER = '#FF6347'  # tomato
    COLOR_UPD_MARKER = '#FFFFFF'  # white

    COLOR_BP_BG = '#884444'

    font1 = None

    def __init__(self):
        self.font1 = Font(family=self.FONT_MONO_CODE, size=self.SIZ_FONT_SRC)

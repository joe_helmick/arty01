class Arch:

    # #################################################################################################################
    #  WIDTHS AND FUNDAMENTAL NUMERIC EXTREMES
    # #################################################################################################################
    # Word/read/write width.  The basis for everything :)
    WIDTH_WORD = 18
    # This is the total memory size, maximum allowed with the bits available
    MEMORY_DEPTH = (2 ** WIDTH_WORD)
    # Max and min signed and unsigned values for a register.
    MAX_SVALUE_REG = (2 ** (WIDTH_WORD - 1)) - 1
    MIN_SVALUE_REG = -(2 ** (WIDTH_WORD - 1))
    MAX_UVALUE_REG = (2 ** WIDTH_WORD) - 1
    MIN_UVALUE_REG = 0
    WID_GPR = 5

    # T unsigned 8-bit constant
    WID_T = 8
    MAX_VALUE_T = (2 ** WID_T) - 1
    MIN_VALUE_T = 0

    # S signed constant used for relative jumps and near backward calls.
    WID_S = 11
    MAX_VALUE_S = (2 ** (WID_S - 1)) - 1 - 1  # PC has moved
    MIN_VALUE_S = -(2 ** (WID_S - 1)) + 1  # same as above, PC has moved
    S_TWOS_SUBTRACTION = 2 ** WID_S

    @staticmethod
    def fits_in_word(value):
        if Arch.MIN_UVALUE_REG <= value <= Arch.MAX_UVALUE_REG:
            return True
        else:
            return False

    @staticmethod
    def fits_in_t(value):
        if Arch.MIN_VALUE_T <= value <= Arch.MAX_VALUE_T:
            return True
        else:
            return False

    @staticmethod
    def fits_in_s(value):
        if Arch.MIN_VALUE_S <= value <= Arch.MAX_VALUE_S:
            return True
        else:
            return False

    # @formatter:off

    # Opcode constants
    OPC_LD_GPR_EXPR     = 0
    OPC_LD_GPR_GPTR     = 1
#    OPC_LD_GPR_SPTR     = 2
    OPC_COPY_AB         = 3
    OPC_LD_SP           = 4
    OPC_SV_EXPR_GPR     = 5
    OPC_SV_GPTR_GPR     = 6
#    OPC_SV_SPTR_GPR     = 7
#    OPC_SV_GPTR_EXPR    = 8
    OPC_CALL_FAR        = 9
    OPC_CALL_NEAR       = 10
    OPC_ADD_AB          = 11
    OPC_SUB_AB          = 12
    OPC_CMP_AB          = 13
    OPC_MUL_AB          = 14
    OPC_AND_AB          = 15
    OPC_OR_AB           = 16
    OPC_XOR_AB          = 17
    OPC_DIV_ABC         = 18
    # gap
    OPC_JR              = 22
    OPC_JRZ             = 23
    OPC_JRNZ            = 24
    OPC_JRS             = 25
    OPC_JRNS            = 26
    OPC_JA              = 27
    OPC_RET             = 28
    OPC_PUSH            = 29
    OPC_POP             = 30
    OPC_IO_GPR          = 31
    OPC_TX_GPR          = 32
    OPC_RX_GPR          = 33
#    OPC_SHIFT           = 33

    # AT instructions
    OPC_LD_A_T          = 64
    OPC_ADD_A_T         = 65
    OPC_SUB_A_T         = 66
    OPC_CMP_A_T         = 67
    OPC_MUL_A_T         = 68
    OPC_AND_A_T         = 69
    OPC_OR_A_T          = 70
    OPC_XOR_A_T         = 71
    OPC_TX_T            = 72
    OPC_IO_T            = 73

    d_op2 = {
        #   name: ( opcode, wordcount, (word1 tuple), [word2 tuple] )
        #                                                                                     1111111 1
        #                                                                                     7654321 09876543210
        'i_ld_gpr_expr':   (OPC_LD_GPR_EXPR,    2, ('.6', 'a'), ('word',)),                 # op      ......aaaaa + word 2
        'i_ld_gpr_gptr':   (OPC_LD_GPR_GPTR,    1, ('.1', 'b', 'a')),                       # op      .bbbbbaaaaa
        # 'i_ld_gpr_sptr':   (OPC_LD_GPR_SPTR,    2, ('.6', 'a'), ('word',)),                 # op      ......aaaaa + word 2
        'i_copy_ab':       (OPC_COPY_AB,        1, ('.1', 'b', 'a')),                       # op      .bbbbbaaaaa
        'i_ld_sp':         (OPC_LD_SP,          2, ('.11',), ('word',)),                    # op      ........... + word 2

        'i_sv_expr_gpr':   (OPC_SV_EXPR_GPR,    2, ('.6', 'a'), ('word',)),                 # op      ......aaaaa + word 2
        'i_sv_gptr_gpr':   (OPC_SV_GPTR_GPR,    1, ('.1', 'b', 'a')),                       # op      .bbbbbaaaaa
        # 'i_sv_sptr_gpr':   (OPC_SV_SPTR_GPR,    2, ('.6', 'a'), ('word',)),                 # op      ......aaaaa + word 2
        # 'i_sv_gptr_expr':  (OPC_SV_GPTR_EXPR,   2, ('.6', 'a'), ('word',)),                 # op      ......aaaaa + word 2

        'i_call_far':      (OPC_CALL_FAR,       2, ('.11',), ('word',)),                    # op      ........... + word 2
        'i_call_near':     (OPC_CALL_NEAR,      1, ('s',)),                                 # op      sssssssssss

        'i_add_ab':        (OPC_ADD_AB,         1, ('.1', 'b', 'a')),                       # op      .bbbbbaaaaa
        'i_sub_ab':        (OPC_SUB_AB,         1, ('.1', 'b', 'a')),                       # op      .bbbbbaaaaa
        'i_cmp_ab':        (OPC_CMP_AB,         1, ('.1', 'b', 'a')),                       # op      .bbbbbaaaaa
        'i_mul_ab':        (OPC_MUL_AB,         1, ('.1', 'b', 'a')),                       # op      .bbbbbaaaaa
        # 'i_and_ab':        (OPC_AND_AB, 1, ('.1', 'b', 'a')),                             # op      .bbbbbaaaaa
        # 'i_or_ab':         (OPC_OR_AB, 1, ('.1', 'b', 'a')),                              # op      .bbbbbaaaaa
        # 'i_xor_ab':        (OPC_XOR_AB, 1, ('.1', 'b', 'a')),                             # op      .bbbbbaaaaa

        'i_div_abc':       (OPC_DIV_ABC,        2, ('.1', 'b', 'a'), ('.13', 'c')),         # op     .bbbbbaaaaa .............ccccc

        'i_jr':            (OPC_JR,             1, ('s',)),                                 # op      sssssssssss
        'i_jrz':           (OPC_JRZ,            1, ('s',)),                                 # op      sssssssssss
        'i_jrnz':          (OPC_JRNZ,           1, ('s',)),                                 # op      sssssssssss
        'i_jrs':           (OPC_JRS,            1, ('s',)),                                 # op      sssssssssss
        'i_jrns':          (OPC_JRNS,           1, ('s',)),                                 # op      sssssssssss

        'i_ja':            (OPC_JA,             2, ('.11',), ('word',)),                    # op      ........000 + word 2
        'i_jaz':           (OPC_JA,             2, ('.8', 'flag=z', 'ifset'), ('word',)),   # op      ........011 + word 2
        'i_janz':          (OPC_JA,             2, ('.8', 'flag=z', 'ifclr'), ('word',)),   # op      ........010 + word 2
        'i_jas':           (OPC_JA,             2, ('.8', 'flag=s', 'ifset'), ('word',)),   # op      ........101 + word 2
        'i_jans':          (OPC_JA,             2, ('.8', 'flag=s', 'ifclr'), ('word',)),   # op      ........100 + word 2

        'i_ret':           (OPC_RET,            1, ('.11',)),                               # op      ........000
        'i_retz':          (OPC_RET,            1, ('.8', 'flag=z', 'ifset')),              # op      ........011
        'i_retnz':         (OPC_RET,            1, ('.8', 'flag=z', 'ifclr')),              # op      ........010
        'i_rets':          (OPC_RET,            1, ('.8', 'flag=s', 'ifset')),              # op      ........101
        'i_retns':         (OPC_RET,            1, ('.8', 'flag=s', 'ifclr')),              # op      ........100

        'i_push_reg':      (OPC_PUSH,           1, ('.5', 'reg', 'a')),                     # op      .....0aaaaa
        'i_push_flags':    (OPC_PUSH,           1, ('.5', 'flags', '.5')),                  # op      .....1.....
        'i_io_gpr':        (OPC_IO_GPR,         1, ('.5', 'io+', 'a')),                     # op      .....1aaaaa
        'i_io_off':        (OPC_IO_GPR,         1, ('.5', 'io-', '.5')),                    # op      .....0.....
        'i_pop_reg':       (OPC_POP,            1, ('.5', 'reg', 'a')),                     # op      .....0aaaaa
        'i_pop_flags':     (OPC_POP,            1, ('.5', 'flags', '.5')),                  # op      .....1.....
        'i_tx_gpr':        (OPC_TX_GPR,         1, ('.6', 'tx', 'a')),                      # op      ......aaaaa
        'i_rx_gpr':        (OPC_RX_GPR,         1, ('.6', 'rx', 'a')),                      # op      ......aaaaa

        # 'i_shl':           (OPC_SHIFT,         1, ('shl', 'b', 'a')),                       # op      1bbbbbaaaaa
        # 'i_shr':           (OPC_SHIFT,         1, ('shr', 'b', 'a')),                       # op      0bbbbbaaaaa

        # HI MSB (AT) INSTRUCTIONS                                                            1 __ 1111 111
        #         opcode, wordcount, 4-bitopcode, T , A/.                                     7 __ 6543 2109876543210
        'i_ld_a_t':        (OPC_LD_A_T,         1, 0, ('t', 'a')),                          # 1 00  0   ttttttttaaaaa
        'i_add_a_t':       (OPC_ADD_A_T,        1, 1, ('t', 'a')),                          # 1 00  1   ttttttttaaaaa
        'i_sub_a_t':       (OPC_SUB_A_T,        1, 2, ('t', 'a')),                          # 1 00  2   ttttttttaaaaa
        'i_cmp_a_t':       (OPC_CMP_A_T,        1, 3, ('t', 'a')),                          # 1 00  3   ttttttttaaaaa
        'i_mul_a_t':       (OPC_MUL_A_T,        1, 4, ('t', 'a')),                          # 1 00  4   ttttttttaaaaa
        # 'i_and_a_t':       (OPC_AND_A_T, 1, 5, ('t', 'a')),                               # 1 00  5   ttttttttaaaaa
        # 'i_or_a_t':        (OPC_OR_A_T, 1, 6, ('t', 'a')),                                # 1 00  6   ttttttttaaaaa
        # 'i_xor_a_t':       (OPC_XOR_A_T, 1, 7, ('t', 'a')),                               # 1 00  7   ttttttttaaaaa
        'i_tx_t':          (OPC_TX_T,           1, 8, ('t', '.5')),                         # 1 00  8   tttttttt.....
        'i_io_t':          (OPC_IO_T,           1, 9, ('t', '.5')),                         # 1 00  9   tttttttt.....

    }

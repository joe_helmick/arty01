.equ stacktop $2100
.data $1000 numbufstruct
{
              ; $1000 1001 1002 1003 1004 1005 1006
              ;  4096 4097 4098 4099 4100 4101 4102
    buffer  w   7
    space   w   1   fill $20
}
.equ ones_position (buffer + 5)

.org 0
reset:
    ld      sp, stacktop        ; arbitrary location to test instruction
    ldw     r20, space          ; points to space character
    ja      main                ; go to main routine

;----------------------------------------------------------------------
bin2hex:
;   input: r01 has value to convert to string
;   output: <buffer> points at beginning of string
;----------------------------------------------------------------------
    ldw     r04, ones_position  ; point to the digits, ones position
    ld      r02, 16             ; r02 holds the divisor
divby16:
    div     r01, r02, r03       ; r03 gets remainder
    cmp     r03, 10             ; shows negative if register is 9 or less
    jr.s    save_digit          ; negative? must be a digit
    add     r03, 7              ; skip the ASCII gap to the letters
save_digit:
    add     r03, $30            ; add $30 to remainder to get ASCII value
    sv      [r04], r03          ; save the remainder at current position
    sub     r04, 1              ; move left one place for next digit
    cmp     r01, 0              ; decide if we're done yet
    jr.nz   divby16             ; not done? repeat for next remaining value
    ret

;----------------------------------------------------------------------
io_number:
;   alters r02, r01
;----------------------------------------------------------------------
    ldw     r02, buffer         ; point at beginning of buffer
next_char:
    ld      r01, [r02]          ; load the value
    cmp     r01, 0              ; if terminating null,
    ret.z                       ; then return, done
    io1.on  r01                 ; io+
    io1.off r01                 ; io-
    add     r02, 1              ; move to next position
    jr      next_char           ; repeat

;----------------------------------------------------------------------
main:
;----------------------------------------------------------------------
    ldw     r01, $f34b
    call    init_buffer
    call    bin2hex
    call    io_number
    ld      r01, 66 ; "B"
    push    r01
    ld      r01, 65 ; "A"
    io1.on  r01                 ; show "A"
    pop     r01
    io1.on  r01                 ; show "B"
    io1.off r01
    jr      reset
;----------------------------------------------------------------------

.org $2000   ; far call forced by forward AND long
;----------------------------------------------------------------------
init_buffer:
;----------------------------------------------------------------------
    ; set up buffer by filling with spaces and trailing null
    ld      r20, [r20]              ; load from pointer
    copy    r23, r20                ; copy to test copy instruction
    sv      (buffer + 0), r23
    sv      (buffer + 1), r23
    sv      (buffer + 2), r23
    sv      (buffer + 3), r23
    sv      (buffer + 4), r23
    sv      (buffer + 5), r23
    ld      r23, 0
    sv      (buffer + 6), r23
    ret
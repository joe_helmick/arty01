from bitstring import *


# Parse a '.n' value where n is an integer, indicating how many spaces
# (encoded as zeroes) to add as filler bits for the instruction.
def fill_count(item: str):
    i = item.replace('.', '')
    count = int(i)
    return count


class Instruction:

    def __init__(self):
        self.line = None
        self.msb = None
        self.opcode = None
        self.words = []
        self.pc = None
        self.source_line = None
        self.size = None
        self.ra = None
        self.rb = None
        self.k = None
        self.iw1 = None
        self.iw2 = None
        self.mode = None
        self.cc = None

    def __str__(self):
        s = str(self.opcode) + ' ' + \
            str('{0:05x}'.format(self.iw1)) + ' ' + \
            self.source_line.rstrip()
        return s

    # Form an instruction.
    def make(self, lineno: int, source: str, pc: int,
             info: tuple,
             a: int = 0,
             b: int = 0,
             c: int = 0,
             t: int = 0,
             s: int = 0,
             iw2: int = 0):

        # Fill in basic info from info tuple and AsmLine object.
        self.opcode = info[0]
        self.size = info[1]
        self.line = lineno
        self.source_line = source
        self.pc = pc

        do_print = False

        if do_print:
            print(lineno, source, end='')

        # Compute the opcode first, and start the instruction word with it.
        if self.opcode < 64:
            # Normal instruction with regular 7-bit opcode
            ba1 = BitArray(uint=self.opcode, length=7)
            iw1info = info[2]
        else:
            # AT instruction, MSB = 1 then 4 bit opcode from the dictionary info
            ba1 = BitArray(bin='1')
            self.opcode = info[2]
            ba1.append(BitArray(uint=self.opcode, length=4))
            iw1info = info[3]

        if do_print:
            print(ba1.bin + ' | ', end='')

        # Add bits based on items in the tuple of instruction metadata.
        for item in iw1info:

            # Skip over non-string items
            if not isinstance(item, str):
                continue

            # Insert fillers as necessary
            if item.startswith('.'):
                fill_len = fill_count(item)
                ba1.append(BitArray(bin='0' * fill_len))

            if item == 'flag=z':
                ba1.append(BitArray(uint=1, length=2))

            if item == 'flag=s':
                ba1.append(BitArray(uint=2, length=2))

            if item == 'ifset':
                ba1.append(BitArray(bin='1'))  # set

            if item == 'ifclr':
                ba1.append(BitArray(bin='0'))  # clr

            if item == 'reg':
                ba1.append(BitArray(bin='0'))

            if item == 'flags':
                ba1.append(BitArray(bin='1'))

            if item == 'b':
                ba1.append(BitArray(uint=b, length=5))

            if item == 'a':
                ba1.append(BitArray(uint=a, length=5))

            if item == 's':
                ba1.append(BitArray(int=s, length=11))

            if item == 'io+':
                ba1.append(BitArray(bin='1'))

            if item == 'io-':
                ba1.append(BitArray(bin='0'))

            if item == 't':
                ba1.append(BitArray(uint=t, length=8))

            # if item == 'tx':
            #     ba1.append(BitArray(bin='1'))

            # if item == 'shl':
            #     ba1.append(BitArray(bin='1'))

            # if item == 'shr':
            #     ba1.append(BitArray(bin='0'))

        if do_print:
            print(ba1.bin)

        # Make sure the resulting instruction is the proper length; if not,
        # then there is a serious encoding problem.
        assert len(ba1.bin) == 18

        # Convert to integer, add IW2 if necessary, and finish up.
        self.iw1 = int(str(ba1), 2)
        self.words.append(self.iw1)

        # Handle second word if necessary.
        if self.size == 2:
            iw2info = info[3]

            ba2 = BitArray()

            # Add bits based on items in the tuple of instruction metadata.
            for item in iw2info:

                # Skip over non-string items
                if not isinstance(item, str):
                    continue

                # Insert fillers as necessary
                if item.startswith('.'):
                    fill_len = fill_count(item)
                    ba2.append(BitArray(bin='0' * fill_len))

                if item == 'c':
                    ba2.append(BitArray(uint=c, length=5))

                if item == 'word':
                    ba2.append(BitArray(uint=iw2, length=18))

            if do_print:
                print(ba2.bin)

            assert len(ba2.bin) == 18

            # Convert to integer, add IW2 if necessary, and finish up.
            self.iw2 = int(str(ba2), 2)
            self.words.append(self.iw2)

        # print(str(self))

        return self

from vm import VirtualMachine
import sys


class AnsiColors:
    # https: // www.lihaoyi.com / post / BuildyourownCommandLinewithANSIescapecodes.html
    # \u001b[38;5;<code>m

    # NORMAL = '\u001b[37m'  # white
    RESET = '\u001b[0m'  # default console color
    # CHANGED = ' \u001b[31m' # red
    CHANGED = ' \u001b[38;5;226m'  # bright yellow
    NORMAL = '\u001b[38;5;246m'  # dark grey


run = True


def find_end_pc():
    lst = open('D:\\dev-attic\\microprocessors\\Arty01\\assembler\\__main.lst', 'r', encoding='utf-8')
    while True:
        line = lst.readline()
        if '             \u250A .end' in line:
            pcs = line[6:11]
            pc = int(pcs, 16)
            break
    return pc


def termColor(prev, curr):
    if prev != curr:
        return AnsiColors.CHANGED
    else:
        return AnsiColors.NORMAL


def showRegisters(vm):
    # ndx = 0
    # for r in range(len(vm.gpr)):
    #     if ndx % 4 == 0:
    #         print()
    #     prev = vm.gpr[r][2]
    #     curr = vm.gpr[r][1]
    #     color = termColor(prev, curr)
    #     print(color + 'r{0:02} '.format(r) + '{0:06} '.format(curr) + '${0:05x} '.format(curr), end='')
    #     ndx += 1
    # print()

    for r in range(len(vm.gpr)):
        prev = vm.gpr[r][2]
        curr = vm.gpr[r][1]
        if prev != curr:
            print(AnsiColors.CHANGED + 'r{0:02} '.format(r) + '{0:06} '.format(curr) + '${0:05x} '.format(curr))

    prev = vm.PC_prev
    curr = vm.PC
    color = termColor(prev, curr)
    print(color + 'PC=${0:05x}  '.format(curr), end='')

    prev = vm.SP_prev
    curr = vm.SP
    color = termColor(prev, curr)
    print(color + 'SP=${0:05x}  '.format(curr), end='')

    prev = vm.flagZ_prev
    curr = vm.flagZ
    color = termColor(prev, curr)
    print(color + 'Z={0}  '.format(curr), end='')

    prev = vm.flagS_prev
    curr = vm.flagS
    color = termColor(prev, curr)
    print(color + 'S={0}  '.format(curr), end='')

    # Finish by changing back to normal color.
    print(AnsiColors.RESET)
    return

def main():
    VM = VirtualMachine()

    end_pc = find_end_pc()

    if run:

        while True:
            if VM.PC <= end_pc:
                VM.run_next_instruction()
                showRegisters(VM)
            else:
                break

    else:
        while True:
            key = input()
            if VM.PC <= end_pc:
                VM.run_next_instruction()
                showRegisters(VM)


if __name__ == '__main__':
    main()

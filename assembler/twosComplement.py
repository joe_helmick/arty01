from bitstring import Bits


def printValues(bitcount: int):
    max_svalue_reg = (2 ** (bitcount - 1)) - 1
    min_svalue_reg = -(2 ** (bitcount - 1))

    for v in range(min_svalue_reg, max_svalue_reg + 1):
        b = Bits(int=v, length=bitcount)

        strb = str(b.bin).replace('0b', '')
        strboriginal = strb

        bitsleft = bitcount
        nibblelist = []
        while bitsleft > 0:
            nibble = strb[-4:]

            if len(nibble) < 4:
                pad = '0' * (4 - len(nibble))
                nibble = pad + nibble

            nibblelist.insert(0, nibble)
            strb = strb[:-4]
            bitsleft -= 4

        news = ''.join(nibblelist)
        b2 = Bits(bin=news)
        print("v {0}   str {1}   hex {2}".format(v, strboriginal, b2))


def main():
    printValues(8)


if __name__ == '__main__':
    main()
    exit(0)


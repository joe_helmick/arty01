# Generated from ..\Assembler.g4 by ANTLR 4.8
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3N")
        buf.write("\u0162\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31")
        buf.write("\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36")
        buf.write("\4\37\t\37\3\2\7\2@\n\2\f\2\16\2C\13\2\3\2\3\2\3\2\3\3")
        buf.write("\3\3\3\4\3\4\3\4\5\4M\n\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5")
        buf.write("\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3")
        buf.write("\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5")
        buf.write("\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3")
        buf.write("\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5")
        buf.write("\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3")
        buf.write("\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5")
        buf.write("\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5\u00b3")
        buf.write("\n\5\3\5\3\5\3\5\5\5\u00b8\n\5\3\5\3\5\3\5\5\5\u00bd\n")
        buf.write("\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5")
        buf.write("\3\5\3\5\3\5\3\5\3\5\3\5\5\5\u00d2\n\5\3\6\3\6\3\6\5\6")
        buf.write("\u00d7\n\6\3\7\3\7\3\7\3\7\5\7\u00dd\n\7\3\b\3\b\3\b\3")
        buf.write("\b\3\t\3\t\3\t\3\t\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3")
        buf.write("\13\3\13\3\13\5\13\u00f1\n\13\3\13\3\13\3\13\3\13\3\13")
        buf.write("\3\13\3\13\3\13\3\13\7\13\u00fc\n\13\f\13\16\13\u00ff")
        buf.write("\13\13\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r")
        buf.write("\5\r\u010d\n\r\3\16\3\16\3\16\6\16\u0112\n\16\r\16\16")
        buf.write("\16\u0113\3\17\3\17\5\17\u0118\n\17\3\20\3\20\3\20\7\20")
        buf.write("\u011d\n\20\f\20\16\20\u0120\13\20\3\21\3\21\3\22\3\22")
        buf.write("\3\23\3\23\3\24\3\24\3\24\3\24\3\25\3\25\3\25\3\26\3\26")
        buf.write("\3\26\3\26\3\27\3\27\3\27\3\27\3\27\6\27\u0138\n\27\r")
        buf.write("\27\16\27\u0139\3\27\3\27\3\30\3\30\3\31\3\31\3\31\5\31")
        buf.write("\u0143\n\31\3\32\3\32\3\33\3\33\3\33\3\33\3\33\5\33\u014c")
        buf.write("\n\33\3\34\3\34\3\34\3\35\3\35\3\35\3\35\3\36\3\36\3\36")
        buf.write("\7\36\u0158\n\36\f\36\16\36\u015b\13\36\3\37\3\37\3\37")
        buf.write("\5\37\u0160\n\37\3\37\2\3\24 \2\4\6\b\n\f\16\20\22\24")
        buf.write("\26\30\32\34\36 \"$&(*,.\60\62\64\668:<\2\6\4\2\3\3AA")
        buf.write("\3\2\61\62\3\2\63\64\3\2BD\2\u0187\2A\3\2\2\2\4G\3\2\2")
        buf.write("\2\6L\3\2\2\2\b\u00d1\3\2\2\2\n\u00d6\3\2\2\2\f\u00dc")
        buf.write("\3\2\2\2\16\u00de\3\2\2\2\20\u00e2\3\2\2\2\22\u00e6\3")
        buf.write("\2\2\2\24\u00f0\3\2\2\2\26\u0100\3\2\2\2\30\u010c\3\2")
        buf.write("\2\2\32\u010e\3\2\2\2\34\u0117\3\2\2\2\36\u0119\3\2\2")
        buf.write("\2 \u0121\3\2\2\2\"\u0123\3\2\2\2$\u0125\3\2\2\2&\u0127")
        buf.write("\3\2\2\2(\u012b\3\2\2\2*\u012e\3\2\2\2,\u0132\3\2\2\2")
        buf.write(".\u013d\3\2\2\2\60\u0142\3\2\2\2\62\u0144\3\2\2\2\64\u0146")
        buf.write("\3\2\2\2\66\u014d\3\2\2\28\u0150\3\2\2\2:\u0154\3\2\2")
        buf.write("\2<\u015f\3\2\2\2>@\5\6\4\2?>\3\2\2\2@C\3\2\2\2A?\3\2")
        buf.write("\2\2AB\3\2\2\2BD\3\2\2\2CA\3\2\2\2DE\5\4\3\2EF\7\2\2\3")
        buf.write("F\3\3\2\2\2GH\7/\2\2H\5\3\2\2\2IM\5\30\r\2JM\7@\2\2KM")
        buf.write("\5\b\5\2LI\3\2\2\2LJ\3\2\2\2LK\3\2\2\2M\7\3\2\2\2NO\7")
        buf.write("\f\2\2OP\5\22\n\2PQ\7<\2\2QR\5\24\13\2R\u00d2\3\2\2\2")
        buf.write("ST\7\13\2\2TU\5\22\n\2UV\7<\2\2VW\5\24\13\2W\u00d2\3\2")
        buf.write("\2\2XY\7\13\2\2YZ\5\22\n\2Z[\7<\2\2[\\\5\16\b\2\\\u00d2")
        buf.write("\3\2\2\2]^\7\13\2\2^_\5\22\n\2_`\7<\2\2`a\5\20\t\2a\u00d2")
        buf.write("\3\2\2\2bc\7\16\2\2cd\5\22\n\2de\7<\2\2ef\5\22\n\2f\u00d2")
        buf.write("\3\2\2\2gh\7\13\2\2hi\7)\2\2ij\7<\2\2j\u00d2\5\24\13\2")
        buf.write("kl\7\r\2\2lm\5\24\13\2mn\7<\2\2no\5\22\n\2o\u00d2\3\2")
        buf.write("\2\2pq\7\r\2\2qr\5\16\b\2rs\7<\2\2st\5\22\n\2t\u00d2\3")
        buf.write("\2\2\2uv\7\r\2\2vw\5\20\t\2wx\7<\2\2xy\5\22\n\2y\u00d2")
        buf.write("\3\2\2\2z{\7\r\2\2{|\5\16\b\2|}\7<\2\2}~\5\24\13\2~\u00d2")
        buf.write("\3\2\2\2\177\u0080\7\31\2\2\u0080\u00d2\7?\2\2\u0081\u0082")
        buf.write("\7\17\2\2\u0082\u0083\5\22\n\2\u0083\u0084\7<\2\2\u0084")
        buf.write("\u0085\5\24\13\2\u0085\u00d2\3\2\2\2\u0086\u0087\7\17")
        buf.write("\2\2\u0087\u0088\5\22\n\2\u0088\u0089\7<\2\2\u0089\u008a")
        buf.write("\5\22\n\2\u008a\u00d2\3\2\2\2\u008b\u008c\7\20\2\2\u008c")
        buf.write("\u008d\5\22\n\2\u008d\u008e\7<\2\2\u008e\u008f\5\24\13")
        buf.write("\2\u008f\u00d2\3\2\2\2\u0090\u0091\7\20\2\2\u0091\u0092")
        buf.write("\5\22\n\2\u0092\u0093\7<\2\2\u0093\u0094\5\22\n\2\u0094")
        buf.write("\u00d2\3\2\2\2\u0095\u0096\7\21\2\2\u0096\u0097\5\22\n")
        buf.write("\2\u0097\u0098\7<\2\2\u0098\u0099\5\24\13\2\u0099\u00d2")
        buf.write("\3\2\2\2\u009a\u009b\7\21\2\2\u009b\u009c\5\22\n\2\u009c")
        buf.write("\u009d\7<\2\2\u009d\u009e\5\22\n\2\u009e\u00d2\3\2\2\2")
        buf.write("\u009f\u00a0\7\25\2\2\u00a0\u00a1\5\22\n\2\u00a1\u00a2")
        buf.write("\7<\2\2\u00a2\u00a3\5\24\13\2\u00a3\u00d2\3\2\2\2\u00a4")
        buf.write("\u00a5\7\25\2\2\u00a5\u00a6\5\22\n\2\u00a6\u00a7\7<\2")
        buf.write("\2\u00a7\u00a8\5\22\n\2\u00a8\u00d2\3\2\2\2\u00a9\u00aa")
        buf.write("\7\26\2\2\u00aa\u00ab\5\22\n\2\u00ab\u00ac\7<\2\2\u00ac")
        buf.write("\u00ad\5\22\n\2\u00ad\u00ae\7<\2\2\u00ae\u00af\5\22\n")
        buf.write("\2\u00af\u00d2\3\2\2\2\u00b0\u00b2\7\4\2\2\u00b1\u00b3")
        buf.write("\5\f\7\2\u00b2\u00b1\3\2\2\2\u00b2\u00b3\3\2\2\2\u00b3")
        buf.write("\u00b4\3\2\2\2\u00b4\u00d2\7?\2\2\u00b5\u00b7\7\5\2\2")
        buf.write("\u00b6\u00b8\5\f\7\2\u00b7\u00b6\3\2\2\2\u00b7\u00b8\3")
        buf.write("\2\2\2\u00b8\u00b9\3\2\2\2\u00b9\u00d2\7?\2\2\u00ba\u00bc")
        buf.write("\7\6\2\2\u00bb\u00bd\5\f\7\2\u00bc\u00bb\3\2\2\2\u00bc")
        buf.write("\u00bd\3\2\2\2\u00bd\u00d2\3\2\2\2\u00be\u00bf\7\27\2")
        buf.write("\2\u00bf\u00d2\7\36\2\2\u00c0\u00c1\7\27\2\2\u00c1\u00d2")
        buf.write("\5\22\n\2\u00c2\u00c3\7\30\2\2\u00c3\u00d2\7\36\2\2\u00c4")
        buf.write("\u00c5\7\30\2\2\u00c5\u00d2\5\22\n\2\u00c6\u00c7\7\32")
        buf.write("\2\2\u00c7\u00d2\5\22\n\2\u00c8\u00c9\7\34\2\2\u00c9\u00d2")
        buf.write("\5\22\n\2\u00ca\u00cb\7\32\2\2\u00cb\u00d2\5\n\6\2\u00cc")
        buf.write("\u00cd\7\34\2\2\u00cd\u00d2\5\n\6\2\u00ce\u00d2\7\35\2")
        buf.write("\2\u00cf\u00d0\7\33\2\2\u00d0\u00d2\5\22\n\2\u00d1N\3")
        buf.write("\2\2\2\u00d1S\3\2\2\2\u00d1X\3\2\2\2\u00d1]\3\2\2\2\u00d1")
        buf.write("b\3\2\2\2\u00d1g\3\2\2\2\u00d1k\3\2\2\2\u00d1p\3\2\2\2")
        buf.write("\u00d1u\3\2\2\2\u00d1z\3\2\2\2\u00d1\177\3\2\2\2\u00d1")
        buf.write("\u0081\3\2\2\2\u00d1\u0086\3\2\2\2\u00d1\u008b\3\2\2\2")
        buf.write("\u00d1\u0090\3\2\2\2\u00d1\u0095\3\2\2\2\u00d1\u009a\3")
        buf.write("\2\2\2\u00d1\u009f\3\2\2\2\u00d1\u00a4\3\2\2\2\u00d1\u00a9")
        buf.write("\3\2\2\2\u00d1\u00b0\3\2\2\2\u00d1\u00b5\3\2\2\2\u00d1")
        buf.write("\u00ba\3\2\2\2\u00d1\u00be\3\2\2\2\u00d1\u00c0\3\2\2\2")
        buf.write("\u00d1\u00c2\3\2\2\2\u00d1\u00c4\3\2\2\2\u00d1\u00c6\3")
        buf.write("\2\2\2\u00d1\u00c8\3\2\2\2\u00d1\u00ca\3\2\2\2\u00d1\u00cc")
        buf.write("\3\2\2\2\u00d1\u00ce\3\2\2\2\u00d1\u00cf\3\2\2\2\u00d2")
        buf.write("\t\3\2\2\2\u00d3\u00d7\7I\2\2\u00d4\u00d7\7?\2\2\u00d5")
        buf.write("\u00d7\5\26\f\2\u00d6\u00d3\3\2\2\2\u00d6\u00d4\3\2\2")
        buf.write("\2\u00d6\u00d5\3\2\2\2\u00d7\13\3\2\2\2\u00d8\u00dd\7")
        buf.write("\7\2\2\u00d9\u00dd\7\b\2\2\u00da\u00dd\7\t\2\2\u00db\u00dd")
        buf.write("\7\n\2\2\u00dc\u00d8\3\2\2\2\u00dc\u00d9\3\2\2\2\u00dc")
        buf.write("\u00da\3\2\2\2\u00dc\u00db\3\2\2\2\u00dd\r\3\2\2\2\u00de")
        buf.write("\u00df\79\2\2\u00df\u00e0\5\22\n\2\u00e0\u00e1\78\2\2")
        buf.write("\u00e1\17\3\2\2\2\u00e2\u00e3\79\2\2\u00e3\u00e4\7?\2")
        buf.write("\2\u00e4\u00e5\78\2\2\u00e5\21\3\2\2\2\u00e6\u00e7\t\2")
        buf.write("\2\2\u00e7\23\3\2\2\2\u00e8\u00e9\b\13\1\2\u00e9\u00ea")
        buf.write("\7;\2\2\u00ea\u00eb\5\24\13\2\u00eb\u00ec\7:\2\2\u00ec")
        buf.write("\u00f1\3\2\2\2\u00ed\u00f1\7?\2\2\u00ee\u00f1\7I\2\2\u00ef")
        buf.write("\u00f1\5\26\f\2\u00f0\u00e8\3\2\2\2\u00f0\u00ed\3\2\2")
        buf.write("\2\u00f0\u00ee\3\2\2\2\u00f0\u00ef\3\2\2\2\u00f1\u00fd")
        buf.write("\3\2\2\2\u00f2\u00f3\f\t\2\2\u00f3\u00f4\7\60\2\2\u00f4")
        buf.write("\u00fc\5\24\13\t\u00f5\u00f6\f\b\2\2\u00f6\u00f7\t\3\2")
        buf.write("\2\u00f7\u00fc\5\24\13\t\u00f8\u00f9\f\7\2\2\u00f9\u00fa")
        buf.write("\t\4\2\2\u00fa\u00fc\5\24\13\b\u00fb\u00f2\3\2\2\2\u00fb")
        buf.write("\u00f5\3\2\2\2\u00fb\u00f8\3\2\2\2\u00fc\u00ff\3\2\2\2")
        buf.write("\u00fd\u00fb\3\2\2\2\u00fd\u00fe\3\2\2\2\u00fe\25\3\2")
        buf.write("\2\2\u00ff\u00fd\3\2\2\2\u0100\u0101\t\5\2\2\u0101\27")
        buf.write("\3\2\2\2\u0102\u010d\5&\24\2\u0103\u010d\5(\25\2\u0104")
        buf.write("\u010d\5,\27\2\u0105\u010d\5\66\34\2\u0106\u010d\58\35")
        buf.write("\2\u0107\u010d\5*\26\2\u0108\u010d\5 \21\2\u0109\u010d")
        buf.write("\5\"\22\2\u010a\u010d\5$\23\2\u010b\u010d\5\32\16\2\u010c")
        buf.write("\u0102\3\2\2\2\u010c\u0103\3\2\2\2\u010c\u0104\3\2\2\2")
        buf.write("\u010c\u0105\3\2\2\2\u010c\u0106\3\2\2\2\u010c\u0107\3")
        buf.write("\2\2\2\u010c\u0108\3\2\2\2\u010c\u0109\3\2\2\2\u010c\u010a")
        buf.write("\3\2\2\2\u010c\u010b\3\2\2\2\u010d\31\3\2\2\2\u010e\u010f")
        buf.write("\7.\2\2\u010f\u0111\7?\2\2\u0110\u0112\5\36\20\2\u0111")
        buf.write("\u0110\3\2\2\2\u0112\u0113\3\2\2\2\u0113\u0111\3\2\2\2")
        buf.write("\u0113\u0114\3\2\2\2\u0114\33\3\2\2\2\u0115\u0118\5\26")
        buf.write("\f\2\u0116\u0118\7I\2\2\u0117\u0115\3\2\2\2\u0117\u0116")
        buf.write("\3\2\2\2\u0118\35\3\2\2\2\u0119\u011e\5\34\17\2\u011a")
        buf.write("\u011b\7<\2\2\u011b\u011d\5\34\17\2\u011c\u011a\3\2\2")
        buf.write("\2\u011d\u0120\3\2\2\2\u011e\u011c\3\2\2\2\u011e\u011f")
        buf.write("\3\2\2\2\u011f\37\3\2\2\2\u0120\u011e\3\2\2\2\u0121\u0122")
        buf.write("\7+\2\2\u0122!\3\2\2\2\u0123\u0124\7,\2\2\u0124#\3\2\2")
        buf.write("\2\u0125\u0126\7-\2\2\u0126%\3\2\2\2\u0127\u0128\7#\2")
        buf.write("\2\u0128\u0129\7?\2\2\u0129\u012a\5\24\13\2\u012a\'\3")
        buf.write("\2\2\2\u012b\u012c\7!\2\2\u012c\u012d\5\24\13\2\u012d")
        buf.write(")\3\2\2\2\u012e\u012f\7&\2\2\u012f\u0130\7\3\2\2\u0130")
        buf.write("\u0131\7A\2\2\u0131+\3\2\2\2\u0132\u0133\5.\30\2\u0133")
        buf.write("\u0134\5\60\31\2\u0134\u0135\5\62\32\2\u0135\u0137\7\67")
        buf.write("\2\2\u0136\u0138\5\64\33\2\u0137\u0136\3\2\2\2\u0138\u0139")
        buf.write("\3\2\2\2\u0139\u0137\3\2\2\2\u0139\u013a\3\2\2\2\u013a")
        buf.write("\u013b\3\2\2\2\u013b\u013c\7\66\2\2\u013c-\3\2\2\2\u013d")
        buf.write("\u013e\7\"\2\2\u013e/\3\2\2\2\u013f\u0143\5\26\f\2\u0140")
        buf.write("\u0143\7?\2\2\u0141\u0143\7(\2\2\u0142\u013f\3\2\2\2\u0142")
        buf.write("\u0140\3\2\2\2\u0142\u0141\3\2\2\2\u0143\61\3\2\2\2\u0144")
        buf.write("\u0145\7?\2\2\u0145\63\3\2\2\2\u0146\u0147\7?\2\2\u0147")
        buf.write("\u0148\7*\2\2\u0148\u014b\5\24\13\2\u0149\u014a\7\'\2")
        buf.write("\2\u014a\u014c\5\24\13\2\u014b\u0149\3\2\2\2\u014b\u014c")
        buf.write("\3\2\2\2\u014c\65\3\2\2\2\u014d\u014e\7$\2\2\u014e\u014f")
        buf.write("\7H\2\2\u014f\67\3\2\2\2\u0150\u0151\7%\2\2\u0151\u0152")
        buf.write("\7?\2\2\u0152\u0153\5:\36\2\u01539\3\2\2\2\u0154\u0159")
        buf.write("\5<\37\2\u0155\u0156\7<\2\2\u0156\u0158\5<\37\2\u0157")
        buf.write("\u0155\3\2\2\2\u0158\u015b\3\2\2\2\u0159\u0157\3\2\2\2")
        buf.write("\u0159\u015a\3\2\2\2\u015a;\3\2\2\2\u015b\u0159\3\2\2")
        buf.write("\2\u015c\u0160\5\26\f\2\u015d\u0160\7J\2\2\u015e\u0160")
        buf.write("\7I\2\2\u015f\u015c\3\2\2\2\u015f\u015d\3\2\2\2\u015f")
        buf.write("\u015e\3\2\2\2\u0160=\3\2\2\2\26AL\u00b2\u00b7\u00bc\u00d1")
        buf.write("\u00d6\u00dc\u00f0\u00fb\u00fd\u010c\u0113\u0117\u011e")
        buf.write("\u0139\u0142\u014b\u0159\u015f")
        return buf.getvalue()


class AssemblerParser ( Parser ):

    grammarFileName = "Assembler.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "'**'", "'*'", "'/'", "'+'", 
                     "'-'", "'='", "'}'", "'{'", "']'", "'['", "')'", "'('", 
                     "','", "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "'\"'" ]

    symbolicNames = [ "<INVALID>", "GPR", "JR", "JA", "RET", "COND_Z", "COND_NZ", 
                      "COND_S", "COND_NS", "LD", "LDW", "SV", "COPY", "ADD", 
                      "SUB", "CMP", "AND", "OR", "XOR", "MUL", "DIV", "PUSH", 
                      "POP", "CALL", "TX", "RX", "IO", "IO_OFF", "FLAGS", 
                      "SHR", "SHL", "ORG", "STRUCT", "EQUATE", "INCLUDE", 
                      "STRINGDECL", "ALIAS", "FILL", "ORIGIN_NEXT", "STACKPOINTER", 
                      "DECLWORD", "LSTPROGRESS", "LSTSYMTABLE", "LSTDECODING", 
                      "BINDATA", "END", "POW", "SPLAT", "DIVIDE", "PLUS", 
                      "MINUS", "EQUALS", "RBRACE", "LBRACE", "RBRACK", "LBRACK", 
                      "RPAREN", "LPAREN", "SEP", "NL", "LINECOMMENT", "SYMNAME", 
                      "LABEL", "ALIASNAME", "HEXNUM", "BINNUM", "DECNUM", 
                      "DECDIGIT", "HEXDIGIT", "BINDIGIT", "FILENAME", "CHAR", 
                      "StringLiteral", "UnterminatedStringLiteral", "DQ", 
                      "WS", "ERRORCHARACTER" ]

    RULE_program = 0
    RULE_end = 1
    RULE_block = 2
    RULE_instruction = 3
    RULE_ioconstant = 4
    RULE_flagcond = 5
    RULE_gprptr = 6
    RULE_symptr = 7
    RULE_gpr = 8
    RULE_expr = 9
    RULE_number = 10
    RULE_directive = 11
    RULE_bindata = 12
    RULE_bindataItem = 13
    RULE_bindataList = 14
    RULE_printEachLine = 15
    RULE_printSymbolTable = 16
    RULE_printDecoding = 17
    RULE_equate = 18
    RULE_org = 19
    RULE_alias = 20
    RULE_struct = 21
    RULE_structdecl = 22
    RULE_structorigin = 23
    RULE_structname = 24
    RULE_vardecl = 25
    RULE_include = 26
    RULE_defstring = 27
    RULE_stringpartcollection = 28
    RULE_stringpart = 29

    ruleNames =  [ "program", "end", "block", "instruction", "ioconstant", 
                   "flagcond", "gprptr", "symptr", "gpr", "expr", "number", 
                   "directive", "bindata", "bindataItem", "bindataList", 
                   "printEachLine", "printSymbolTable", "printDecoding", 
                   "equate", "org", "alias", "struct", "structdecl", "structorigin", 
                   "structname", "vardecl", "include", "defstring", "stringpartcollection", 
                   "stringpart" ]

    EOF = Token.EOF
    GPR=1
    JR=2
    JA=3
    RET=4
    COND_Z=5
    COND_NZ=6
    COND_S=7
    COND_NS=8
    LD=9
    LDW=10
    SV=11
    COPY=12
    ADD=13
    SUB=14
    CMP=15
    AND=16
    OR=17
    XOR=18
    MUL=19
    DIV=20
    PUSH=21
    POP=22
    CALL=23
    TX=24
    RX=25
    IO=26
    IO_OFF=27
    FLAGS=28
    SHR=29
    SHL=30
    ORG=31
    STRUCT=32
    EQUATE=33
    INCLUDE=34
    STRINGDECL=35
    ALIAS=36
    FILL=37
    ORIGIN_NEXT=38
    STACKPOINTER=39
    DECLWORD=40
    LSTPROGRESS=41
    LSTSYMTABLE=42
    LSTDECODING=43
    BINDATA=44
    END=45
    POW=46
    SPLAT=47
    DIVIDE=48
    PLUS=49
    MINUS=50
    EQUALS=51
    RBRACE=52
    LBRACE=53
    RBRACK=54
    LBRACK=55
    RPAREN=56
    LPAREN=57
    SEP=58
    NL=59
    LINECOMMENT=60
    SYMNAME=61
    LABEL=62
    ALIASNAME=63
    HEXNUM=64
    BINNUM=65
    DECNUM=66
    DECDIGIT=67
    HEXDIGIT=68
    BINDIGIT=69
    FILENAME=70
    CHAR=71
    StringLiteral=72
    UnterminatedStringLiteral=73
    DQ=74
    WS=75
    ERRORCHARACTER=76

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.8")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ProgramContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def end(self):
            return self.getTypedRuleContext(AssemblerParser.EndContext,0)


        def EOF(self):
            return self.getToken(AssemblerParser.EOF, 0)

        def block(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AssemblerParser.BlockContext)
            else:
                return self.getTypedRuleContext(AssemblerParser.BlockContext,i)


        def getRuleIndex(self):
            return AssemblerParser.RULE_program

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProgram" ):
                return visitor.visitProgram(self)
            else:
                return visitor.visitChildren(self)




    def program(self):

        localctx = AssemblerParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 63
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << AssemblerParser.JR) | (1 << AssemblerParser.JA) | (1 << AssemblerParser.RET) | (1 << AssemblerParser.LD) | (1 << AssemblerParser.LDW) | (1 << AssemblerParser.SV) | (1 << AssemblerParser.COPY) | (1 << AssemblerParser.ADD) | (1 << AssemblerParser.SUB) | (1 << AssemblerParser.CMP) | (1 << AssemblerParser.MUL) | (1 << AssemblerParser.DIV) | (1 << AssemblerParser.PUSH) | (1 << AssemblerParser.POP) | (1 << AssemblerParser.CALL) | (1 << AssemblerParser.TX) | (1 << AssemblerParser.RX) | (1 << AssemblerParser.IO) | (1 << AssemblerParser.IO_OFF) | (1 << AssemblerParser.ORG) | (1 << AssemblerParser.STRUCT) | (1 << AssemblerParser.EQUATE) | (1 << AssemblerParser.INCLUDE) | (1 << AssemblerParser.STRINGDECL) | (1 << AssemblerParser.ALIAS) | (1 << AssemblerParser.LSTPROGRESS) | (1 << AssemblerParser.LSTSYMTABLE) | (1 << AssemblerParser.LSTDECODING) | (1 << AssemblerParser.BINDATA) | (1 << AssemblerParser.LABEL))) != 0):
                self.state = 60
                self.block()
                self.state = 65
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 66
            self.end()
            self.state = 67
            self.match(AssemblerParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class EndContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def END(self):
            return self.getToken(AssemblerParser.END, 0)

        def getRuleIndex(self):
            return AssemblerParser.RULE_end

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEnd" ):
                return visitor.visitEnd(self)
            else:
                return visitor.visitChildren(self)




    def end(self):

        localctx = AssemblerParser.EndContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_end)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 69
            self.match(AssemblerParser.END)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class BlockContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return AssemblerParser.RULE_block

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class BlockInstructionContext(BlockContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.BlockContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def instruction(self):
            return self.getTypedRuleContext(AssemblerParser.InstructionContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBlockInstruction" ):
                return visitor.visitBlockInstruction(self)
            else:
                return visitor.visitChildren(self)


    class BlockDirectiveContext(BlockContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.BlockContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def directive(self):
            return self.getTypedRuleContext(AssemblerParser.DirectiveContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBlockDirective" ):
                return visitor.visitBlockDirective(self)
            else:
                return visitor.visitChildren(self)


    class BlockLabelContext(BlockContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.BlockContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def LABEL(self):
            return self.getToken(AssemblerParser.LABEL, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBlockLabel" ):
                return visitor.visitBlockLabel(self)
            else:
                return visitor.visitChildren(self)



    def block(self):

        localctx = AssemblerParser.BlockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_block)
        try:
            self.state = 74
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [AssemblerParser.ORG, AssemblerParser.STRUCT, AssemblerParser.EQUATE, AssemblerParser.INCLUDE, AssemblerParser.STRINGDECL, AssemblerParser.ALIAS, AssemblerParser.LSTPROGRESS, AssemblerParser.LSTSYMTABLE, AssemblerParser.LSTDECODING, AssemblerParser.BINDATA]:
                localctx = AssemblerParser.BlockDirectiveContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 71
                self.directive()
                pass
            elif token in [AssemblerParser.LABEL]:
                localctx = AssemblerParser.BlockLabelContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 72
                self.match(AssemblerParser.LABEL)
                pass
            elif token in [AssemblerParser.JR, AssemblerParser.JA, AssemblerParser.RET, AssemblerParser.LD, AssemblerParser.LDW, AssemblerParser.SV, AssemblerParser.COPY, AssemblerParser.ADD, AssemblerParser.SUB, AssemblerParser.CMP, AssemblerParser.MUL, AssemblerParser.DIV, AssemblerParser.PUSH, AssemblerParser.POP, AssemblerParser.CALL, AssemblerParser.TX, AssemblerParser.RX, AssemblerParser.IO, AssemblerParser.IO_OFF]:
                localctx = AssemblerParser.BlockInstructionContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 73
                self.instruction()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class InstructionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return AssemblerParser.RULE_instruction

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class I_tx_gprContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def TX(self):
            return self.getToken(AssemblerParser.TX, 0)
        def gpr(self):
            return self.getTypedRuleContext(AssemblerParser.GprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_tx_gpr" ):
                return visitor.visitI_tx_gpr(self)
            else:
                return visitor.visitChildren(self)


    class I_ld_gpr_tContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def LD(self):
            return self.getToken(AssemblerParser.LD, 0)
        def gpr(self):
            return self.getTypedRuleContext(AssemblerParser.GprContext,0)

        def SEP(self):
            return self.getToken(AssemblerParser.SEP, 0)
        def expr(self):
            return self.getTypedRuleContext(AssemblerParser.ExprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_ld_gpr_t" ):
                return visitor.visitI_ld_gpr_t(self)
            else:
                return visitor.visitChildren(self)


    class I_ld_gpr_sptrContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def LD(self):
            return self.getToken(AssemblerParser.LD, 0)
        def gpr(self):
            return self.getTypedRuleContext(AssemblerParser.GprContext,0)

        def SEP(self):
            return self.getToken(AssemblerParser.SEP, 0)
        def symptr(self):
            return self.getTypedRuleContext(AssemblerParser.SymptrContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_ld_gpr_sptr" ):
                return visitor.visitI_ld_gpr_sptr(self)
            else:
                return visitor.visitChildren(self)


    class I_pop_flagsContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def POP(self):
            return self.getToken(AssemblerParser.POP, 0)
        def FLAGS(self):
            return self.getToken(AssemblerParser.FLAGS, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_pop_flags" ):
                return visitor.visitI_pop_flags(self)
            else:
                return visitor.visitChildren(self)


    class I_push_flagsContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def PUSH(self):
            return self.getToken(AssemblerParser.PUSH, 0)
        def FLAGS(self):
            return self.getToken(AssemblerParser.FLAGS, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_push_flags" ):
                return visitor.visitI_push_flags(self)
            else:
                return visitor.visitChildren(self)


    class I_sv_gptr_gprContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def SV(self):
            return self.getToken(AssemblerParser.SV, 0)
        def gprptr(self):
            return self.getTypedRuleContext(AssemblerParser.GprptrContext,0)

        def SEP(self):
            return self.getToken(AssemblerParser.SEP, 0)
        def gpr(self):
            return self.getTypedRuleContext(AssemblerParser.GprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_sv_gptr_gpr" ):
                return visitor.visitI_sv_gptr_gpr(self)
            else:
                return visitor.visitChildren(self)


    class I_ld_gpr_gptrContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def LD(self):
            return self.getToken(AssemblerParser.LD, 0)
        def gpr(self):
            return self.getTypedRuleContext(AssemblerParser.GprContext,0)

        def SEP(self):
            return self.getToken(AssemblerParser.SEP, 0)
        def gprptr(self):
            return self.getTypedRuleContext(AssemblerParser.GprptrContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_ld_gpr_gptr" ):
                return visitor.visitI_ld_gpr_gptr(self)
            else:
                return visitor.visitChildren(self)


    class I_cmp_gpr_gprContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def CMP(self):
            return self.getToken(AssemblerParser.CMP, 0)
        def gpr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AssemblerParser.GprContext)
            else:
                return self.getTypedRuleContext(AssemblerParser.GprContext,i)

        def SEP(self):
            return self.getToken(AssemblerParser.SEP, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_cmp_gpr_gpr" ):
                return visitor.visitI_cmp_gpr_gpr(self)
            else:
                return visitor.visitChildren(self)


    class I_ld_sp_exprContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def LD(self):
            return self.getToken(AssemblerParser.LD, 0)
        def STACKPOINTER(self):
            return self.getToken(AssemblerParser.STACKPOINTER, 0)
        def SEP(self):
            return self.getToken(AssemblerParser.SEP, 0)
        def expr(self):
            return self.getTypedRuleContext(AssemblerParser.ExprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_ld_sp_expr" ):
                return visitor.visitI_ld_sp_expr(self)
            else:
                return visitor.visitChildren(self)


    class I_tx_tContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def TX(self):
            return self.getToken(AssemblerParser.TX, 0)
        def ioconstant(self):
            return self.getTypedRuleContext(AssemblerParser.IoconstantContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_tx_t" ):
                return visitor.visitI_tx_t(self)
            else:
                return visitor.visitChildren(self)


    class I_sub_gpr_gprContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def SUB(self):
            return self.getToken(AssemblerParser.SUB, 0)
        def gpr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AssemblerParser.GprContext)
            else:
                return self.getTypedRuleContext(AssemblerParser.GprContext,i)

        def SEP(self):
            return self.getToken(AssemblerParser.SEP, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_sub_gpr_gpr" ):
                return visitor.visitI_sub_gpr_gpr(self)
            else:
                return visitor.visitChildren(self)


    class I_cmp_gpr_tContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def CMP(self):
            return self.getToken(AssemblerParser.CMP, 0)
        def gpr(self):
            return self.getTypedRuleContext(AssemblerParser.GprContext,0)

        def SEP(self):
            return self.getToken(AssemblerParser.SEP, 0)
        def expr(self):
            return self.getTypedRuleContext(AssemblerParser.ExprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_cmp_gpr_t" ):
                return visitor.visitI_cmp_gpr_t(self)
            else:
                return visitor.visitChildren(self)


    class I_div_gpr_gpr_gprContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def DIV(self):
            return self.getToken(AssemblerParser.DIV, 0)
        def gpr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AssemblerParser.GprContext)
            else:
                return self.getTypedRuleContext(AssemblerParser.GprContext,i)

        def SEP(self, i:int=None):
            if i is None:
                return self.getTokens(AssemblerParser.SEP)
            else:
                return self.getToken(AssemblerParser.SEP, i)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_div_gpr_gpr_gpr" ):
                return visitor.visitI_div_gpr_gpr_gpr(self)
            else:
                return visitor.visitChildren(self)


    class I_ld_gpr_exprContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def LDW(self):
            return self.getToken(AssemblerParser.LDW, 0)
        def gpr(self):
            return self.getTypedRuleContext(AssemblerParser.GprContext,0)

        def SEP(self):
            return self.getToken(AssemblerParser.SEP, 0)
        def expr(self):
            return self.getTypedRuleContext(AssemblerParser.ExprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_ld_gpr_expr" ):
                return visitor.visitI_ld_gpr_expr(self)
            else:
                return visitor.visitChildren(self)


    class I_sub_gpr_tContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def SUB(self):
            return self.getToken(AssemblerParser.SUB, 0)
        def gpr(self):
            return self.getTypedRuleContext(AssemblerParser.GprContext,0)

        def SEP(self):
            return self.getToken(AssemblerParser.SEP, 0)
        def expr(self):
            return self.getTypedRuleContext(AssemblerParser.ExprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_sub_gpr_t" ):
                return visitor.visitI_sub_gpr_t(self)
            else:
                return visitor.visitChildren(self)


    class I_rx_gprContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def RX(self):
            return self.getToken(AssemblerParser.RX, 0)
        def gpr(self):
            return self.getTypedRuleContext(AssemblerParser.GprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_rx_gpr" ):
                return visitor.visitI_rx_gpr(self)
            else:
                return visitor.visitChildren(self)


    class I_sv_expr_gprContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def SV(self):
            return self.getToken(AssemblerParser.SV, 0)
        def expr(self):
            return self.getTypedRuleContext(AssemblerParser.ExprContext,0)

        def SEP(self):
            return self.getToken(AssemblerParser.SEP, 0)
        def gpr(self):
            return self.getTypedRuleContext(AssemblerParser.GprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_sv_expr_gpr" ):
                return visitor.visitI_sv_expr_gpr(self)
            else:
                return visitor.visitChildren(self)


    class I_add_gpr_gprContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ADD(self):
            return self.getToken(AssemblerParser.ADD, 0)
        def gpr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AssemblerParser.GprContext)
            else:
                return self.getTypedRuleContext(AssemblerParser.GprContext,i)

        def SEP(self):
            return self.getToken(AssemblerParser.SEP, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_add_gpr_gpr" ):
                return visitor.visitI_add_gpr_gpr(self)
            else:
                return visitor.visitChildren(self)


    class I_callContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def CALL(self):
            return self.getToken(AssemblerParser.CALL, 0)
        def SYMNAME(self):
            return self.getToken(AssemblerParser.SYMNAME, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_call" ):
                return visitor.visitI_call(self)
            else:
                return visitor.visitChildren(self)


    class I_sv_gptr_exprContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def SV(self):
            return self.getToken(AssemblerParser.SV, 0)
        def gprptr(self):
            return self.getTypedRuleContext(AssemblerParser.GprptrContext,0)

        def SEP(self):
            return self.getToken(AssemblerParser.SEP, 0)
        def expr(self):
            return self.getTypedRuleContext(AssemblerParser.ExprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_sv_gptr_expr" ):
                return visitor.visitI_sv_gptr_expr(self)
            else:
                return visitor.visitChildren(self)


    class I_sv_sptr_gprContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def SV(self):
            return self.getToken(AssemblerParser.SV, 0)
        def symptr(self):
            return self.getTypedRuleContext(AssemblerParser.SymptrContext,0)

        def SEP(self):
            return self.getToken(AssemblerParser.SEP, 0)
        def gpr(self):
            return self.getTypedRuleContext(AssemblerParser.GprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_sv_sptr_gpr" ):
                return visitor.visitI_sv_sptr_gpr(self)
            else:
                return visitor.visitChildren(self)


    class I_io_gprContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def IO(self):
            return self.getToken(AssemblerParser.IO, 0)
        def gpr(self):
            return self.getTypedRuleContext(AssemblerParser.GprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_io_gpr" ):
                return visitor.visitI_io_gpr(self)
            else:
                return visitor.visitChildren(self)


    class I_io_tContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def IO(self):
            return self.getToken(AssemblerParser.IO, 0)
        def ioconstant(self):
            return self.getTypedRuleContext(AssemblerParser.IoconstantContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_io_t" ):
                return visitor.visitI_io_t(self)
            else:
                return visitor.visitChildren(self)


    class I_io_offContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def IO_OFF(self):
            return self.getToken(AssemblerParser.IO_OFF, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_io_off" ):
                return visitor.visitI_io_off(self)
            else:
                return visitor.visitChildren(self)


    class I_copy_gpr_gprContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def COPY(self):
            return self.getToken(AssemblerParser.COPY, 0)
        def gpr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AssemblerParser.GprContext)
            else:
                return self.getTypedRuleContext(AssemblerParser.GprContext,i)

        def SEP(self):
            return self.getToken(AssemblerParser.SEP, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_copy_gpr_gpr" ):
                return visitor.visitI_copy_gpr_gpr(self)
            else:
                return visitor.visitChildren(self)


    class I_jrContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def JR(self):
            return self.getToken(AssemblerParser.JR, 0)
        def SYMNAME(self):
            return self.getToken(AssemblerParser.SYMNAME, 0)
        def flagcond(self):
            return self.getTypedRuleContext(AssemblerParser.FlagcondContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_jr" ):
                return visitor.visitI_jr(self)
            else:
                return visitor.visitChildren(self)


    class I_mul_gpr_tContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def MUL(self):
            return self.getToken(AssemblerParser.MUL, 0)
        def gpr(self):
            return self.getTypedRuleContext(AssemblerParser.GprContext,0)

        def SEP(self):
            return self.getToken(AssemblerParser.SEP, 0)
        def expr(self):
            return self.getTypedRuleContext(AssemblerParser.ExprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_mul_gpr_t" ):
                return visitor.visitI_mul_gpr_t(self)
            else:
                return visitor.visitChildren(self)


    class I_add_gpr_tContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ADD(self):
            return self.getToken(AssemblerParser.ADD, 0)
        def gpr(self):
            return self.getTypedRuleContext(AssemblerParser.GprContext,0)

        def SEP(self):
            return self.getToken(AssemblerParser.SEP, 0)
        def expr(self):
            return self.getTypedRuleContext(AssemblerParser.ExprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_add_gpr_t" ):
                return visitor.visitI_add_gpr_t(self)
            else:
                return visitor.visitChildren(self)


    class I_pop_gprContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def POP(self):
            return self.getToken(AssemblerParser.POP, 0)
        def gpr(self):
            return self.getTypedRuleContext(AssemblerParser.GprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_pop_gpr" ):
                return visitor.visitI_pop_gpr(self)
            else:
                return visitor.visitChildren(self)


    class I_jaContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def JA(self):
            return self.getToken(AssemblerParser.JA, 0)
        def SYMNAME(self):
            return self.getToken(AssemblerParser.SYMNAME, 0)
        def flagcond(self):
            return self.getTypedRuleContext(AssemblerParser.FlagcondContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_ja" ):
                return visitor.visitI_ja(self)
            else:
                return visitor.visitChildren(self)


    class I_push_gprContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def PUSH(self):
            return self.getToken(AssemblerParser.PUSH, 0)
        def gpr(self):
            return self.getTypedRuleContext(AssemblerParser.GprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_push_gpr" ):
                return visitor.visitI_push_gpr(self)
            else:
                return visitor.visitChildren(self)


    class I_mul_gpr_gprContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def MUL(self):
            return self.getToken(AssemblerParser.MUL, 0)
        def gpr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AssemblerParser.GprContext)
            else:
                return self.getTypedRuleContext(AssemblerParser.GprContext,i)

        def SEP(self):
            return self.getToken(AssemblerParser.SEP, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_mul_gpr_gpr" ):
                return visitor.visitI_mul_gpr_gpr(self)
            else:
                return visitor.visitChildren(self)


    class I_retContext(InstructionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.InstructionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def RET(self):
            return self.getToken(AssemblerParser.RET, 0)
        def flagcond(self):
            return self.getTypedRuleContext(AssemblerParser.FlagcondContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitI_ret" ):
                return visitor.visitI_ret(self)
            else:
                return visitor.visitChildren(self)



    def instruction(self):

        localctx = AssemblerParser.InstructionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_instruction)
        self._la = 0 # Token type
        try:
            self.state = 207
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,5,self._ctx)
            if la_ == 1:
                localctx = AssemblerParser.I_ld_gpr_exprContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 76
                self.match(AssemblerParser.LDW)
                self.state = 77
                self.gpr()
                self.state = 78
                self.match(AssemblerParser.SEP)
                self.state = 79
                self.expr(0)
                pass

            elif la_ == 2:
                localctx = AssemblerParser.I_ld_gpr_tContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 81
                self.match(AssemblerParser.LD)
                self.state = 82
                self.gpr()
                self.state = 83
                self.match(AssemblerParser.SEP)
                self.state = 84
                self.expr(0)
                pass

            elif la_ == 3:
                localctx = AssemblerParser.I_ld_gpr_gptrContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 86
                self.match(AssemblerParser.LD)
                self.state = 87
                self.gpr()
                self.state = 88
                self.match(AssemblerParser.SEP)
                self.state = 89
                self.gprptr()
                pass

            elif la_ == 4:
                localctx = AssemblerParser.I_ld_gpr_sptrContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 91
                self.match(AssemblerParser.LD)
                self.state = 92
                self.gpr()
                self.state = 93
                self.match(AssemblerParser.SEP)
                self.state = 94
                self.symptr()
                pass

            elif la_ == 5:
                localctx = AssemblerParser.I_copy_gpr_gprContext(self, localctx)
                self.enterOuterAlt(localctx, 5)
                self.state = 96
                self.match(AssemblerParser.COPY)
                self.state = 97
                self.gpr()
                self.state = 98
                self.match(AssemblerParser.SEP)
                self.state = 99
                self.gpr()
                pass

            elif la_ == 6:
                localctx = AssemblerParser.I_ld_sp_exprContext(self, localctx)
                self.enterOuterAlt(localctx, 6)
                self.state = 101
                self.match(AssemblerParser.LD)
                self.state = 102
                self.match(AssemblerParser.STACKPOINTER)
                self.state = 103
                self.match(AssemblerParser.SEP)
                self.state = 104
                self.expr(0)
                pass

            elif la_ == 7:
                localctx = AssemblerParser.I_sv_expr_gprContext(self, localctx)
                self.enterOuterAlt(localctx, 7)
                self.state = 105
                self.match(AssemblerParser.SV)
                self.state = 106
                self.expr(0)
                self.state = 107
                self.match(AssemblerParser.SEP)
                self.state = 108
                self.gpr()
                pass

            elif la_ == 8:
                localctx = AssemblerParser.I_sv_gptr_gprContext(self, localctx)
                self.enterOuterAlt(localctx, 8)
                self.state = 110
                self.match(AssemblerParser.SV)
                self.state = 111
                self.gprptr()
                self.state = 112
                self.match(AssemblerParser.SEP)
                self.state = 113
                self.gpr()
                pass

            elif la_ == 9:
                localctx = AssemblerParser.I_sv_sptr_gprContext(self, localctx)
                self.enterOuterAlt(localctx, 9)
                self.state = 115
                self.match(AssemblerParser.SV)
                self.state = 116
                self.symptr()
                self.state = 117
                self.match(AssemblerParser.SEP)
                self.state = 118
                self.gpr()
                pass

            elif la_ == 10:
                localctx = AssemblerParser.I_sv_gptr_exprContext(self, localctx)
                self.enterOuterAlt(localctx, 10)
                self.state = 120
                self.match(AssemblerParser.SV)
                self.state = 121
                self.gprptr()
                self.state = 122
                self.match(AssemblerParser.SEP)
                self.state = 123
                self.expr(0)
                pass

            elif la_ == 11:
                localctx = AssemblerParser.I_callContext(self, localctx)
                self.enterOuterAlt(localctx, 11)
                self.state = 125
                self.match(AssemblerParser.CALL)
                self.state = 126
                self.match(AssemblerParser.SYMNAME)
                pass

            elif la_ == 12:
                localctx = AssemblerParser.I_add_gpr_tContext(self, localctx)
                self.enterOuterAlt(localctx, 12)
                self.state = 127
                self.match(AssemblerParser.ADD)
                self.state = 128
                self.gpr()
                self.state = 129
                self.match(AssemblerParser.SEP)
                self.state = 130
                self.expr(0)
                pass

            elif la_ == 13:
                localctx = AssemblerParser.I_add_gpr_gprContext(self, localctx)
                self.enterOuterAlt(localctx, 13)
                self.state = 132
                self.match(AssemblerParser.ADD)
                self.state = 133
                self.gpr()
                self.state = 134
                self.match(AssemblerParser.SEP)
                self.state = 135
                self.gpr()
                pass

            elif la_ == 14:
                localctx = AssemblerParser.I_sub_gpr_tContext(self, localctx)
                self.enterOuterAlt(localctx, 14)
                self.state = 137
                self.match(AssemblerParser.SUB)
                self.state = 138
                self.gpr()
                self.state = 139
                self.match(AssemblerParser.SEP)
                self.state = 140
                self.expr(0)
                pass

            elif la_ == 15:
                localctx = AssemblerParser.I_sub_gpr_gprContext(self, localctx)
                self.enterOuterAlt(localctx, 15)
                self.state = 142
                self.match(AssemblerParser.SUB)
                self.state = 143
                self.gpr()
                self.state = 144
                self.match(AssemblerParser.SEP)
                self.state = 145
                self.gpr()
                pass

            elif la_ == 16:
                localctx = AssemblerParser.I_cmp_gpr_tContext(self, localctx)
                self.enterOuterAlt(localctx, 16)
                self.state = 147
                self.match(AssemblerParser.CMP)
                self.state = 148
                self.gpr()
                self.state = 149
                self.match(AssemblerParser.SEP)
                self.state = 150
                self.expr(0)
                pass

            elif la_ == 17:
                localctx = AssemblerParser.I_cmp_gpr_gprContext(self, localctx)
                self.enterOuterAlt(localctx, 17)
                self.state = 152
                self.match(AssemblerParser.CMP)
                self.state = 153
                self.gpr()
                self.state = 154
                self.match(AssemblerParser.SEP)
                self.state = 155
                self.gpr()
                pass

            elif la_ == 18:
                localctx = AssemblerParser.I_mul_gpr_tContext(self, localctx)
                self.enterOuterAlt(localctx, 18)
                self.state = 157
                self.match(AssemblerParser.MUL)
                self.state = 158
                self.gpr()
                self.state = 159
                self.match(AssemblerParser.SEP)
                self.state = 160
                self.expr(0)
                pass

            elif la_ == 19:
                localctx = AssemblerParser.I_mul_gpr_gprContext(self, localctx)
                self.enterOuterAlt(localctx, 19)
                self.state = 162
                self.match(AssemblerParser.MUL)
                self.state = 163
                self.gpr()
                self.state = 164
                self.match(AssemblerParser.SEP)
                self.state = 165
                self.gpr()
                pass

            elif la_ == 20:
                localctx = AssemblerParser.I_div_gpr_gpr_gprContext(self, localctx)
                self.enterOuterAlt(localctx, 20)
                self.state = 167
                self.match(AssemblerParser.DIV)
                self.state = 168
                self.gpr()
                self.state = 169
                self.match(AssemblerParser.SEP)
                self.state = 170
                self.gpr()
                self.state = 171
                self.match(AssemblerParser.SEP)
                self.state = 172
                self.gpr()
                pass

            elif la_ == 21:
                localctx = AssemblerParser.I_jrContext(self, localctx)
                self.enterOuterAlt(localctx, 21)
                self.state = 174
                self.match(AssemblerParser.JR)
                self.state = 176
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << AssemblerParser.COND_Z) | (1 << AssemblerParser.COND_NZ) | (1 << AssemblerParser.COND_S) | (1 << AssemblerParser.COND_NS))) != 0):
                    self.state = 175
                    self.flagcond()


                self.state = 178
                self.match(AssemblerParser.SYMNAME)
                pass

            elif la_ == 22:
                localctx = AssemblerParser.I_jaContext(self, localctx)
                self.enterOuterAlt(localctx, 22)
                self.state = 179
                self.match(AssemblerParser.JA)
                self.state = 181
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << AssemblerParser.COND_Z) | (1 << AssemblerParser.COND_NZ) | (1 << AssemblerParser.COND_S) | (1 << AssemblerParser.COND_NS))) != 0):
                    self.state = 180
                    self.flagcond()


                self.state = 183
                self.match(AssemblerParser.SYMNAME)
                pass

            elif la_ == 23:
                localctx = AssemblerParser.I_retContext(self, localctx)
                self.enterOuterAlt(localctx, 23)
                self.state = 184
                self.match(AssemblerParser.RET)
                self.state = 186
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << AssemblerParser.COND_Z) | (1 << AssemblerParser.COND_NZ) | (1 << AssemblerParser.COND_S) | (1 << AssemblerParser.COND_NS))) != 0):
                    self.state = 185
                    self.flagcond()


                pass

            elif la_ == 24:
                localctx = AssemblerParser.I_push_flagsContext(self, localctx)
                self.enterOuterAlt(localctx, 24)
                self.state = 188
                self.match(AssemblerParser.PUSH)
                self.state = 189
                self.match(AssemblerParser.FLAGS)
                pass

            elif la_ == 25:
                localctx = AssemblerParser.I_push_gprContext(self, localctx)
                self.enterOuterAlt(localctx, 25)
                self.state = 190
                self.match(AssemblerParser.PUSH)
                self.state = 191
                self.gpr()
                pass

            elif la_ == 26:
                localctx = AssemblerParser.I_pop_flagsContext(self, localctx)
                self.enterOuterAlt(localctx, 26)
                self.state = 192
                self.match(AssemblerParser.POP)
                self.state = 193
                self.match(AssemblerParser.FLAGS)
                pass

            elif la_ == 27:
                localctx = AssemblerParser.I_pop_gprContext(self, localctx)
                self.enterOuterAlt(localctx, 27)
                self.state = 194
                self.match(AssemblerParser.POP)
                self.state = 195
                self.gpr()
                pass

            elif la_ == 28:
                localctx = AssemblerParser.I_tx_gprContext(self, localctx)
                self.enterOuterAlt(localctx, 28)
                self.state = 196
                self.match(AssemblerParser.TX)
                self.state = 197
                self.gpr()
                pass

            elif la_ == 29:
                localctx = AssemblerParser.I_io_gprContext(self, localctx)
                self.enterOuterAlt(localctx, 29)
                self.state = 198
                self.match(AssemblerParser.IO)
                self.state = 199
                self.gpr()
                pass

            elif la_ == 30:
                localctx = AssemblerParser.I_tx_tContext(self, localctx)
                self.enterOuterAlt(localctx, 30)
                self.state = 200
                self.match(AssemblerParser.TX)
                self.state = 201
                self.ioconstant()
                pass

            elif la_ == 31:
                localctx = AssemblerParser.I_io_tContext(self, localctx)
                self.enterOuterAlt(localctx, 31)
                self.state = 202
                self.match(AssemblerParser.IO)
                self.state = 203
                self.ioconstant()
                pass

            elif la_ == 32:
                localctx = AssemblerParser.I_io_offContext(self, localctx)
                self.enterOuterAlt(localctx, 32)
                self.state = 204
                self.match(AssemblerParser.IO_OFF)
                pass

            elif la_ == 33:
                localctx = AssemblerParser.I_rx_gprContext(self, localctx)
                self.enterOuterAlt(localctx, 33)
                self.state = 205
                self.match(AssemblerParser.RX)
                self.state = 206
                self.gpr()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class IoconstantContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CHAR(self):
            return self.getToken(AssemblerParser.CHAR, 0)

        def SYMNAME(self):
            return self.getToken(AssemblerParser.SYMNAME, 0)

        def number(self):
            return self.getTypedRuleContext(AssemblerParser.NumberContext,0)


        def getRuleIndex(self):
            return AssemblerParser.RULE_ioconstant

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIoconstant" ):
                return visitor.visitIoconstant(self)
            else:
                return visitor.visitChildren(self)




    def ioconstant(self):

        localctx = AssemblerParser.IoconstantContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_ioconstant)
        try:
            self.state = 212
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [AssemblerParser.CHAR]:
                self.enterOuterAlt(localctx, 1)
                self.state = 209
                self.match(AssemblerParser.CHAR)
                pass
            elif token in [AssemblerParser.SYMNAME]:
                self.enterOuterAlt(localctx, 2)
                self.state = 210
                self.match(AssemblerParser.SYMNAME)
                pass
            elif token in [AssemblerParser.HEXNUM, AssemblerParser.BINNUM, AssemblerParser.DECNUM]:
                self.enterOuterAlt(localctx, 3)
                self.state = 211
                self.number()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FlagcondContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return AssemblerParser.RULE_flagcond

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class Flagcond_sContext(FlagcondContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.FlagcondContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def COND_S(self):
            return self.getToken(AssemblerParser.COND_S, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFlagcond_s" ):
                return visitor.visitFlagcond_s(self)
            else:
                return visitor.visitChildren(self)


    class Flagcond_nsContext(FlagcondContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.FlagcondContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def COND_NS(self):
            return self.getToken(AssemblerParser.COND_NS, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFlagcond_ns" ):
                return visitor.visitFlagcond_ns(self)
            else:
                return visitor.visitChildren(self)


    class Flagcond_zContext(FlagcondContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.FlagcondContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def COND_Z(self):
            return self.getToken(AssemblerParser.COND_Z, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFlagcond_z" ):
                return visitor.visitFlagcond_z(self)
            else:
                return visitor.visitChildren(self)


    class Flagcond_nzContext(FlagcondContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.FlagcondContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def COND_NZ(self):
            return self.getToken(AssemblerParser.COND_NZ, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFlagcond_nz" ):
                return visitor.visitFlagcond_nz(self)
            else:
                return visitor.visitChildren(self)



    def flagcond(self):

        localctx = AssemblerParser.FlagcondContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_flagcond)
        try:
            self.state = 218
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [AssemblerParser.COND_Z]:
                localctx = AssemblerParser.Flagcond_zContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 214
                self.match(AssemblerParser.COND_Z)
                pass
            elif token in [AssemblerParser.COND_NZ]:
                localctx = AssemblerParser.Flagcond_nzContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 215
                self.match(AssemblerParser.COND_NZ)
                pass
            elif token in [AssemblerParser.COND_S]:
                localctx = AssemblerParser.Flagcond_sContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 216
                self.match(AssemblerParser.COND_S)
                pass
            elif token in [AssemblerParser.COND_NS]:
                localctx = AssemblerParser.Flagcond_nsContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 217
                self.match(AssemblerParser.COND_NS)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class GprptrContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LBRACK(self):
            return self.getToken(AssemblerParser.LBRACK, 0)

        def gpr(self):
            return self.getTypedRuleContext(AssemblerParser.GprContext,0)


        def RBRACK(self):
            return self.getToken(AssemblerParser.RBRACK, 0)

        def getRuleIndex(self):
            return AssemblerParser.RULE_gprptr

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGprptr" ):
                return visitor.visitGprptr(self)
            else:
                return visitor.visitChildren(self)




    def gprptr(self):

        localctx = AssemblerParser.GprptrContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_gprptr)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 220
            self.match(AssemblerParser.LBRACK)
            self.state = 221
            self.gpr()
            self.state = 222
            self.match(AssemblerParser.RBRACK)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SymptrContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LBRACK(self):
            return self.getToken(AssemblerParser.LBRACK, 0)

        def SYMNAME(self):
            return self.getToken(AssemblerParser.SYMNAME, 0)

        def RBRACK(self):
            return self.getToken(AssemblerParser.RBRACK, 0)

        def getRuleIndex(self):
            return AssemblerParser.RULE_symptr

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSymptr" ):
                return visitor.visitSymptr(self)
            else:
                return visitor.visitChildren(self)




    def symptr(self):

        localctx = AssemblerParser.SymptrContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_symptr)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 224
            self.match(AssemblerParser.LBRACK)
            self.state = 225
            self.match(AssemblerParser.SYMNAME)
            self.state = 226
            self.match(AssemblerParser.RBRACK)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class GprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def GPR(self):
            return self.getToken(AssemblerParser.GPR, 0)

        def ALIASNAME(self):
            return self.getToken(AssemblerParser.ALIASNAME, 0)

        def getRuleIndex(self):
            return AssemblerParser.RULE_gpr

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGpr" ):
                return visitor.visitGpr(self)
            else:
                return visitor.visitChildren(self)




    def gpr(self):

        localctx = AssemblerParser.GprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_gpr)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 228
            _la = self._input.LA(1)
            if not(_la==AssemblerParser.GPR or _la==AssemblerParser.ALIASNAME):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return AssemblerParser.RULE_expr

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class CharExprContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def CHAR(self):
            return self.getToken(AssemblerParser.CHAR, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCharExpr" ):
                return visitor.visitCharExpr(self)
            else:
                return visitor.visitChildren(self)


    class PowerExprContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AssemblerParser.ExprContext)
            else:
                return self.getTypedRuleContext(AssemblerParser.ExprContext,i)

        def POW(self):
            return self.getToken(AssemblerParser.POW, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPowerExpr" ):
                return visitor.visitPowerExpr(self)
            else:
                return visitor.visitChildren(self)


    class NumberExprContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def number(self):
            return self.getTypedRuleContext(AssemblerParser.NumberContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNumberExpr" ):
                return visitor.visitNumberExpr(self)
            else:
                return visitor.visitChildren(self)


    class PlusMinusExprContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AssemblerParser.ExprContext)
            else:
                return self.getTypedRuleContext(AssemblerParser.ExprContext,i)

        def PLUS(self):
            return self.getToken(AssemblerParser.PLUS, 0)
        def MINUS(self):
            return self.getToken(AssemblerParser.MINUS, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPlusMinusExpr" ):
                return visitor.visitPlusMinusExpr(self)
            else:
                return visitor.visitChildren(self)


    class MultDivExprContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AssemblerParser.ExprContext)
            else:
                return self.getTypedRuleContext(AssemblerParser.ExprContext,i)

        def SPLAT(self):
            return self.getToken(AssemblerParser.SPLAT, 0)
        def DIVIDE(self):
            return self.getToken(AssemblerParser.DIVIDE, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMultDivExpr" ):
                return visitor.visitMultDivExpr(self)
            else:
                return visitor.visitChildren(self)


    class SymbolExprContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def SYMNAME(self):
            return self.getToken(AssemblerParser.SYMNAME, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSymbolExpr" ):
                return visitor.visitSymbolExpr(self)
            else:
                return visitor.visitChildren(self)


    class ParenExprContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def LPAREN(self):
            return self.getToken(AssemblerParser.LPAREN, 0)
        def expr(self):
            return self.getTypedRuleContext(AssemblerParser.ExprContext,0)

        def RPAREN(self):
            return self.getToken(AssemblerParser.RPAREN, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParenExpr" ):
                return visitor.visitParenExpr(self)
            else:
                return visitor.visitChildren(self)



    def expr(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = AssemblerParser.ExprContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 18
        self.enterRecursionRule(localctx, 18, self.RULE_expr, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 238
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [AssemblerParser.LPAREN]:
                localctx = AssemblerParser.ParenExprContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx

                self.state = 231
                self.match(AssemblerParser.LPAREN)
                self.state = 232
                self.expr(0)
                self.state = 233
                self.match(AssemblerParser.RPAREN)
                pass
            elif token in [AssemblerParser.SYMNAME]:
                localctx = AssemblerParser.SymbolExprContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 235
                self.match(AssemblerParser.SYMNAME)
                pass
            elif token in [AssemblerParser.CHAR]:
                localctx = AssemblerParser.CharExprContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 236
                self.match(AssemblerParser.CHAR)
                pass
            elif token in [AssemblerParser.HEXNUM, AssemblerParser.BINNUM, AssemblerParser.DECNUM]:
                localctx = AssemblerParser.NumberExprContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 237
                self.number()
                pass
            else:
                raise NoViableAltException(self)

            self._ctx.stop = self._input.LT(-1)
            self.state = 251
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,10,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 249
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,9,self._ctx)
                    if la_ == 1:
                        localctx = AssemblerParser.PowerExprContext(self, AssemblerParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 240
                        if not self.precpred(self._ctx, 7):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 7)")
                        self.state = 241
                        self.match(AssemblerParser.POW)
                        self.state = 242
                        self.expr(7)
                        pass

                    elif la_ == 2:
                        localctx = AssemblerParser.MultDivExprContext(self, AssemblerParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 243
                        if not self.precpred(self._ctx, 6):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 6)")
                        self.state = 244
                        _la = self._input.LA(1)
                        if not(_la==AssemblerParser.SPLAT or _la==AssemblerParser.DIVIDE):
                            self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 245
                        self.expr(7)
                        pass

                    elif la_ == 3:
                        localctx = AssemblerParser.PlusMinusExprContext(self, AssemblerParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 246
                        if not self.precpred(self._ctx, 5):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 5)")
                        self.state = 247
                        _la = self._input.LA(1)
                        if not(_la==AssemblerParser.PLUS or _la==AssemblerParser.MINUS):
                            self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 248
                        self.expr(6)
                        pass

             
                self.state = 253
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,10,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class NumberContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def DECNUM(self):
            return self.getToken(AssemblerParser.DECNUM, 0)

        def HEXNUM(self):
            return self.getToken(AssemblerParser.HEXNUM, 0)

        def BINNUM(self):
            return self.getToken(AssemblerParser.BINNUM, 0)

        def getRuleIndex(self):
            return AssemblerParser.RULE_number

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNumber" ):
                return visitor.visitNumber(self)
            else:
                return visitor.visitChildren(self)




    def number(self):

        localctx = AssemblerParser.NumberContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_number)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 254
            _la = self._input.LA(1)
            if not(((((_la - 64)) & ~0x3f) == 0 and ((1 << (_la - 64)) & ((1 << (AssemblerParser.HEXNUM - 64)) | (1 << (AssemblerParser.BINNUM - 64)) | (1 << (AssemblerParser.DECNUM - 64)))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DirectiveContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def equate(self):
            return self.getTypedRuleContext(AssemblerParser.EquateContext,0)


        def org(self):
            return self.getTypedRuleContext(AssemblerParser.OrgContext,0)


        def struct(self):
            return self.getTypedRuleContext(AssemblerParser.StructContext,0)


        def include(self):
            return self.getTypedRuleContext(AssemblerParser.IncludeContext,0)


        def defstring(self):
            return self.getTypedRuleContext(AssemblerParser.DefstringContext,0)


        def alias(self):
            return self.getTypedRuleContext(AssemblerParser.AliasContext,0)


        def printEachLine(self):
            return self.getTypedRuleContext(AssemblerParser.PrintEachLineContext,0)


        def printSymbolTable(self):
            return self.getTypedRuleContext(AssemblerParser.PrintSymbolTableContext,0)


        def printDecoding(self):
            return self.getTypedRuleContext(AssemblerParser.PrintDecodingContext,0)


        def bindata(self):
            return self.getTypedRuleContext(AssemblerParser.BindataContext,0)


        def getRuleIndex(self):
            return AssemblerParser.RULE_directive

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDirective" ):
                return visitor.visitDirective(self)
            else:
                return visitor.visitChildren(self)




    def directive(self):

        localctx = AssemblerParser.DirectiveContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_directive)
        try:
            self.state = 266
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [AssemblerParser.EQUATE]:
                self.enterOuterAlt(localctx, 1)
                self.state = 256
                self.equate()
                pass
            elif token in [AssemblerParser.ORG]:
                self.enterOuterAlt(localctx, 2)
                self.state = 257
                self.org()
                pass
            elif token in [AssemblerParser.STRUCT]:
                self.enterOuterAlt(localctx, 3)
                self.state = 258
                self.struct()
                pass
            elif token in [AssemblerParser.INCLUDE]:
                self.enterOuterAlt(localctx, 4)
                self.state = 259
                self.include()
                pass
            elif token in [AssemblerParser.STRINGDECL]:
                self.enterOuterAlt(localctx, 5)
                self.state = 260
                self.defstring()
                pass
            elif token in [AssemblerParser.ALIAS]:
                self.enterOuterAlt(localctx, 6)
                self.state = 261
                self.alias()
                pass
            elif token in [AssemblerParser.LSTPROGRESS]:
                self.enterOuterAlt(localctx, 7)
                self.state = 262
                self.printEachLine()
                pass
            elif token in [AssemblerParser.LSTSYMTABLE]:
                self.enterOuterAlt(localctx, 8)
                self.state = 263
                self.printSymbolTable()
                pass
            elif token in [AssemblerParser.LSTDECODING]:
                self.enterOuterAlt(localctx, 9)
                self.state = 264
                self.printDecoding()
                pass
            elif token in [AssemblerParser.BINDATA]:
                self.enterOuterAlt(localctx, 10)
                self.state = 265
                self.bindata()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class BindataContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def BINDATA(self):
            return self.getToken(AssemblerParser.BINDATA, 0)

        def SYMNAME(self):
            return self.getToken(AssemblerParser.SYMNAME, 0)

        def bindataList(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AssemblerParser.BindataListContext)
            else:
                return self.getTypedRuleContext(AssemblerParser.BindataListContext,i)


        def getRuleIndex(self):
            return AssemblerParser.RULE_bindata

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBindata" ):
                return visitor.visitBindata(self)
            else:
                return visitor.visitChildren(self)




    def bindata(self):

        localctx = AssemblerParser.BindataContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_bindata)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 268
            self.match(AssemblerParser.BINDATA)
            self.state = 269
            self.match(AssemblerParser.SYMNAME)
            self.state = 271 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 270
                self.bindataList()
                self.state = 273 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (((((_la - 64)) & ~0x3f) == 0 and ((1 << (_la - 64)) & ((1 << (AssemblerParser.HEXNUM - 64)) | (1 << (AssemblerParser.BINNUM - 64)) | (1 << (AssemblerParser.DECNUM - 64)) | (1 << (AssemblerParser.CHAR - 64)))) != 0)):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class BindataItemContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return AssemblerParser.RULE_bindataItem

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class BindataItemCharContext(BindataItemContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.BindataItemContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def CHAR(self):
            return self.getToken(AssemblerParser.CHAR, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBindataItemChar" ):
                return visitor.visitBindataItemChar(self)
            else:
                return visitor.visitChildren(self)


    class BindataItemNumberContext(BindataItemContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.BindataItemContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def number(self):
            return self.getTypedRuleContext(AssemblerParser.NumberContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBindataItemNumber" ):
                return visitor.visitBindataItemNumber(self)
            else:
                return visitor.visitChildren(self)



    def bindataItem(self):

        localctx = AssemblerParser.BindataItemContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_bindataItem)
        try:
            self.state = 277
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [AssemblerParser.HEXNUM, AssemblerParser.BINNUM, AssemblerParser.DECNUM]:
                localctx = AssemblerParser.BindataItemNumberContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 275
                self.number()
                pass
            elif token in [AssemblerParser.CHAR]:
                localctx = AssemblerParser.BindataItemCharContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 276
                self.match(AssemblerParser.CHAR)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class BindataListContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def bindataItem(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AssemblerParser.BindataItemContext)
            else:
                return self.getTypedRuleContext(AssemblerParser.BindataItemContext,i)


        def SEP(self, i:int=None):
            if i is None:
                return self.getTokens(AssemblerParser.SEP)
            else:
                return self.getToken(AssemblerParser.SEP, i)

        def getRuleIndex(self):
            return AssemblerParser.RULE_bindataList

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBindataList" ):
                return visitor.visitBindataList(self)
            else:
                return visitor.visitChildren(self)




    def bindataList(self):

        localctx = AssemblerParser.BindataListContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_bindataList)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 279
            self.bindataItem()
            self.state = 284
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==AssemblerParser.SEP:
                self.state = 280
                self.match(AssemblerParser.SEP)
                self.state = 281
                self.bindataItem()
                self.state = 286
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PrintEachLineContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LSTPROGRESS(self):
            return self.getToken(AssemblerParser.LSTPROGRESS, 0)

        def getRuleIndex(self):
            return AssemblerParser.RULE_printEachLine

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPrintEachLine" ):
                return visitor.visitPrintEachLine(self)
            else:
                return visitor.visitChildren(self)




    def printEachLine(self):

        localctx = AssemblerParser.PrintEachLineContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_printEachLine)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 287
            self.match(AssemblerParser.LSTPROGRESS)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PrintSymbolTableContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LSTSYMTABLE(self):
            return self.getToken(AssemblerParser.LSTSYMTABLE, 0)

        def getRuleIndex(self):
            return AssemblerParser.RULE_printSymbolTable

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPrintSymbolTable" ):
                return visitor.visitPrintSymbolTable(self)
            else:
                return visitor.visitChildren(self)




    def printSymbolTable(self):

        localctx = AssemblerParser.PrintSymbolTableContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_printSymbolTable)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 289
            self.match(AssemblerParser.LSTSYMTABLE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PrintDecodingContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LSTDECODING(self):
            return self.getToken(AssemblerParser.LSTDECODING, 0)

        def getRuleIndex(self):
            return AssemblerParser.RULE_printDecoding

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPrintDecoding" ):
                return visitor.visitPrintDecoding(self)
            else:
                return visitor.visitChildren(self)




    def printDecoding(self):

        localctx = AssemblerParser.PrintDecodingContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_printDecoding)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 291
            self.match(AssemblerParser.LSTDECODING)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class EquateContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EQUATE(self):
            return self.getToken(AssemblerParser.EQUATE, 0)

        def SYMNAME(self):
            return self.getToken(AssemblerParser.SYMNAME, 0)

        def expr(self):
            return self.getTypedRuleContext(AssemblerParser.ExprContext,0)


        def getRuleIndex(self):
            return AssemblerParser.RULE_equate

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEquate" ):
                return visitor.visitEquate(self)
            else:
                return visitor.visitChildren(self)




    def equate(self):

        localctx = AssemblerParser.EquateContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_equate)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 293
            self.match(AssemblerParser.EQUATE)
            self.state = 294
            self.match(AssemblerParser.SYMNAME)
            self.state = 295
            self.expr(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class OrgContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ORG(self):
            return self.getToken(AssemblerParser.ORG, 0)

        def expr(self):
            return self.getTypedRuleContext(AssemblerParser.ExprContext,0)


        def getRuleIndex(self):
            return AssemblerParser.RULE_org

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitOrg" ):
                return visitor.visitOrg(self)
            else:
                return visitor.visitChildren(self)




    def org(self):

        localctx = AssemblerParser.OrgContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_org)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 297
            self.match(AssemblerParser.ORG)
            self.state = 298
            self.expr(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AliasContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return AssemblerParser.RULE_alias

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class AliasdefContext(AliasContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.AliasContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ALIAS(self):
            return self.getToken(AssemblerParser.ALIAS, 0)
        def GPR(self):
            return self.getToken(AssemblerParser.GPR, 0)
        def ALIASNAME(self):
            return self.getToken(AssemblerParser.ALIASNAME, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAliasdef" ):
                return visitor.visitAliasdef(self)
            else:
                return visitor.visitChildren(self)



    def alias(self):

        localctx = AssemblerParser.AliasContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_alias)
        try:
            localctx = AssemblerParser.AliasdefContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 300
            self.match(AssemblerParser.ALIAS)
            self.state = 301
            self.match(AssemblerParser.GPR)
            self.state = 302
            self.match(AssemblerParser.ALIASNAME)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StructContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def structdecl(self):
            return self.getTypedRuleContext(AssemblerParser.StructdeclContext,0)


        def structorigin(self):
            return self.getTypedRuleContext(AssemblerParser.StructoriginContext,0)


        def structname(self):
            return self.getTypedRuleContext(AssemblerParser.StructnameContext,0)


        def LBRACE(self):
            return self.getToken(AssemblerParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(AssemblerParser.RBRACE, 0)

        def vardecl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AssemblerParser.VardeclContext)
            else:
                return self.getTypedRuleContext(AssemblerParser.VardeclContext,i)


        def getRuleIndex(self):
            return AssemblerParser.RULE_struct

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStruct" ):
                return visitor.visitStruct(self)
            else:
                return visitor.visitChildren(self)




    def struct(self):

        localctx = AssemblerParser.StructContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_struct)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 304
            self.structdecl()
            self.state = 305
            self.structorigin()
            self.state = 306
            self.structname()
            self.state = 307
            self.match(AssemblerParser.LBRACE)
            self.state = 309 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 308
                self.vardecl()
                self.state = 311 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==AssemblerParser.SYMNAME):
                    break

            self.state = 313
            self.match(AssemblerParser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StructdeclContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def STRUCT(self):
            return self.getToken(AssemblerParser.STRUCT, 0)

        def getRuleIndex(self):
            return AssemblerParser.RULE_structdecl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStructdecl" ):
                return visitor.visitStructdecl(self)
            else:
                return visitor.visitChildren(self)




    def structdecl(self):

        localctx = AssemblerParser.StructdeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_structdecl)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 315
            self.match(AssemblerParser.STRUCT)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StructoriginContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return AssemblerParser.RULE_structorigin

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class DefvarOriginNumberContext(StructoriginContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.StructoriginContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def number(self):
            return self.getTypedRuleContext(AssemblerParser.NumberContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDefvarOriginNumber" ):
                return visitor.visitDefvarOriginNumber(self)
            else:
                return visitor.visitChildren(self)


    class DefvarOriginSymbolContext(StructoriginContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.StructoriginContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def SYMNAME(self):
            return self.getToken(AssemblerParser.SYMNAME, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDefvarOriginSymbol" ):
                return visitor.visitDefvarOriginSymbol(self)
            else:
                return visitor.visitChildren(self)


    class DefvarOriginNextContext(StructoriginContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.StructoriginContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ORIGIN_NEXT(self):
            return self.getToken(AssemblerParser.ORIGIN_NEXT, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDefvarOriginNext" ):
                return visitor.visitDefvarOriginNext(self)
            else:
                return visitor.visitChildren(self)



    def structorigin(self):

        localctx = AssemblerParser.StructoriginContext(self, self._ctx, self.state)
        self.enterRule(localctx, 46, self.RULE_structorigin)
        try:
            self.state = 320
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [AssemblerParser.HEXNUM, AssemblerParser.BINNUM, AssemblerParser.DECNUM]:
                localctx = AssemblerParser.DefvarOriginNumberContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 317
                self.number()
                pass
            elif token in [AssemblerParser.SYMNAME]:
                localctx = AssemblerParser.DefvarOriginSymbolContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 318
                self.match(AssemblerParser.SYMNAME)
                pass
            elif token in [AssemblerParser.ORIGIN_NEXT]:
                localctx = AssemblerParser.DefvarOriginNextContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 319
                self.match(AssemblerParser.ORIGIN_NEXT)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StructnameContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SYMNAME(self):
            return self.getToken(AssemblerParser.SYMNAME, 0)

        def getRuleIndex(self):
            return AssemblerParser.RULE_structname

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStructname" ):
                return visitor.visitStructname(self)
            else:
                return visitor.visitChildren(self)




    def structname(self):

        localctx = AssemblerParser.StructnameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 48, self.RULE_structname)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 322
            self.match(AssemblerParser.SYMNAME)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class VardeclContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SYMNAME(self):
            return self.getToken(AssemblerParser.SYMNAME, 0)

        def DECLWORD(self):
            return self.getToken(AssemblerParser.DECLWORD, 0)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AssemblerParser.ExprContext)
            else:
                return self.getTypedRuleContext(AssemblerParser.ExprContext,i)


        def FILL(self):
            return self.getToken(AssemblerParser.FILL, 0)

        def getRuleIndex(self):
            return AssemblerParser.RULE_vardecl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVardecl" ):
                return visitor.visitVardecl(self)
            else:
                return visitor.visitChildren(self)




    def vardecl(self):

        localctx = AssemblerParser.VardeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 50, self.RULE_vardecl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 324
            self.match(AssemblerParser.SYMNAME)
            self.state = 325
            self.match(AssemblerParser.DECLWORD)
            self.state = 326
            self.expr(0)
            self.state = 329
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==AssemblerParser.FILL:
                self.state = 327
                self.match(AssemblerParser.FILL)
                self.state = 328
                self.expr(0)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class IncludeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INCLUDE(self):
            return self.getToken(AssemblerParser.INCLUDE, 0)

        def FILENAME(self):
            return self.getToken(AssemblerParser.FILENAME, 0)

        def getRuleIndex(self):
            return AssemblerParser.RULE_include

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInclude" ):
                return visitor.visitInclude(self)
            else:
                return visitor.visitChildren(self)




    def include(self):

        localctx = AssemblerParser.IncludeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 52, self.RULE_include)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 331
            self.match(AssemblerParser.INCLUDE)
            self.state = 332
            self.match(AssemblerParser.FILENAME)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DefstringContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def STRINGDECL(self):
            return self.getToken(AssemblerParser.STRINGDECL, 0)

        def SYMNAME(self):
            return self.getToken(AssemblerParser.SYMNAME, 0)

        def stringpartcollection(self):
            return self.getTypedRuleContext(AssemblerParser.StringpartcollectionContext,0)


        def getRuleIndex(self):
            return AssemblerParser.RULE_defstring

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDefstring" ):
                return visitor.visitDefstring(self)
            else:
                return visitor.visitChildren(self)




    def defstring(self):

        localctx = AssemblerParser.DefstringContext(self, self._ctx, self.state)
        self.enterRule(localctx, 54, self.RULE_defstring)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 334
            self.match(AssemblerParser.STRINGDECL)
            self.state = 335
            self.match(AssemblerParser.SYMNAME)
            self.state = 336
            self.stringpartcollection()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StringpartcollectionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def stringpart(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AssemblerParser.StringpartContext)
            else:
                return self.getTypedRuleContext(AssemblerParser.StringpartContext,i)


        def SEP(self, i:int=None):
            if i is None:
                return self.getTokens(AssemblerParser.SEP)
            else:
                return self.getToken(AssemblerParser.SEP, i)

        def getRuleIndex(self):
            return AssemblerParser.RULE_stringpartcollection

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStringpartcollection" ):
                return visitor.visitStringpartcollection(self)
            else:
                return visitor.visitChildren(self)




    def stringpartcollection(self):

        localctx = AssemblerParser.StringpartcollectionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 56, self.RULE_stringpartcollection)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 338
            self.stringpart()
            self.state = 343
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==AssemblerParser.SEP:
                self.state = 339
                self.match(AssemblerParser.SEP)
                self.state = 340
                self.stringpart()
                self.state = 345
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StringpartContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return AssemblerParser.RULE_stringpart

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class StringPartStringContext(StringpartContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.StringpartContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def StringLiteral(self):
            return self.getToken(AssemblerParser.StringLiteral, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStringPartString" ):
                return visitor.visitStringPartString(self)
            else:
                return visitor.visitChildren(self)


    class StringPartCharContext(StringpartContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.StringpartContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def CHAR(self):
            return self.getToken(AssemblerParser.CHAR, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStringPartChar" ):
                return visitor.visitStringPartChar(self)
            else:
                return visitor.visitChildren(self)


    class StringPartExprContext(StringpartContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a AssemblerParser.StringpartContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def number(self):
            return self.getTypedRuleContext(AssemblerParser.NumberContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStringPartExpr" ):
                return visitor.visitStringPartExpr(self)
            else:
                return visitor.visitChildren(self)



    def stringpart(self):

        localctx = AssemblerParser.StringpartContext(self, self._ctx, self.state)
        self.enterRule(localctx, 58, self.RULE_stringpart)
        try:
            self.state = 349
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [AssemblerParser.HEXNUM, AssemblerParser.BINNUM, AssemblerParser.DECNUM]:
                localctx = AssemblerParser.StringPartExprContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 346
                self.number()
                pass
            elif token in [AssemblerParser.StringLiteral]:
                localctx = AssemblerParser.StringPartStringContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 347
                self.match(AssemblerParser.StringLiteral)
                pass
            elif token in [AssemblerParser.CHAR]:
                localctx = AssemblerParser.StringPartCharContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 348
                self.match(AssemblerParser.CHAR)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[9] = self.expr_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expr_sempred(self, localctx:ExprContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 7)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 6)
         

            if predIndex == 2:
                return self.precpred(self._ctx, 5)
         





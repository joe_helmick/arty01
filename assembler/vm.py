from JMUArchitecture import *
import serial as COM


class VirtualMachine:

    CON_INT_VECTOR = 0x00008

    # Initialize empty RAM of proper size.
    memory = [0] * Arch.MEMORY_DEPTH
    memory_original = [0] * Arch.MEMORY_DEPTH

    # Register file.
    gpr = [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0]
    ]

    gpr_changed = []

    # Other MCU flags, registers, and ports.
    flagZ, flagZ_prev = False, False
    flagS, flagS_prev = False, False
    SP, SP_prev = 0, 0
    PC, PC_prev = 0, 0
    addr, addr_prev = 0, 0
    memval, memval_prev = 0, 0
    tx_byte, io_byte = 0, 0
    rx_byte = 0

    # Initialize other stuff.
    handled = False
    halt = False
    serial = None
    instructionCount = 0
    debuglinewidth = 60
    debugcharpos = 0

    baSign = None


    def copy_previous_state(self):
        for r in self.gpr_changed:
            self.gpr[r][1] = self.gpr[r][0]
        self.SP_prev = self.SP
        self.flagS_prev = self.flagS
        self.flagZ_prev = self.flagZ
        return


    def kbd_interrupt(self, bytecount, vector):
        # Save the current PC on the stack
        self.preDecSp()
        self.memory[self.SP] = self.PC
        # Simulate jump to CPU ISR location
        self.PC = vector

        # Simulate the ISR here.
        inchars = list()
        count = bytecount
        while count > 0:
            inchar = self.serial.read(1)
            inchars.append(inchar)
            count -= 1

        inchars.append(b'\00')
        print('inchars', inchars)

        # Simulate what an IRET instruction would do.
        self.PC = self.memory[self.SP]
        self.postIncSP()
        return


    def detect_kbd_interrupt(self):
        # This next line mimics the way the RXUART might raise a signal to the CPU
        # indicating that there are characters in the read buffer.
        # In this model, the CPU would then immediately return
        count = self.serial.in_waiting
        if count > 0:
            self.kbd_interrupt(count, self.CON_INT_VECTOR)
        return


    def run_instruction(self, instr):
        # Bail out in halt condition.
        if self.halt:
            return

        if (self.instructionCount % 100) == 0:
            self.detect_kbd_interrupt()

        self.copy_previous_state()
        self.gpr_changed.clear()
        self.execute(instr)
        return


    def __init__(self, memory):
        # Make a live copy of RAM and a backup in case or reset.
        self.memory = memory
        self.memory_original = self.memory.copy()

        # Create a serial port connection.
        self.serial = COM.Serial('COM3',
                                 baudrate=115_200,
                                 bytesize=COM.EIGHTBITS,
                                 parity=COM.PARITY_NONE,
                                 stopbits=COM.STOPBITS_ONE,
                                 timeout=0)
        return


    def setFlags(self, value: int):
        self.flagZ = (value == 0)
        self.flagS = (value >= 0x2000)
        return


    def showfeedback(self, show_now=False):
        self.instructionCount += 1
        if (self.instructionCount % 100_000 == 0) or (show_now is True):
            print('vm.instructionCount', f'{self.instructionCount:,}')
        # print(self.message)
        self.handled = True
        return


    def set_gpr(self, index, value):

        self.gpr_changed.append(index)
        # # Save previous value first.
        # self.gpr[index][2] = self.gpr[index][1]
        # Set new value.
        self.gpr[index][0] = value
        return


    def get_gpr(self, index):
        return self.gpr[index][0]


    def get_gpr_prev(self, index):
        return self.gpr[index][1]


    def postIncSP(self):
        # Save previous value first.
        self.SP_prev = self.SP
        # Set new value.
        if self.SP == Arch.MAX_UVALUE_REG:
            self.SP = 0
        else:
            self.SP += 1
        return


    def preDecSp(self):
        # Save previous value first.
        self.SP_prev = self.SP
        # Set new value
        if self.SP == 0:
            self.SP = Arch.MAX_UVALUE_REG
        else:
            self.SP -= 1
        return


    @staticmethod
    def flag_check(flagZ, flagS, flagid, flagsc):
        if flagid == 0:
            return True
        if flagid == 1 and flagsc == 1 and flagZ is True:
            return True
        if flagid == 2 and flagsc == 1 and flagS is True:
            return True
        if flagid == 1 and flagsc == 0 and flagZ is False:
            return True
        if flagid == 2 and flagsc == 0 and flagS is False:
            return True
        return False


    def execute(self, instr):
        # Instruction is pre-decoded; so here we just advance the
        # program counter based on the instruction's length and
        # then go about executing it.
        if instr.iw2 is None:
            self.PC += 1
        else:
            self.PC += 2

        # Set handled to False to find non-coded instructions
        self.handled = False

        # Now execute instructions based on opcode....

        if instr.opcode == Arch.OPC_LD_GPR_EXPR:
            self.set_gpr(instr.a, instr.iw2)
            # self.message = 'ldw r' + str(self.a) + ', ' + str(self.iw2)
            self.showfeedback()
            self.handled = True
            return

        if instr.opcode == Arch.OPC_LD_A_T:
            self.set_gpr(instr.a, instr.t)
            # self.message = 'ld r' + str(self.a) + ', ' + str(self.t)
            self.showfeedback()
            self.handled = True
            return

        if instr.opcode == Arch.OPC_ADD_A_T:
            v = self.get_gpr(instr.a)
            v += instr.t
            # TODO: NEED A REAL 18-BIT INTEGER HERE
            if v == -1:
                v = 0x3ffff
            self.set_gpr(instr.a, v)
            self.setFlags(v)
            # self.message = 'add r' + str(self.a) + ', ' + str(self.t)
            self.showfeedback()
            self.handled = True
            return

        if instr.opcode == Arch.OPC_ADD_AB:
            va = self.get_gpr(instr.a)
            vb = self.get_gpr(instr.b)
            result = va + vb
            # TODO: NEED A REAL 18-BIT INTEGER HERE
            if result == -1:
                result = 0x3ffff
            self.set_gpr(instr.a, result)
            self.setFlags(result)
            # self.message = 'add r' + str(self.a) + ', r' + str(self.b)
            self.showfeedback()
            self.handled = True
            return

        if instr.opcode == Arch.OPC_SUB_A_T:
            v = self.get_gpr(instr.a)
            v -= instr.t
            # TODO: NEED A REAL 18-BIT INTEGER HERE
            if v == -1:
                v = 0x3ffff
            self.set_gpr(instr.a, v)
            self.setFlags(v)
            # self.message = 'sub r' + str(self.a) + ', ' + str(self.t)
            self.showfeedback()
            self.handled = True
            return

        if instr.opcode == Arch.OPC_SUB_AB:
            va = self.get_gpr(instr.a)
            vb = self.get_gpr(instr.b)
            diff = va - vb
            # TODO: NEED A REAL 18-BIT INTEGER HERE
            if diff == -1:
                diff = 0x3ffff
            self.setFlags(diff)
            # self.message = 'sub r' + str(self.a) + ', r' + str(self.b)
            self.showfeedback()
            self.handled = True
            return

        if instr.opcode == Arch.OPC_DIV_ABC:
            dividend = self.get_gpr(instr.a)
            divisor = self.get_gpr(instr.b)
            quotient = int(dividend / divisor)
            remainder = int(dividend % divisor)
            self.set_gpr(instr.a, quotient)
            self.set_gpr(instr.c, remainder)
            # self.setFlags(dividend)  # TODO NOT SURE ABOUT THIS SETTING OF FLAG???
            self.setFlags(quotient)  # TODO NOT SURE ABOUT THIS SETTING OF FLAG, CHECK VERILOG MODEL
            # self.message = 'div r' + str(self.a) + ', r' + str(self.b) + ', r' + str(self.c)
            self.showfeedback()
            self.handled = True
            return

        if instr.opcode == Arch.OPC_LD_GPR_GPTR:
            ptr = self.get_gpr(instr.b)
            value = self.memory[ptr]
            self.set_gpr(instr.a, value)
            # self.message = 'ld r' + str(self.a) + ', r[' + str(str(self.b)) + ']'
            self.showfeedback()
            self.handled = True
            return

        if instr.opcode == Arch.OPC_JA:
            doAction = self.flag_check(self.flagZ, self.flagS, instr.flagid, instr.flagsc)
            self.handled = True
            if doAction:
                self.PC = instr.iw2
                self.showfeedback()
            return

        if instr.opcode == Arch.OPC_JR:
            self.PC = self.PC + instr.s
            # self.message = 'jr ' + str(self.s)
            self.showfeedback()
            self.handled = True
            return

        if instr.opcode == Arch.OPC_JRZ:
            if self.flagZ is True:
                self.PC = self.PC + instr.s
            # self.message = 'jr.z ' + str(self.s)
            self.showfeedback()
            self.handled = True
            return

        if instr.opcode == Arch.OPC_JRNZ:
            if self.flagZ is False:
                self.PC = self.PC + instr.s
            # self.message = 'jr.nz ' + str(self.s)
            self.showfeedback()
            self.handled = True
            return

        if instr.opcode == Arch.OPC_JRS:
            if self.flagS is True:
                self.PC = self.PC + instr.s
            # self.message = 'jr.s ' + str(self.s)
            self.showfeedback()
            return

        if instr.opcode == Arch.OPC_JRNS:
            if self.flagS is False:
                self.PC = self.PC + instr.s
            # self.message = 'jr.ns ' + str(self.s)
            self.showfeedback()
            self.handled = True
            return

        if instr.opcode == Arch.OPC_CMP_A_T:
            v = self.get_gpr(instr.a)
            v -= instr.t
            # TODO: NEED A REAL 18-BIT INTEGER HERE
            if v == -1:
                v = 0x3ffff
            self.setFlags(v)
            # self.message = 'cmp r' + str(self.a) + ', ' + str(self.t)
            self.showfeedback()
            self.handled = True
            return

        if instr.opcode == Arch.OPC_CMP_AB:
            va = self.get_gpr(instr.a)
            vb = self.get_gpr(instr.b)
            diff = va - vb
            # TODO: NEED A REAL 18-BIT INTEGER HERE
            if diff == -1:
                diff = 0x3ffff
            self.setFlags(diff)
            # self.message = 'cmp r' + str(self.a) + ', r' + str(self.b)
            self.showfeedback()
            self.handled = True
            return

        if instr.opcode == Arch.OPC_MUL_AB:
            va = self.get_gpr(instr.a)
            vb = self.get_gpr(instr.b)
            prod = va * vb
            self.set_gpr(instr.a, prod)
            self.setFlags(prod)
            # self.message = 'mul r' + str(self.a) + ', r' + str(self.b)
            self.showfeedback()
            self.handled = True
            return

        if instr.opcode == Arch.OPC_MUL_A_T:
            va = self.get_gpr(instr.a)
            vt = self.get_gpr(instr.t)
            prod = va * vt
            self.set_gpr(instr.a, prod)
            self.setFlags(prod)
            # self.message = 'mul r' + str(self.a) + ', ' + str(self.t)
            self.showfeedback()
            self.handled = True
            return

        if instr.opcode == Arch.OPC_RET:
            doAction = self.flag_check(self.flagZ, self.flagS, instr.flagid, instr.flagsc)
            self.showfeedback()
            self.handled = True
            if doAction:
                self.PC = self.memory[self.SP]
                self.postIncSP()
            return

        if instr.opcode == Arch.OPC_CALL_FAR:
            self.preDecSp()
            self.memory[self.SP] = self.PC
            self.PC = instr.iw2
            # self.message = 'call ' + str(self.iw2)
            self.showfeedback()
            self.handled = True
            return

        if instr.opcode == Arch.OPC_CALL_NEAR:
            self.preDecSp()
            self.memory[self.SP] = self.PC
            self.PC = self.PC + instr.s
            # self.message = 'call ' + str(self.s)
            self.showfeedback()
            self.handled = True
            return

        if instr.opcode == Arch.OPC_SV_GPTR_GPR:
            val = self.get_gpr(instr.b)
            ptr = self.get_gpr(instr.a)
            self.memory[ptr] = val
            # self.message = 'sv [r' + str(self.a) + '], r' + str(str(self.b))
            self.showfeedback()
            self.handled = True
            return

        if instr.opcode == Arch.OPC_TX_T:
            self.tx_byte = instr.t
            # self.message = 'tx ' + str(self.tx_byte)
            self.serial.write(bytes([self.tx_byte]))
            self.showfeedback()
            self.handled = True
            return

        if instr.opcode == Arch.OPC_TX_GPR:
            self.tx_byte = self.get_gpr(instr.a)
            # self.message = 'tx r' + str(a)
            self.serial.write(bytes([self.tx_byte]))
            self.showfeedback()
            self.handled = True
            return

        if instr.opcode == Arch.OPC_RX_GPR:
            pass
            # # bites = self.serial.read(size=1)
            # bites = self.serial.read()
            # if len(bites) > 0:
            #     print('bites', bites)
            #
            #     # if self.rx_byte == 32:
            #     #     self.set_gpr(self.a, self.rx_byte)
            #
            #     self.rx_byte = bites[0]
            #     self.set_gpr(instr.a, self.rx_byte)
            #
            #     # self.message = 'rx r' + str(a)
            # self.showfeedback()
            # self.handled = True
            # return

        if instr.opcode == Arch.OPC_PUSH:
            self.preDecSp()
            if instr.opt5 == 0:  # gpr
                val = self.get_gpr(instr.a)
                self.memory[self.SP] = val
                # self.message = 'push r' + str(self.a)
            else:  # flags? TODO PUSH FLAGS
                pass
            self.showfeedback()
            self.handled = True
            return

        if instr.opcode == Arch.OPC_POP:
            if instr.opt5 == 0:  # gpr
                val = self.memory[self.SP]
                self.set_gpr(instr.a, val)
                # self.message = 'pop r' + str(self.a)
            else:  # flags? TODO PUSH FLAGS
                pass
            self.postIncSP()
            self.showfeedback()
            self.handled = True
            return

        if instr.opcode == Arch.OPC_SV_EXPR_GPR:
            self.memory[instr.iw2] = self.get_gpr(instr.a)
            # self.message = 'sv ' + str(self.iw2) + ', ' + str(self.a)
            self.showfeedback()
            return

        if instr.opcode == Arch.OPC_COPY_AB:
            val = self.get_gpr(instr.b)
            self.set_gpr(instr.a, val)
            # self.message = 'copy ' + str(self.a) + ', ' + str(self.b)
            self.showfeedback()
            self.handled = True
            return

        if not self.handled:
            print('UNHANDLED OPCODE', instr.opcode)
            raise ValueError



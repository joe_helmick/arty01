`include "arch_defines.vh"  // SET "SIMULATION" IN THIS FILE

module executor(
	input wire sys_rst,
	input wire clk_sys,
	input wire [6:0] fsm_state,
	input wire [17:0] ram_data_dout,
	input wire [17:0] ram_code_dout,
	input wire [17:0] rf_douta,
	input wire [17:0] rf_doutb,
	input wire [17:0] rf_doutc,
	input wire [17:0] sp_new,
	input wire [17:0] pc,
	input wire Z,
	input wire S,
	input wire [17:0] alu_result,
	input wire [17:0] alu_md_result,
	input wire [17:0] alu_md_remainder,
	// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
	`ifdef SIMULATION
		output wire [17:0] iw1,
		output wire [17:0] iw2,
		output wire [6:0] opcode,
		output wire [1:0] flagid,
		output wire flagsc,
		output wire opt5,
		output wire [2:0] flags,
		output wire fault,
	`endif

	output wire [7:0] io_byte,				// actual IO
	output wire io_strobe,					// actual IO
	output wire [7:0] txdata,           // RS232 output byte
	output wire txwrite,                // RS232 output signal

	output wire [4:0] a,						// reg address
	output wire [4:0] b,						// reg address
	output wire [4:0] c,						// reg address
	output wire [7:0] t,						// AT instructions

	output wire pc_enable,					// enable the program counter
	
	output wire pc_action_step,			// program counter 
	output wire pc_action_jr,				// program counter
	output wire signed [10:0] s,			// jump delta
	output wire pc_action_ja,				// program counter
	output wire [17:0] pc_ja_addr,		// program counter

	output wire ram_data_we,				// memory write
	output wire [17:0] ram_data_addr,	// memory write
	output wire [17:0] ram_data_din,		// memory write

	output wire done,							// fsm indicator

	output wire rf_wea,						// regfile write
	output wire rf_wec,						// regfile write
	output wire [17:0] rf_dina,			// regfile write
	output wire [17:0] rf_dinc,			// regfile write

	output wire sp_add,						// sp update
	output wire sp_alu_en,					// sp update
	output wire [17:0] sp,					// sp update

	output wire alu_en,						// alu
	output wire [3:0] alu_op,				// alu
	output wire [17:0] alu_a,				// alu
	output wire [17:0] alu_b,				// alu
	output wire which_alu,					// alu
	output wire muldiv_action,				// alu
	output wire muldiv_start				// alu

);

// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
// Declarations.
	reg doneR;
	reg [4:0] aR, bR, cR;
	reg [7:0] tR;
	reg signed [10:0] sR;
	reg [1:0] flagidR;
	reg flagscR;
	reg opt5R;
	reg [2:0] flagsR;
	reg faultR; 
	reg [7:0] gpio1_byteR; 
	reg gpio1_strobeR;
	reg [17:0] far_call_addr;
	reg [17:0] iw1R;
	reg [17:0] iw2R;
	reg ram_data_weR;
	reg [17:0] ram_data_addrR;
	reg [17:0] ram_data_dinR;
	reg [17:0] rf_dinaR;
	reg [17:0] rf_dincR;
	reg rf_weaR;
	reg rf_wecR;
	reg [17:0] spR;
	reg sp_addR;
	reg sp_alu_enR;
	reg alu_enR;
	reg [3:0] alu_opR;
	reg [17:0] alu_aR;
	reg [17:0] alu_bR;
	reg which_aluR;
	reg muldiv_actionR;
	reg muldiv_startR;
	reg pc_action_stepR;
	reg pc_action_jrR;
	reg pc_action_jaR;
	reg pc_enableR;
	reg [17:0] pc_ja_addrR;
	reg [6:0] opcodeR;
	reg [7:0] txdataR;
	reg txwriteR;


// Assignments of register variables to port outputs.
	`ifdef SIMULATION
		assign iw1 = iw1R;
		assign iw2 = iw2R;
		assign opcode = opcodeR;
		assign flagid = flagidR;
		assign flagsc = flagscR;
		assign opt5 = opt5R;
		assign flags = flagsR;
		assign fault = faultR;
	`endif

	assign io_byte = gpio1_byteR;
	assign io_strobe = gpio1_strobeR;
	assign txdata = txdataR;
	assign txwrite = txwriteR;

	assign a = aR;
	assign b = bR;
	assign c = cR;
	assign t = tR;

	assign pc_enable = pc_enableR;
	assign pc_action_step = pc_action_stepR;
	assign pc_action_jr = pc_action_jrR;
	assign s = sR;
	assign pc_action_ja = pc_action_jaR;
	assign pc_ja_addr = pc_ja_addrR;

	assign ram_data_we = ram_data_weR;
	assign ram_data_addr = ram_data_addrR;
	assign ram_data_din = ram_data_dinR;

	assign done = doneR;

	assign rf_wea = rf_weaR;
	assign rf_wec = rf_wecR;
	assign rf_dina = rf_dinaR;
	assign rf_dinc = rf_dincR;

	assign sp_add = sp_addR;
	assign sp_alu_en = sp_alu_enR;
	assign sp = spR;

	assign alu_en = alu_enR;
	assign alu_op = alu_opR;
	assign alu_a = alu_aR;
	assign alu_b = alu_bR;
	assign which_alu = which_aluR;
	assign muldiv_action = muldiv_actionR;
	assign muldiv_start = muldiv_startR;


// MAIN EXECUTOR
// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
//always @ (posedge clk_sys)
always @ (posedge clk_sys)
begin
	if (sys_rst) begin
		`ifdef SIMULATION
			faultR <= 0;
			flagsR <= 0;
		`endif
		gpio1_byteR <= 0;
		gpio1_strobeR <= 0;
		doneR <= 0;
	end
	else
	begin
		doneR <= 0;
		faultR <= 0;
		//$display("%t opcode %d, fsm %d opcode %d fsm %h ", $time, opcode, fsm_state, codestate[11:5], codestate[4:0]);
		casez ({opcodeR, fsm_state})

			{7'b???????, `FSM_LD_IW1_INIT}: begin
				ram_data_weR <= 0;
				iw1R <= ram_code_dout;
				end

			{7'b???????, `FSM_LD_IW1_WAIT}: pc_step();

			{7'b???????, `FSM_DECODE}: begin
				pc_enableR <= 0;
				aR <= iw1R[4:0];

				if (iw1R[17] == 1'b1) begin // an AT instruction
					opcodeR <= {3'b100, iw1R[16:13]};
					tR <= iw1R[12:5]; 
				end
				else begin // regular instruction
					opcodeR <= iw1R[17:11];
					bR <= iw1R[9:5];
					sR <= iw1R[10:0];
					flagidR <= iw1R[2:1];
					flagscR <= iw1R[0];
					opt5R <= iw1R[5];
				end
			end
			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			// From here onward we know the opcode, so start executing.
			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			// load regA with value of expression
			// 2-word instruction; expr is in second word
			// 	ldw @digitpointer, (numberbuffer + 5)
			// 	ldw r30, (buf + 5)
			{`OPCODE_LD_GPR_EXPR, 7'd4}: pc_step();
			{`OPCODE_LD_GPR_EXPR, 7'd5}: pc_enableR <= 0;
			{`OPCODE_LD_GPR_EXPR, 7'd6}: begin
				iw2R <= ram_code_dout;
				load_regA(ram_code_dout);
				end
			{`OPCODE_LD_GPR_EXPR, 7'd7}: save_a_and_done();
			{`OPCODE_LD_GPR_EXPR, 7'd8}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			// load regA with value pointed at by regB
			// SAME AS OLD OPCODE_LD_PTR code
			// 1-word instruction 
			// 	ld r29, [@base_addr]
			// 	ld r29, [r31]
			{`OPCODE_LD_GPR_GPTR, 7'd4}: ram_data_addrR <= rf_doutb;
			{`OPCODE_LD_GPR_GPTR, 7'd5}: ; 
			{`OPCODE_LD_GPR_GPTR, 7'd6}: ; 
			{`OPCODE_LD_GPR_GPTR, 7'd7}: begin
				rf_dinaR <= ram_data_dout;
				rf_weaR <= 1;
				end
			{`OPCODE_LD_GPR_GPTR, 7'd8}: save_a_and_done();	
			{`OPCODE_LD_GPR_GPTR, 7'd9}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			// load regA with value of symbolic pointer in word 2
			// 2-word instruction; pointer location in second word
			// 	ld r31, [generation] 
			{`OPCODE_LD_GPR_SPTR, 7'd4}: pc_step();
			{`OPCODE_LD_GPR_SPTR, 7'd5}: pc_enableR <= 0;
			{`OPCODE_LD_GPR_SPTR, 7'd6}: begin
				// set the RAM read address from second CODE word
				iw2R <= ram_code_dout;
				ram_data_addrR <= ram_code_dout;
				end
			{`OPCODE_LD_GPR_SPTR, 7'd7}: ; // wait for RAM read
			{`OPCODE_LD_GPR_SPTR, 7'd8}: load_regA(ram_data_dout);
			{`OPCODE_LD_GPR_SPTR, 7'd9}: save_a_and_done();
			{`OPCODE_LD_GPR_SPTR, 7'd10}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			// copy contents of regB into regA
			// 1-word instruction
			// 	copy r29, r22
			{`OPCODE_COPY_AB, 7'd4}: load_regA(rf_doutb);
			{`OPCODE_COPY_AB, 7'd5}: save_a_and_done();
			{`OPCODE_COPY_AB, 7'd6}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			// load stack pointer with word value; unnecessary unless
			// creating a non-default stack, as SP = 0 at startup,
			// and pushing a value decrements it to top of RAM $3ffff.
			// 2-word instruction
			// 	ld SP, 0
			{`OPCODE_LD_SP, 7'd4}: pc_step();
			{`OPCODE_LD_SP, 7'd5}: pc_enableR <= 0;
			{`OPCODE_LD_SP, 7'd6}: begin
				iw2R <= ram_code_dout;
				spR <= ram_code_dout;
				doneR <= 1;
				end
			{`OPCODE_LD_SP, 7'd7}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			// save register's value at a location identified by expression
			// 2-word instruction, address identified by expression
			// 	sv (buffer + 1), r00
			{`OPCODE_SV_EXPR_GPR, 7'd4}: pc_step();
			{`OPCODE_SV_EXPR_GPR, 7'd5}: pc_enableR <= 0;
			{`OPCODE_SV_EXPR_GPR, 7'd6}: begin
				iw2R <= ram_code_dout;
				ram_data_addrR <= ram_code_dout;
				end
			{`OPCODE_SV_EXPR_GPR, 7'd7}: begin
				ram_data_dinR <= rf_douta;
				ram_data_weR <= 1;
				end
			{`OPCODE_SV_EXPR_GPR, 7'd8}: begin
				ram_data_weR <= 0;
				doneR <= 1;
				end
			{`OPCODE_SV_EXPR_GPR, 7'd9}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			// save register at location pointed to by other register
			// same code as old SV_PTR code
			// 1-word instruction
			// 	sv [r28], r29
			//			so if r28 holds $2000, (it's a pointer to that address)
			//			and r29 = 5
			//			then RAM[$2000] <= 5
			// sv [r31], r07 => 030ff
			// 0000110 0 00111 11111
			//     6        7   31
			//   opcode  regb  rega
			{`OPCODE_SV_GPTR_GPR, 7'd4}: begin
				ram_data_addrR <= rf_douta;	// rega has the address value
				ram_data_dinR <= rf_doutb;		// regb has the data
				ram_data_weR <= 1;				// write it
				end
			{`OPCODE_SV_GPTR_GPR, 7'd5}: ram_data_weR <= 0;
			{`OPCODE_SV_GPTR_GPR, 7'd6}: ;
			{`OPCODE_SV_GPTR_GPR, 7'd7}: doneR <= 1;
			{`OPCODE_SV_GPTR_GPR, 7'd8}: ; 

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			// save regA at location pointed at by symbol in word 2
			// 2-word instruction; word2 has symbol value
			// basic indirect symbolic method of saving/updating a variable's value

			// 0381f 02000 ┊ 	sv      [generation], r31
			// => sv [02000:8192], r31
			// => 0000111 0 00000 11111   + word 2 = 02000
			//       7        0      31     02000
			//   opcode     empty  rega    iw2

			{`OPCODE_SV_SPTR_GPR, 7'd4}: pc_step();
			{`OPCODE_SV_SPTR_GPR, 7'd5}: begin
				pc_enableR <= 0;
				end
			{`OPCODE_SV_SPTR_GPR, 7'd6}: begin
				iw2R <= ram_code_dout;					
				ram_data_addrR <= ram_code_dout;    // word2 has the address to save to
				ram_data_dinR <= rf_douta;				// rega has the data to save
				end
			{`OPCODE_SV_SPTR_GPR, 7'd7}: begin
				ram_data_weR <= 1;						// write it
				end
			{`OPCODE_SV_SPTR_GPR, 7'd8}: begin
				ram_data_weR <= 0;
				doneR <= 1;
				end
			{`OPCODE_SV_SPTR_GPR, 7'd9}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			// sv [r00], (buffer+ 1)
			// save an expression value at the location pointed to by regA
			// 2-word instruction
			// 00010000 ......aaaaa + word 2
			//    8           rega     iw2
			// TODO: DON'T HAVE A TEST FOR THIS OPCODE YET.
			// FIXME:
			{`OPCODE_SV_GPTR_EXPR, 7'd4}: begin
				faultR <= 1;
				doneR <= 1;
				end

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			{`OPCODE_CALL_FAR, 7'd4}: pc_step();
			{`OPCODE_CALL_FAR, 7'd5}: pc_enableR <= 0;
			{`OPCODE_CALL_FAR, 7'd6}: begin
				iw2R <= ram_code_dout;
				far_call_addr <= ram_code_dout;
				sp_addR <= 0;  // pre-decrement sp
				sp_alu_enR <= 1;
				end
			{`OPCODE_CALL_FAR, 7'd7}: begin
				spR <= sp_new; 
				sp_alu_enR <= 0;
				end
			{`OPCODE_CALL_FAR, 7'd8}: begin
				ram_data_dinR <= pc;
				ram_data_addrR <= sp;
				ram_data_weR <= 1;
				jump_absolute(far_call_addr);
				end
			{`OPCODE_CALL_FAR, 7'd9}: begin
				ram_data_weR <= 0;
				pc_enableR <= 0;
				end
			{`OPCODE_CALL_FAR, 7'd10}: doneR <= 1;
			{`OPCODE_CALL_FAR, 7'd11}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			{`OPCODE_CALL_NEAR, 7'd4}: begin
				sp_addR <= 0;  // pre-decrement sp
				sp_alu_enR <= 1;
				end
			{`OPCODE_CALL_NEAR, 7'd5}: begin
				spR <= sp_new; 
				sp_alu_enR <= 0;
				end
			{`OPCODE_CALL_NEAR, 7'd6}: begin
				ram_data_dinR <= pc;
				ram_data_addrR <= sp;
				ram_data_weR <= 1;
				jump_relative();
				end
			{`OPCODE_CALL_NEAR, 7'd7}: begin
				ram_data_weR <= 0;
				pc_enableR <= 0;
				end
			{`OPCODE_CALL_NEAR, 7'd8}: doneR <= 1;
			{`OPCODE_CALL_NEAR, 7'd9}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			{`OPCODE_ADD_AB, 7'd4}: load_alu_ab(`OP_ADD);
			{`OPCODE_ADD_AB, 7'd5}: alu_enR <= 0;
			{`OPCODE_ADD_AB, 7'd6}:	;
			{`OPCODE_ADD_AB, 7'd7}:	read_alu_ax();
			{`OPCODE_ADD_AB, 7'd8}: save_a_and_done();
			{`OPCODE_ADD_AB, 7'd9}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			{`OPCODE_SUB_AB, 7'd4}: load_alu_ab(`OP_SUB);
			{`OPCODE_SUB_AB, 7'd5}: alu_enR <= 0;
			{`OPCODE_SUB_AB, 7'd6}: ;
			{`OPCODE_SUB_AB, 7'd7}: read_alu_ax();
			{`OPCODE_SUB_AB, 7'd8}: save_a_and_done();
			{`OPCODE_SUB_AB, 7'd9}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			{`OPCODE_CMP_AB, 7'd4}: load_alu_ab(`OP_CMP);
			{`OPCODE_CMP_AB, 7'd5}: alu_enR <= 0;
			{`OPCODE_CMP_AB, 7'd6}: begin
				doneR <= 1;
				end
			{`OPCODE_CMP_AB, 7'd7}: begin
				flagsR[`FLAG_ID_Z] <= Z;
				flagsR[`FLAG_ID_S] <= S;
				end

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			{`OPCODE_MUL_AB, 7'd4}: begin
				alu_aR <= rf_douta;
				alu_bR <= rf_doutb;
				which_aluR <= `USE_ALU_MULTDIV;
				muldiv_actionR <= 1; // choose multiply action
				muldiv_startR <= 1; // activate module
				end
			{`OPCODE_MUL_AB, 7'd5}: begin
				muldiv_startR <= 0;  // turn off the start signal and let it run
				end
			{`OPCODE_MUL_AB, 7'd6}: ; 
			{`OPCODE_MUL_AB, 7'd7}: begin 
				rf_weaR <= 1;
				rf_dinaR <= alu_md_result;
				end
			{`OPCODE_MUL_AB, 7'd8}: save_a_and_done();
			{`OPCODE_MUL_AB, 7'd9}: begin
				flagsR[`FLAG_ID_Z] <= Z;
				flagsR[`FLAG_ID_S] <= S;
				end

			// // ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			// {`OPCODE_AND_AB, 7'd4}: load_alu_ab(`OP_AND);
			// {`OPCODE_AND_AB, 7'd5}: alu_enR <= 0;
			// {`OPCODE_AND_AB, 7'd6}: ;
			// {`OPCODE_AND_AB, 7'd7}: read_alu_ax();
			// {`OPCODE_AND_AB, 7'd8}: save_a_and_done();
			// {`OPCODE_AND_AB, 7'd9}: ;

			// // ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			// {`OPCODE_OR_AB, 7'd4}: load_alu_ab(`OP_OR);
			// {`OPCODE_OR_AB, 7'd5}: alu_enR <= 0;
			// {`OPCODE_OR_AB, 7'd6}: ;
			// {`OPCODE_OR_AB, 7'd7}: read_alu_ax();
			// {`OPCODE_OR_AB, 7'd8}: save_a_and_done();
			// {`OPCODE_OR_AB, 7'd9}: ;

			// // ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			// {`OPCODE_XOR_AB, 7'd4}: load_alu_ab(`OP_XOR);
			// {`OPCODE_XOR_AB, 7'd5}: alu_enR <= 0;
			// {`OPCODE_XOR_AB, 7'd6}: ;
			// {`OPCODE_XOR_AB, 7'd7}: read_alu_ax();
			// {`OPCODE_XOR_AB, 7'd8}: save_a_and_done();
			// {`OPCODE_XOR_AB, 7'd9}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			{`OPCODE_DIV_ABC, 7'd4}: pc_step();
			{`OPCODE_DIV_ABC, 7'd5}: pc_enableR <= 0;
			{`OPCODE_DIV_ABC, 7'd6}: begin
				iw2R <= ram_code_dout;
				// extract the c register number from this (second) instruction word
				cR <= ram_code_dout & 18'b000000000000011111;
				alu_aR <= rf_douta;
				alu_bR <= rf_doutb;
				which_aluR <= `USE_ALU_MULTDIV;
				muldiv_actionR <= 0; // choose divide action
				muldiv_startR <= 1; // start the divider
				end
			{`OPCODE_DIV_ABC, 7'd7}: begin
				muldiv_startR <= 0; // turn off start signal, let it run...
				end
			{`OPCODE_DIV_ABC, 7'd63}: begin
				rf_weaR <= 1;
				rf_dinaR <= alu_md_result;
				rf_wecR <= 1;
				rf_dincR <= alu_md_remainder;
				end
			{`OPCODE_DIV_ABC, 7'd64}: begin
 				rf_weaR <= 0;
				rf_wecR <= 0;
				doneR <= 1;
				end
			{`OPCODE_DIV_ABC, 7'd65}: begin
				flagsR[`FLAG_ID_Z] <= Z;
				flagsR[`FLAG_ID_S] <= S;
				end

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			{`OPCODE_JR, 7'd4}:	jump_relative();
			{`OPCODE_JR, 7'd5}:	pc_enableR <= 0; 
			{`OPCODE_JR, 7'd6}: doneR <= 1;
			{`OPCODE_JR, 7'd7}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			{`OPCODE_JRZ, 7'd4}: 
				if (flagsR[`FLAG_ID_Z] == 1) jump_relative(); else doneR <= 1; 
			{`OPCODE_JRZ, 7'd5}: pc_enableR <= 0; 
			{`OPCODE_JRZ, 7'd6}: doneR <= 1;
			{`OPCODE_JRZ, 7'd7}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			{`OPCODE_JRNZ, 7'd4}:
				if (flagsR[`FLAG_ID_Z] == 0) jump_relative(); else doneR <= 1;
			{`OPCODE_JRNZ, 7'd5}: pc_enableR <= 0; 
			{`OPCODE_JRNZ, 7'd6}: doneR <= 1;
			{`OPCODE_JRNZ, 7'd7}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			{`OPCODE_JRS, 7'd4}: 
				if (flagsR[`FLAG_ID_S] == 1) jump_relative(); else doneR <= 1; 
			{`OPCODE_JRS, 7'd5}: pc_enableR <= 0; 
			{`OPCODE_JRS, 7'd6}: doneR <= 1;
			{`OPCODE_JRS, 7'd7}: ;
			
			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			{`OPCODE_JRNS, 7'd4}:
				if (flagsR[`FLAG_ID_S] == 0) jump_relative(); else doneR <= 1;
			{`OPCODE_JRNS, 7'd5}: pc_enableR <= 0; 
			{`OPCODE_JRNS, 7'd6}: doneR <= 1;
			{`OPCODE_JRNS, 7'd7}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			{`OPCODE_JA, 7'd4}: pc_step();
			{`OPCODE_JA, 7'd5}: pc_enableR <= 0;
			{`OPCODE_JA, 7'd6}: begin
				iw2R = ram_code_dout;
				if (flagidR == 0)
					 jump_absolute(ram_code_dout);
				else
					case ( {flagidR, flagscR} )
						{`FLAG_ID_Z, `SET}: if (flagsR[`FLAG_ID_Z]) jump_absolute(ram_code_dout); else doneR <= 1;
						{`FLAG_ID_Z, `CLR}: if ( ! flagsR[`FLAG_ID_Z]) jump_absolute(ram_code_dout); else doneR <= 1;
						{`FLAG_ID_S, `SET}: if (flagsR[`FLAG_ID_S]) jump_absolute(ram_code_dout); else doneR <= 1;
						{`FLAG_ID_S, `CLR}: if ( ! flagsR[`FLAG_ID_S]) jump_absolute(ram_code_dout); else doneR <= 1;
						default: faultR <= 1;
					endcase
				end
			{`OPCODE_JA, 7'd7}: pc_enableR <= 0;
			{`OPCODE_JA, 7'd8}: doneR <= 1;
			{`OPCODE_JA, 7'd9}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			{`OPCODE_RET, 7'd4}: begin
				if (flagidR == 0)
					 ram_data_addrR <= sp;
				else
					case ( {flagidR, flagscR} )
						{`FLAG_ID_Z, `SET}: if (flagsR[`FLAG_ID_Z]) ram_data_addrR <= sp; else doneR <= 1;
						{`FLAG_ID_Z, `CLR}: if ( ! flagsR[`FLAG_ID_Z]) ram_data_addrR <= sp; else doneR <= 1;
						{`FLAG_ID_S, `SET}: if (flagsR[`FLAG_ID_S]) ram_data_addrR <= sp; else doneR <= 1;
						{`FLAG_ID_S, `CLR}: if ( ! flagsR[`FLAG_ID_S]) ram_data_addrR <= sp; else doneR <= 1;
						default: faultR <= 1;
					endcase
				end
			{`OPCODE_RET, 7'd5}: ;
			{`OPCODE_RET, 7'd6}: ;
			{`OPCODE_RET, 7'd7}: begin
				jump_absolute(ram_data_dout);
				sp_addR <= 1;
				sp_alu_enR <= 1;
				end
			{`OPCODE_RET, 7'd8}: begin
				pc_enableR <= 0;
				spR <= sp_new;
				sp_alu_enR <= 0;
				end
			{`OPCODE_RET, 7'd9}: doneR <= 1;
			{`OPCODE_RET, 7'd10}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			{`OPCODE_PUSH, 7'd4}: begin
				sp_addR <= 0;  // pre-decrement sp
				sp_alu_enR <= 1;
				end
			{`OPCODE_PUSH, 7'd5}: begin
				spR <= sp_new;
				sp_alu_enR <= 0;
				if (opt5R) ram_data_dinR <= flagsR; else ram_data_dinR <= rf_douta;
				ram_data_addrR <= sp_new;
				ram_data_weR <= 1;
				end
			{`OPCODE_PUSH, 7'd6}: begin
				ram_data_weR <= 0;
				doneR <= 1;
				end 
			{`OPCODE_PUSH, 7'd7}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			{`OPCODE_POP, 7'd4}: ram_data_addrR <= sp; // begin read
			{`OPCODE_POP, 7'd5}: ;
			{`OPCODE_POP, 7'd6}: begin
				if (opt5R)
					flagsR <= ram_data_dout[2:0];
				else begin
					rf_weaR <= 1;
					rf_dinaR <= ram_data_dout;
				end
				sp_addR <= 1; // post-increment sp
				sp_alu_enR <= 1;
				end
			{`OPCODE_POP, 7'd7}: begin
				save_a_and_done();
				sp_alu_enR <= 0;
				spR <= sp_new;
				end
			{`OPCODE_POP, 7'd8}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			{`OPCODE_IO_GPR, 7'd4}:
				if (opt5R)
					gpio1_byteR <= rf_douta;
				else
					gpio1_strobeR <= 0;
			{`OPCODE_IO_GPR, 7'd5}: begin
				if (opt5R)
			 		gpio1_strobeR <= 1;
				else
					gpio1_byteR <= 8'bz;
				doneR <= 1;	
				end
			{`OPCODE_IO_GPR, 7'd6}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			{`OPCODE_TX_GPR, 7'd4}: begin
				txdataR <= rf_douta;
				txwriteR <= 1;
				end
			{`OPCODE_TX_GPR, 7'd5}: begin
				doneR <= 1;
				txwriteR <= 0;
				end
			{`OPCODE_TX_GPR, 7'd6}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			{`OPCODE_LD_AT, 7'd4}: begin
				rf_weaR <= 1;
				rf_dinaR <= t;
				end
			{`OPCODE_LD_AT, 7'd5}: save_a_and_done();
			{`OPCODE_LD_AT, 7'd6}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			{`OPCODE_ADD_AT, 7'd4}: load_alu_at(`OP_ADD);
			{`OPCODE_ADD_AT, 7'd5}: alu_enR <= 0;
			{`OPCODE_ADD_AT, 7'd6}: ;
			{`OPCODE_ADD_AT, 7'd7}: read_alu_ax();
			{`OPCODE_ADD_AT, 7'd8}: save_a_and_done();
			{`OPCODE_ADD_AT, 7'd9}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			{`OPCODE_SUB_AT, 7'd4}: load_alu_at(`OP_SUB);
			{`OPCODE_SUB_AT, 7'd5}: alu_enR <= 0;
			{`OPCODE_SUB_AT, 7'd6}: ;
			{`OPCODE_SUB_AT, 7'd7}: read_alu_ax();
			{`OPCODE_SUB_AT, 7'd8}: save_a_and_done();
			{`OPCODE_SUB_AT, 7'd9}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			// Same as a subtraction, only we don't call finishing
			// tasks because we don't want to actually subtract, 
			// just want to set the flags as though we did.
			{`OPCODE_CMP_AT, 7'd4}: load_alu_at(`OP_CMP);
			{`OPCODE_CMP_AT, 7'd5}: begin
				alu_enR <= 0;
				end
			{`OPCODE_CMP_AT, 7'd6}:	doneR <= 1;
			{`OPCODE_CMP_AT, 7'd7}: begin
				flagsR[`FLAG_ID_Z] <= Z;
				flagsR[`FLAG_ID_S] <= S;
				end

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			{`OPCODE_MUL_AT, 7'd4}: begin
				which_aluR = `USE_ALU_MULTDIV;
				alu_aR <= rf_douta;
				alu_bR <= t;
				muldiv_actionR <= 1; // choose multiply action
				muldiv_startR <= 1; // activate module
				end
			{`OPCODE_MUL_AT, 7'd5}: muldiv_startR <= 0;
			{`OPCODE_MUL_AT, 7'd6}: ; 
			{`OPCODE_MUL_AT, 7'd7}:  begin
				rf_weaR <= 1;
				rf_dinaR <= alu_md_result;
				end
			{`OPCODE_MUL_AT, 7'd8}: save_a_and_done();
			{`OPCODE_MUL_AT, 7'd9}: begin
				flagsR[`FLAG_ID_Z] <= Z;
				flagsR[`FLAG_ID_S] <= S;
				end

			// // ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			// {`OPCODE_AND_AT, 7'd4}: load_alu_at(`OP_AND);
			// {`OPCODE_AND_AT, 7'd5}: alu_enR <= 0;
			// {`OPCODE_AND_AT, 7'd6}: ;
			// {`OPCODE_AND_AT, 7'd7}: read_alu_ax();
			// {`OPCODE_AND_AT, 7'd8}: save_a_and_done();
			// {`OPCODE_AND_AT, 7'd9}: ;

			// // ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			// {`OPCODE_OR_AT, 7'd4}: load_alu_at(`OP_OR);
			// {`OPCODE_OR_AT, 7'd5}: alu_enR <= 0;
			// {`OPCODE_OR_AT, 7'd6}: ;
			// {`OPCODE_OR_AT, 7'd7}: read_alu_ax();
			// {`OPCODE_OR_AT, 7'd8}: save_a_and_done();
			// {`OPCODE_OR_AT, 7'd9}: ;

			// // ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			// {`OPCODE_XOR_AT, 7'd4}: load_alu_at(`OP_XOR);
			// {`OPCODE_XOR_AT, 7'd5}: alu_enR <= 0;
			// {`OPCODE_XOR_AT, 7'd6}: ;
			// {`OPCODE_XOR_AT, 7'd7}: read_alu_ax();
			// {`OPCODE_XOR_AT, 7'd8}: save_a_and_done();
			// {`OPCODE_XOR_AT, 7'd9}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			{`OPCODE_TX_T, 7'd4}: begin
				txdataR <= t;
				txwriteR <= 1;
				end
			{`OPCODE_TX_T, 7'd5}: begin
				doneR <= 1;
				txwriteR <= 0;
				end
			{`OPCODE_TX_T, 7'd6}: ;

			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			{`OPCODE_IO_T, 7'd4}: begin
				gpio1_byteR <= t;
				end
			{`OPCODE_IO_T, 7'd5}: begin
				gpio1_strobeR <= 1;
				doneR <= 1;	
				end
			{`OPCODE_IO_T, 7'd6}: ;

			// // ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			// // tested
			// {`OPCODE_SV_PTR, 7'd4}: begin
			// 	ram_data_addrR <= rf_douta;
			// 	ram_data_dinR <= rf_doutb;
			// 	ram_data_weR <= 1;
			// 	end
			// {`OPCODE_SV_PTR, 7'd5}: ram_data_weR <= 0;
			// {`OPCODE_SV_PTR, 7'd6}: ;
			// {`OPCODE_SV_PTR, 7'd7}: doneR <= 1;
			// {`OPCODE_SV_PTR, 7'd8}:	;			

			// // ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
			// // tested
			// {`OPCODE_LD_PTR, 7'd4}: begin
			// 	ram_data_addrR <= rf_doutb;
			// 	end
			// {`OPCODE_LD_PTR, 7'd5}: ;
			// {`OPCODE_LD_PTR, 7'd6}: ;
			// {`OPCODE_LD_PTR, 7'd7}: begin
			// 	rf_dinaR <= ram_data_dout;
			// 	rf_weaR <= 1;
			// 	end
			// {`OPCODE_LD_PTR, 7'd8}: save_a_and_done();
			// {`OPCODE_LD_PTR, 7'd9}: ;



			// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪

			default: faultR <= 1;
		endcase
	end
end




// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪

task load_alu_ab(input [3:0] op);
begin
	which_aluR <= `USE_ALU_GENERAL;
	alu_aR <= rf_douta;
	alu_bR <= rf_doutb;
	alu_opR <= op;
	alu_enR <= 1;
end endtask

// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪

task load_alu_at(input [3:0] op);
begin
	which_aluR <= `USE_ALU_GENERAL;
	alu_aR <= rf_douta;
	alu_bR <= t;
	alu_opR <= op;
	alu_enR <= 1;
end endtask

// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪

task read_alu_ax();
begin
	rf_weaR <= 1;
	rf_dinaR <= alu_result;
	flagsR[`FLAG_ID_Z] <= Z;
	flagsR[`FLAG_ID_S] <= S;
end endtask

// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪

task load_regA(input [17:0] value);
begin
	rf_dinaR <= value;
	rf_weaR <= 1;
end endtask

// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪

task save_a_and_done();
begin
	rf_weaR <= 0;
	doneR <= 1;
end endtask

// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪

task pc_step();
begin
	// Just apply a delta of 1 to current address.
	pc_action_stepR <= 1;
	pc_action_jrR <= 0;
	pc_action_jaR <= 0;
	pc_enableR <= 1;
end endtask

// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪

task jump_relative();
begin
	// Relative jumps are done using S.
	pc_action_stepR <= 0;
	pc_action_jrR <= 1;
	pc_action_jaR <= 0;
	pc_enableR <= 1;
end endtask

// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪

task jump_absolute(input [17:0] address);
begin
	// Jump absolute by simply changing addresses with augend value of zero.
	pc_action_stepR <= 0;
	pc_action_jrR <= 0;
	pc_action_jaR <= 1;
	pc_ja_addrR <= address;
	pc_enableR <= 1;
end endtask


endmodule

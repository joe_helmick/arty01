from unittest import TestCase
from vm import VirtualMachine



class TestVirtualMachine(TestCase):

    def setUp(self) -> None:
        self.vm = VirtualMachine()

    def test_initialize_01(self):
        self.vm = VirtualMachine()
        self.assertNotEqual(self.vm, None, 'vm not instantiated')

    def test_LD_GPR_EXPR(self):
        addr = 1000
        self.vm.memory[addr] = 0x0001e
        addr += 1
        self.vm.memory[addr] = 0x00307
        self.vm.PC = addr - 1

        self.vm.run_next_instruction()
        self.assertEquals(self.vm.get_gpr(30), 0x00307, 'ld_gpr_expr failed')

    def test_LD_A_T(self):
        addr = 1000
        self.vm.memory[addr] = 0x20400
        self.vm.PC = addr

        self.vm.run_next_instruction()
        self.assertEquals(self.vm.get_gpr(0), 0x20, 'ld_a_t failed')

    def test_ADD_A_T(self):
        addr = 0xc8
        self.vm.memory[addr] = 0x22026
        self.vm.PC = addr

        self.vm.set_gpr(6, 10)
        self.vm.run_next_instruction()
        self.assertEquals(self.vm.get_gpr(6), 11, 'add_a_t failed')

    def test_SUB_A_T_1(self):
        # 277 00084 24021       ┊    sub @ r_above, 1
        addr = 0x84
        self.vm.memory[addr] = 0x24021
        self.vm.PC = addr

        self.vm.set_gpr(1, 0)
        self.vm.run_next_instruction()
        self.assertEquals(self.vm.get_gpr(1), 0x3ffff, 'sub 1 from zero failed')

    def test_SUB_A_T_2(self):
        # 277 00084 24021       ┊    sub @ r_above, 1
        addr = 0x84
        self.vm.memory[addr] = 0x24021
        self.vm.PC = addr

        self.vm.set_gpr(1, 22)
        self.vm.run_next_instruction()
        self.assertEquals(self.vm.get_gpr(1), 21, 'sub 1 from 22 failed')

    def test_DIV_ABC(self):
        addr = 0x66
        self.vm.memory[addr] = 0x093bf
        addr += 1
        self.vm.memory[addr] = 0x0001c
        self.vm.PC = addr - 1

        self.vm.set_gpr(31, 12034)
        self.vm.set_gpr(29, 10)

        self.vm.run_next_instruction()
        self.assertEquals(self.vm.get_gpr(31), 1203, 'div_abc failed divisor')
        self.assertEquals(self.vm.get_gpr(28), 4, 'div_abc failed remainder')

    def test_LD_GPR_GPTR(self):
        # 533 0012b 00bdf       ┊    ld r31, [r30]
        addr = 0x12b
        self.vm.memory[addr] = 0x00bdf
        self.vm.PC = addr
        val = 7
        ptr = 0x2000
        self.vm.memory[ptr] = val
        self.vm.set_gpr(30, ptr)

        self.vm.run_next_instruction()
        self.assertEquals(self.vm.get_gpr(31), val, 'ld_gpr_ptr failed')

    def test_jrnz_1(self):
        # 000de 0c009       ┊ 	jr.nz   dog_cell_dead
        addr = 0xde
        self.vm.memory[addr] = 0x0c009
        self.vm.flagZ = True

        self.vm.PC = addr
        self.vm.run_next_instruction()
        self.assertEquals(self.vm.PC, addr + 1, 'jrnz 1 failed')

    def test_jrnz_2(self):
        # 000de 0c009       ┊ 	jr.nz   dog_cell_dead
        addr = 0xde
        self.vm.memory[addr] = 0x0c009
        self.vm.flagZ = False

        self.vm.PC = addr
        self.vm.run_next_instruction()
        self.assertEquals(self.vm.PC, 0x000e8, 'jrnz 2 failed')

    def test_cmp_a_t_1(self):
        # 419 000df 26006       ┊ 	cmp     @neighbor_count, 0
        addr = 0xdf
        self.vm.PC = addr
        self.vm.set_gpr(6, 0)
        self.vm.run_next_instruction()
        self.assertEquals(self.vm.get_gpr(6), 0, 'cmp_a_t 1 failed')

    def test_cmp_a_t_2(self):
        # 429 000e5 26066       ┊ 	cmp     @neighbor_count, 3
        addr = 0xe5
        self.vm.PC = addr
        self.vm.set_gpr(6, 3)
        self.vm.run_next_instruction()
        self.assertEquals(self.vm.get_gpr(6), 3, 'cmp_a_t 3 failed')

from antlr4 import *
from JMUAsmVisitorPass2 import *
from JMUAsmVisitorPass1 import *
from JMUStrictErrorListener import *
from JMUEnvironment import JMUEnvironment
from JMUAsmProject import AsmProject
from JMULexer import JMULexer
from os import path, getcwd
import os
from AssemblerLexer import *


#  The global enviroment containing all data structures.
ENV = JMUEnvironment()
PRJ = None
option_write_coe_file = False


def fill_ram_with_instructions():
    for asm_line in PRJ.main_file.asm_lines:
        addr = asm_line.program_counter
        if asm_line.words is None:
            continue
        for word in asm_line.words:
            ENV.ram[addr] = word
            addr += 1
    return


def write_coe_file(filename: str):

    jfn = os.path.basename(filename)
    jfn = jfn.replace('.asm', '.coe')

    file = open('outputs/' + jfn, 'w')
    file.write('memory_initialization_radix=16;\n')
    file.write('memory_initialization_vector=\n')
    for loc in range(0, Arch.MEMORY_DEPTH):
        word = ENV.ram[loc]
        if word is None:
            file.write('{:05x}'.format(0))
        else:
            file.write('{:05x}'.format(word))
        if loc == Arch.MEMORY_DEPTH - 1:
            file.write(';')
        else:
            file.write(',\n')
    file.close()
    return


def do_lex_parse_setup(filename):
    input_stream = FileStream(filename, encoding='utf-8')
    lexer = JMULexer(ENV.errors, input_stream)
    ENV.token_stream = CommonTokenStream(lexer)
    parser = AssemblerParser(ENV.token_stream)
    parser.removeErrorListeners()
    parser.addErrorListener(StrictErrorListener(ENV.errors))
    ENV.parse_tree = parser.program()  # "program" is the start rule, the topmost


def do_lex_parse_1():
    visitor = JMUAsmVisitorPass1(ENV, PRJ)
    visitor.visit(ENV.parse_tree)


def do_lex_parse_2():
    visitor = JMUAsmVisitorPass2(ENV, PRJ)
    visitor.visit(ENV.parse_tree)


def main(filename):
    global PRJ

    # This is the name of the final generated assembly listing
    final_filename = '__main.asm'
    PRJ = AsmProject(ENV.errors, final_filename)

    # Load up the main source file.
    PRJ.load(filename, ismain=True)

    # Set up parser and lexer.
    try:
        do_lex_parse_setup(final_filename)
    except CustomException as ex:
        ENV.errors.append(ex.__str__())

    # Pass 1, address and symbol resolution.
    try:
        do_lex_parse_1()
    except CustomException as ex:
        if isinstance(ex, Error):
            ENV.errors.append(ex.__str__())
        if isinstance(ex, Advisory):
            ENV.advisories.append(ex.__str__())

    if len(ENV.errors) > 0:
        print()
        print(ENV.program_header("PASS 1"))
        print(PRJ.main_file.get_listing())
        print(ENV.program_footer("PASS 1"))
        ENV.print_errors_and_warnings()
        ENV.show_assembly_result()

        if ENV.dirPrintSymbolTable:
            ENV.print_symbol_dictionary()
        if len(ENV.errors) > 0:
            return

    # Pass 2, instruction encoding.
    try:
        do_lex_parse_2()
    except CustomException as ex:
        if isinstance(ex, Error):
            ENV.errors.append(ex.__str__())
        if isinstance(ex, Advisory):
            ENV.advisories.append(ex.__str__())

    print()
    print(ENV.program_header("PASS 2"))
    print(PRJ.main_file.get_listing())
    print(ENV.program_footer("PASS 2"))
    PRJ.check_unencoded_instructions()
    ENV.print_errors_and_warnings()
    ENV.show_assembly_result()

    if ENV.dirPrintSymbolTable:
        ENV.print_symbol_dictionary()

    if ENV.dirPrintDecoding:
        PRJ.show_binary_breakdown()

    # If there are no errors, then do finish-up stuff to do post-build
    # operations, like RAM image for Vivado, and a listing file for
    # the virtual machine simulator
    if len(ENV.errors) == 0:

        # Write the RAM file for the simulator.
        fill_ram_with_instructions()
        ENV.ram_dump()

        # Write the listing file for the simulator.
        listfile = open('__main.lst', 'w', encoding='utf-8', newline='\n')
        listfile.write(PRJ.main_file.get_listing(),)
        listfile.close()

        # Write COE file for Vivado RAM imaging.
        if option_write_coe_file:
            write_coe_file(PRJ.main_file.filename)

    return


def check_args(argv):
    global option_write_coe_file

    # Check for number of command-line arguments, exit with message if incorrect.
    if len(argv) != 3:  # Two arguments, [0] is the command itself.
        print('ERROR: command arguments:')
        print('args = --mainfile=<main asm filename>  --output_coe=[yes|no]')
        exit(1)

    # Compute the full path of the main asm file.
    start_filename = argv[1].replace('--mainfile=', '').strip()
    full_path = getcwd() + path.sep + start_filename

    ENV.vlog_tst_pgm = start_filename.replace('.asm', '') + '.v'

    if not path.exists(full_path):
        print('ERROR: main file', full_path, ' does not exist.')
        exit(1)

    # Check the --write-coe argument.
    write_coe = argv[2].replace('--output-coe=', '').strip()
    if write_coe != 'yes' and write_coe != 'no':
        print('ERROR: --write-coe must be either yes or no.')
        exit(1)

    # Set global variable based on --write-coe argument.
    if write_coe == 'yes':
        option_write_coe_file = True
    else:
        option_write_coe_file = False

    return full_path


if __name__ == '__main__':
    full_path = check_args(sys.argv)
    print("ASSEMBLING ", full_path)
    main(full_path)
    exit(0)

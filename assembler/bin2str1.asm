.data $1000 numbufstruct
{
    ; $1000 1001 1002 1003 1004 1005 1006
    buffer  w   7
}

.org 0
    jr main
;----------------------------------------------------------------------
bin2str:
;----------------------------------------------------------------------
    ; input r01 has value to convert to string
    ; output buffer hold string value
;----------------------------------------------------------------------
clearbuffer:
    ; clear buffer by filling with nulls first
    ld r00, $20
    sv (buffer + 0), r00
    sv (buffer + 1), r00
    sv (buffer + 2), r00
    sv (buffer + 3), r00
    sv (buffer + 4), r00
    sv (buffer + 5), r00
    ld r00, 0
    sv (buffer + 6), r00
setptrones:
    ldw r04, buffer + 5 ; point to the digits
loadvalue:
    ld  r02, 10         ; r02 holds the divisor
divby10:
    div r01, r02, r03   ; r03 gets remainder
convrem:
    add r03, $30        ; add $30 to get ASCII value
    sv [r04], r03       ;
    dec r04             ; move to the left one place for next digit
    cmp r01, 0          ;
    jr.nz divby10
    ret
;----------------------------------------------------------------------
main:
    ldw r01, 261045
    call bin2str
    ldw     r02, buffer
showcurchar:
    ld      r03, [r02]
    cmp     r03, 0
    jr.z    main
    io1.on  r03
    io1.off r03
    inc     r02
    jr showcurchar

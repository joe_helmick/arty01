from setuptools import setup

setup(
    name='assembler',
    version='1.0',
    packages=[''],
    url='',
    license='NONE All RIghts Reserved',
    author='Joe Helmick',
    author_email='joe_helmick@comcast.net',
    description='assembler for custom FPGA-based microprocessor'
)

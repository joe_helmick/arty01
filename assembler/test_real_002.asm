.equ stack_top $2000

.data $200 datablock
{
    var1    w   1   fill    $3ffff
}
.string name "Joe"

.org 0
    ja      start       ;                                   ; i_ja

;-----------------------------------------------------------------------
signal_2:
    push    r02         ; sp decrements, again
    ld      r02, $15    ; r02 <= $15
    io1.on  r02         ; io on show $15                   i_io1_on
    pop     r02         ; sp incs, r02 <= $a3
    io1.on  r02         ; io on show $a3                   i_io1_on
    io1.off r02         ; io off                           i_io1_off
    ret                 ;                                  i_ret
;-----------------------------------------------------------------------

;-----------------------------------------------------------------------
start:
    ld      sp, stack_top ; sp = $2000                     i_ld_sp
    ld      r00, 1      ;                                  i_ld_a_t
    ld      r01, 0
    ld      r02, 0

    add     r00, 2      ; 3                                i_add_a_t
    sub     r00, 1      ; 2                                i_sub_a_t
    inc     r00         ; 3                                i_incdec 1
    dec     r00         ; 2                                i_incdec 0
    copy    r01, r00    ; 2 2                              i_copy_ab

    add     r00, r01    ; 4 2                              i_add_ab
    sub     r00, r01    ; 2 2                              i_sub_ab

    ld      r02, $9a    ; r02 <= $9a
    call    signal_1    ; far (forward) call               i_call_far

    ld      r02, $a3    ; r02 <= $a3
    call    signal_2    ; near (close backward) call       i_call_near

    ldw     r00, var1   ; r00 points at var 1, addr $200   i_ld_gpr_expr
    ld      r01, [r00]  ; r01 <= $ff                       i_ld_ptr
    dec     r01         ; r01 <= $fe
    sv      [r00], r01  ; save value                       i_sv_ptr
    ld      r01, $00    ; r01 <= 0 reset the value
    ld      r01, [r00]  ; r01 <= $fe proving the save worked
    jr      start       ;                                  i_jr
;-----------------------------------------------------------------------

;-----------------------------------------------------------------------
signal_1:
    push    r02         ; sp decrements, again             i_push_reg
    ld      r02, $17    ; r02 <= $17                       i_ld_a_t
    io1.on  r02         ; io on show $17                   i_io1_on
    io1.off r02         ; io off                           i_io1_off
    pop     r02         ; sp incs, r02 <= $9a              i_pop_reg
    ret                 ;                                  i_ret
;-----------------------------------------------------------------------

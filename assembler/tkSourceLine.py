from enum import Enum


class BreakPoint(Enum):
    CLEAR = 0
    SET = 1
    DISABLED = 2


class SourceLine:
    number = None
    text = None
    bp = BreakPoint.CLEAR
    upd = BreakPoint.CLEAR
    iw1 = None
    iw2 = None
    pc = None
    word_count = 0

    def __init__(self, number, text):
        self.number = number
        self.text = text
        self.bp = BreakPoint.CLEAR
        self.upd = BreakPoint.CLEAR

        pcstring = text[6:11]
        if len(pcstring.strip()) == 0:
            self.pc = None
        else:
            self.pc = int(pcstring, 16)

        iw1string = text[12:17]
        if len(iw1string.strip()) == 0:
            self.iw1 = None
        else:
            self.iw1 = int(iw1string, 16)
            self.word_count += 1

        iw2string = text[18:24]
        if len(iw2string.strip()) == 0:
            self.iw2 = None
        else:
            self.iw2 = int(iw2string, 16)
            self.word_count += 1

    def bp_cycle(self):
        if self.bp == BreakPoint.CLEAR:
            self.bp = BreakPoint.SET
            return
        if self.bp == BreakPoint.SET:
            self.bp = BreakPoint.DISABLED
            return
        if self.bp == BreakPoint.DISABLED:
            self.bp = BreakPoint.CLEAR
            return
        return

    def upd_cycle(self):
        if self.upd == BreakPoint.CLEAR:
            self.upd = BreakPoint.SET
            return
        if self.upd == BreakPoint.SET:
            self.upd = BreakPoint.DISABLED
            return
        if self.upd == BreakPoint.DISABLED:
            self.upd = BreakPoint.CLEAR
            return
        return

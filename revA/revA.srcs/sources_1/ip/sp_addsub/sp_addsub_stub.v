// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (win64) Build 2708876 Wed Nov  6 21:40:23 MST 2019
// Date        : Sun Jun 14 09:09:58 2020
// Host        : DESKTOP-THNB40S running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               D:/dev-attic/microprocessors/Arty01/revA/revA.srcs/sources_1/ip/sp_addsub/sp_addsub_stub.v
// Design      : sp_addsub
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "c_addsub_v12_0_14,Vivado 2019.2" *)
module sp_addsub(A, ADD, CE, S)
/* synthesis syn_black_box black_box_pad_pin="A[17:0],ADD,CE,S[17:0]" */;
  input [17:0]A;
  input ADD;
  input CE;
  output [17:0]S;
endmodule

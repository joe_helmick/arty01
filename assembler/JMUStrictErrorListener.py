from antlr4.error.ErrorListener import *
# from antlr4.error.ErrorStrategy import *


class StrictErrorListener(ErrorListener):

    def __init__(self, errors):
        self.errors = errors

    def syntaxError(self, recognizer, offendingSymbol, line, column, msg, e):
        message = "SYNTAX ERROR: Line {0} col {1}: {2}".format(line, column, msg)
        self.errors.append(message)

    def reportAmbiguity(self, recognizer, dfa, startIndex, stopIndex, exact, ambigAlts, configs):
        # pass
        message = "AMBIGUOUS at " + str(startIndex) + " " + str(stopIndex)
        self.errors.append(message)

    def reportAttemptingFullContext(self, recognizer, dfa, startIndex, stopIndex, conflictingAlts, configs):
        # pass
        message = "FULLCONTEXT at " + str(startIndex) + " " + str(stopIndex)
        self.errors.append(message)

    def reportContextSensitivity(self, recognizer, dfa, startIndex, stopIndex, prediction, configs):
        # pass
        message = "SENSITVE at "
        self.errors.append(message)


# class JMUBailErrorStrategy(BailErrorStrategy):
#
#     def __init__(self):
#         super().__init__()
#
#     def recover(self, e: RecognitionException):
#
#         self.errors.append('BAIL ' + e.offendingToken)
#
#         super().recover()
#
#         context = recognizer._ctx
#         while context is not None:
#             context.exception = e
#             context = context.parentCtx
#         raise ParseCancellationException(e)

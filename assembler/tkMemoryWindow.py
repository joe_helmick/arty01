import tkinter as tk
from tkKonstants import tkkuistandards as tkk
import sys


class MemoryWindow(tk.Toplevel):
    root = None
    tkk = None
    WIN_WID = None
    WIN_HEI = None

    ROWS = 0
    COLS = 0
    addr = 0
    COUNT = 0
    RAMSIZE = 0
    MAX_BASE_ADDR = 0

    cnvmem = None
    char_pix = 0
    font1 = None
    wid_header = 0
    wid_data = 0

    mode = None
    etLocation = None

    def compute_win_size(self):
        ADDR_CHARS = 6  # 'xxxxx '
        VAL_CHARS = 6   # 'xxxxx '
        EXTRA_WID = 18
        EXTRA_HEI = 20
        MIN_WID = 240
        self.char_pix = self.tkk.font1.measure("0")
        ASCII_WID = (self.COLS + 1) * self.char_pix

        self.wid_header = self.char_pix * ADDR_CHARS
        self.wid_data = self.char_pix * VAL_CHARS
        # EXAMPLE 8 VALUE LINE
        # $00000: 00000 00000 00000 00000 00000 00000 00000 00000
        w1 = self.char_pix * ADDR_CHARS
        w2 = self.char_pix * VAL_CHARS * self.COLS
        self.WIN_WID = w1 + w2 + EXTRA_WID
        self.WIN_WID += ASCII_WID
        self.WIN_WID = max(self.WIN_WID, MIN_WID)

        self.WIN_HEI = (self.tkk.HEI_LINE * self.ROWS) + EXTRA_HEI
        self.WIN_HEI += self.tkk.HEI_LINE  # add one extra line to print offsets
        self.WIN_HEI += 32  # TODO extra space for search field
        return


    def on_closing(self):
        if self.mode == self.tkk.FROM_RAMTOP:
            self.root.win_stack.destroy()
            self.root.win_stack = None
        else:
            self.root.win_ram.destroy()
            self.root.win_ram = None
        return


    def callback_search(self, event):
        sval = self.etLocation.get('1.0', 'end-1c')
        try:
            # Convert the input number to hex integer
            hexval = int(sval, 16)
            # Compute offset from valid row base address.
            offset = hexval % self.COLS
            # Now set our address to this new address and show listing.
            self.addr = hexval - offset
            self.constrain()
            self.paint(addrChanged=None, addrTarget=hexval)
        except ValueError:
            print('exception in callback_search()', sys.exc_info()[0])
        finally:
            self.etLocation.delete('1.0', 'end')


    def __init__(self, root, title, addr, rows, cols, vm, mode):
        super().__init__()
        # Get a reference to the tk constants class.
        self.tkk = tkk()
        # Set the parent window.
        self.root = root
        # Set a reference to the virtual machine.
        self.vm = vm
        # Compute and set some constants and computed constants.
        self.RAMSIZE = len(vm.memory)
        self.ROWS = rows
        self.COLS = cols
        self.COUNT = rows * cols
        self.MAX_BASE_ADDR = self.RAMSIZE - self.COUNT
        # Set the starting address based on mode (memory or stack window).
        self.mode = mode
        if mode == self.tkk.FROM_RAMTOP:
            self.addr = self.RAMSIZE
            self.constrain()
        else:
            self.addr = addr
        # Configure the window.
        self.title(title)
        self.configure(bg=tkk.COLOR_MAIN_WINDOW)
        self.resizable(width=False, height=False)
        self.compute_win_size()
        self.geometry('{0}x{1}+{2}+{3}'.format(self.WIN_WID, self.WIN_HEI, 0, 0))
        # Create the address search field
        self.etLocation = tk.Text(self, bg=self.tkk.COLOR_BG_DEFAULT, fg=self.tkk.COLOR_PC,
                                  bd=1, highlightthickness=1, font=self.tkk.font1,
                                  width=7, height=1, relief=tk.FLAT, insertbackground=self.tkk.COLOR_PC)
        self.etLocation.pack(pady=(6, 0))
        self.etLocation.bind('<Return>', self.callback_search)
        # Create and configure the canvas.
        self.cnvmem = tk.Canvas(self, bg=tkk.COLOR_BG_DEFAULT, bd=1, highlightthickness=1)
        self.cnvmem.pack(fill=tk.BOTH, expand=1, padx=6, pady=6)
        self.cnvmem.bind('<MouseWheel>', self.callback_wheel)
        # Finish up.
        self.protocol('WM_DELETE_WINDOW', self.on_closing)
        self.paint(addrChanged=None, addrTarget=None)
        return


    def callback_wheel(self, event):
        WHEEL_SCROLL_LINES = 10
        MAGIC_WHEEL_DELTA = 120
        scrollamount = WHEEL_SCROLL_LINES * self.COLS

        if event.delta == MAGIC_WHEEL_DELTA:
            # wheel up, reduce address
            self.addr -= scrollamount
        else:
            # wheel down, increase address
            self.addr += scrollamount

        self.constrain()
        self.paint()
        return


    def constrain(self):
        # Scrolled off the top, so reset to top.
        if self.addr < 0:
            self.addr = 0
            return

        # Scrolled past the bottom, so reset address so that the max base
        # address is the last one showed
        if self.addr >= self.MAX_BASE_ADDR:
            self.addr = self.MAX_BASE_ADDR
            return


    def drawbox(self, x1, y1, color):
        x1 = x1 - 4
        x2 = x1 + self.wid_data - self.char_pix + 7
        y2 = y1 + self.tkk.HEI_LINE
        self.cnvmem.create_line(x1, y1,
                                x2, y1,
                                x2, y2,
                                x1, y2,
                                x1, y1,
                                fill=color, width=3, joinstyle=tk.ROUND)
        return


    @staticmethod
    def map_display_character(char):
        ESC_CHAR = '\u2588'
        NULL_CHAR = '\u2591'
        UNKNOWN_CHAR = '\u25a0'

        if char == 0:
            return NULL_CHAR
        if char == 27:
            return ESC_CHAR
        if 32 <= char <= 127:
            return chr(char)
        else:
            return UNKNOWN_CHAR

    def paint(self, addrChanged=None, addrTarget=None):
        # Delete all canvas items to prevent leaks.
        self.cnvmem.delete('all')
        # Paint the column offsets
        ypos = 0
        xpos = self.wid_header
        text = ''
        for offset in range(0, self.COLS):
            text += '  {0:x}   '.format(offset)  # this is dependent on width of header
        self.cnvmem.create_text(xpos, ypos, text=text, font=self.tkk.font1, anchor=tk.NW, fill=tkk.COLOR_PC)
        # Advance one line to start painting the rows.
        ypos += self.tkk.HEI_LINE

        # Paint each of the rows now.
        for r in range(self.ROWS):
            # Paint this row's base address.
            addr_start = self.addr + (r * self.COLS)
            xpos = 5
            text = '{0:05x} '.format(addr_start)
            self.cnvmem.create_text(xpos, ypos, text=text, font=self.tkk.font1, anchor=tk.NW, fill=tkk.COLOR_PC)
            # Move to the right to start painting data.
            xpos += self.wid_header
            # Print the values at each offset address

            ascii_line = ''

            for c in range(self.COLS):
                color = tkk.COLOR_TXT_MEM_DEFAULT
                addr = c + addr_start

                if addrChanged is not None:
                    if addr == addrChanged:
                        color = tkk.COLOR_TXT_CHANGED

                xpos = self.wid_header + (c * self.wid_data)

                val = self.vm.memory[addr]
                text = '{0:05x}'.format(val)
                ascii_line += self.map_display_character(val)
                self.cnvmem.create_text(xpos, ypos, text=text, font=self.tkk.font1, anchor=tk.NW, fill=color)

                if addrTarget is not None:
                    if addr == addrTarget:
                        self.drawbox(xpos, ypos, self.tkk.COLOR_TXT_CHANGED)

            # Move y position to next line.
            xpos += (self.char_pix * 6)

            self.cnvmem.create_text(xpos, ypos, text=ascii_line, font=self.tkk.font1, anchor=tk.NW, fill=self.tkk.COLOR_TXT_DEFAULT)

            ypos += self.tkk.HEI_LINE
        return




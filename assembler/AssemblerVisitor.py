# Generated from ..\Assembler.g4 by ANTLR 4.8
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .AssemblerParser import AssemblerParser
else:
    from AssemblerParser import AssemblerParser

# This class defines a complete generic visitor for a parse tree produced by AssemblerParser.

class AssemblerVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by AssemblerParser#program.
    def visitProgram(self, ctx:AssemblerParser.ProgramContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#end.
    def visitEnd(self, ctx:AssemblerParser.EndContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#blockDirective.
    def visitBlockDirective(self, ctx:AssemblerParser.BlockDirectiveContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#blockLabel.
    def visitBlockLabel(self, ctx:AssemblerParser.BlockLabelContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#blockInstruction.
    def visitBlockInstruction(self, ctx:AssemblerParser.BlockInstructionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_ld_gpr_expr.
    def visitI_ld_gpr_expr(self, ctx:AssemblerParser.I_ld_gpr_exprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_ld_gpr_t.
    def visitI_ld_gpr_t(self, ctx:AssemblerParser.I_ld_gpr_tContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_ld_gpr_gptr.
    def visitI_ld_gpr_gptr(self, ctx:AssemblerParser.I_ld_gpr_gptrContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_ld_gpr_sptr.
    def visitI_ld_gpr_sptr(self, ctx:AssemblerParser.I_ld_gpr_sptrContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_copy_gpr_gpr.
    def visitI_copy_gpr_gpr(self, ctx:AssemblerParser.I_copy_gpr_gprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_ld_sp_expr.
    def visitI_ld_sp_expr(self, ctx:AssemblerParser.I_ld_sp_exprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_sv_expr_gpr.
    def visitI_sv_expr_gpr(self, ctx:AssemblerParser.I_sv_expr_gprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_sv_gptr_gpr.
    def visitI_sv_gptr_gpr(self, ctx:AssemblerParser.I_sv_gptr_gprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_sv_sptr_gpr.
    def visitI_sv_sptr_gpr(self, ctx:AssemblerParser.I_sv_sptr_gprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_sv_gptr_expr.
    def visitI_sv_gptr_expr(self, ctx:AssemblerParser.I_sv_gptr_exprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_call.
    def visitI_call(self, ctx:AssemblerParser.I_callContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_add_gpr_t.
    def visitI_add_gpr_t(self, ctx:AssemblerParser.I_add_gpr_tContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_add_gpr_gpr.
    def visitI_add_gpr_gpr(self, ctx:AssemblerParser.I_add_gpr_gprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_sub_gpr_t.
    def visitI_sub_gpr_t(self, ctx:AssemblerParser.I_sub_gpr_tContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_sub_gpr_gpr.
    def visitI_sub_gpr_gpr(self, ctx:AssemblerParser.I_sub_gpr_gprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_cmp_gpr_t.
    def visitI_cmp_gpr_t(self, ctx:AssemblerParser.I_cmp_gpr_tContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_cmp_gpr_gpr.
    def visitI_cmp_gpr_gpr(self, ctx:AssemblerParser.I_cmp_gpr_gprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_mul_gpr_t.
    def visitI_mul_gpr_t(self, ctx:AssemblerParser.I_mul_gpr_tContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_mul_gpr_gpr.
    def visitI_mul_gpr_gpr(self, ctx:AssemblerParser.I_mul_gpr_gprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_div_gpr_gpr_gpr.
    def visitI_div_gpr_gpr_gpr(self, ctx:AssemblerParser.I_div_gpr_gpr_gprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_jr.
    def visitI_jr(self, ctx:AssemblerParser.I_jrContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_ja.
    def visitI_ja(self, ctx:AssemblerParser.I_jaContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_ret.
    def visitI_ret(self, ctx:AssemblerParser.I_retContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_push_flags.
    def visitI_push_flags(self, ctx:AssemblerParser.I_push_flagsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_push_gpr.
    def visitI_push_gpr(self, ctx:AssemblerParser.I_push_gprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_pop_flags.
    def visitI_pop_flags(self, ctx:AssemblerParser.I_pop_flagsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_pop_gpr.
    def visitI_pop_gpr(self, ctx:AssemblerParser.I_pop_gprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_tx_gpr.
    def visitI_tx_gpr(self, ctx:AssemblerParser.I_tx_gprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_io_gpr.
    def visitI_io_gpr(self, ctx:AssemblerParser.I_io_gprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_tx_t.
    def visitI_tx_t(self, ctx:AssemblerParser.I_tx_tContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_io_t.
    def visitI_io_t(self, ctx:AssemblerParser.I_io_tContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_io_off.
    def visitI_io_off(self, ctx:AssemblerParser.I_io_offContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#i_rx_gpr.
    def visitI_rx_gpr(self, ctx:AssemblerParser.I_rx_gprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#ioconstant.
    def visitIoconstant(self, ctx:AssemblerParser.IoconstantContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#flagcond_z.
    def visitFlagcond_z(self, ctx:AssemblerParser.Flagcond_zContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#flagcond_nz.
    def visitFlagcond_nz(self, ctx:AssemblerParser.Flagcond_nzContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#flagcond_s.
    def visitFlagcond_s(self, ctx:AssemblerParser.Flagcond_sContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#flagcond_ns.
    def visitFlagcond_ns(self, ctx:AssemblerParser.Flagcond_nsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#gprptr.
    def visitGprptr(self, ctx:AssemblerParser.GprptrContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#symptr.
    def visitSymptr(self, ctx:AssemblerParser.SymptrContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#gpr.
    def visitGpr(self, ctx:AssemblerParser.GprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#charExpr.
    def visitCharExpr(self, ctx:AssemblerParser.CharExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#powerExpr.
    def visitPowerExpr(self, ctx:AssemblerParser.PowerExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#numberExpr.
    def visitNumberExpr(self, ctx:AssemblerParser.NumberExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#plusMinusExpr.
    def visitPlusMinusExpr(self, ctx:AssemblerParser.PlusMinusExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#multDivExpr.
    def visitMultDivExpr(self, ctx:AssemblerParser.MultDivExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#symbolExpr.
    def visitSymbolExpr(self, ctx:AssemblerParser.SymbolExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#parenExpr.
    def visitParenExpr(self, ctx:AssemblerParser.ParenExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#number.
    def visitNumber(self, ctx:AssemblerParser.NumberContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#directive.
    def visitDirective(self, ctx:AssemblerParser.DirectiveContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#bindata.
    def visitBindata(self, ctx:AssemblerParser.BindataContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#bindataItemNumber.
    def visitBindataItemNumber(self, ctx:AssemblerParser.BindataItemNumberContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#bindataItemChar.
    def visitBindataItemChar(self, ctx:AssemblerParser.BindataItemCharContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#bindataList.
    def visitBindataList(self, ctx:AssemblerParser.BindataListContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#printEachLine.
    def visitPrintEachLine(self, ctx:AssemblerParser.PrintEachLineContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#printSymbolTable.
    def visitPrintSymbolTable(self, ctx:AssemblerParser.PrintSymbolTableContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#printDecoding.
    def visitPrintDecoding(self, ctx:AssemblerParser.PrintDecodingContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#equate.
    def visitEquate(self, ctx:AssemblerParser.EquateContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#org.
    def visitOrg(self, ctx:AssemblerParser.OrgContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#aliasdef.
    def visitAliasdef(self, ctx:AssemblerParser.AliasdefContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#struct.
    def visitStruct(self, ctx:AssemblerParser.StructContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#structdecl.
    def visitStructdecl(self, ctx:AssemblerParser.StructdeclContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#defvarOriginNumber.
    def visitDefvarOriginNumber(self, ctx:AssemblerParser.DefvarOriginNumberContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#defvarOriginSymbol.
    def visitDefvarOriginSymbol(self, ctx:AssemblerParser.DefvarOriginSymbolContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#defvarOriginNext.
    def visitDefvarOriginNext(self, ctx:AssemblerParser.DefvarOriginNextContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#structname.
    def visitStructname(self, ctx:AssemblerParser.StructnameContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#vardecl.
    def visitVardecl(self, ctx:AssemblerParser.VardeclContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#include.
    def visitInclude(self, ctx:AssemblerParser.IncludeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#defstring.
    def visitDefstring(self, ctx:AssemblerParser.DefstringContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#stringpartcollection.
    def visitStringpartcollection(self, ctx:AssemblerParser.StringpartcollectionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#stringPartExpr.
    def visitStringPartExpr(self, ctx:AssemblerParser.StringPartExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#stringPartString.
    def visitStringPartString(self, ctx:AssemblerParser.StringPartStringContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by AssemblerParser#stringPartChar.
    def visitStringPartChar(self, ctx:AssemblerParser.StringPartCharContext):
        return self.visitChildren(ctx)



del AssemblerParser
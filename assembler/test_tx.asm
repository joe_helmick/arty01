.org 0
	ja      main

.string phrase1 "Joe Helmick rules! ", $00

;----------------------------------------------------------------------
main:
;----------------------------------------------------------------------
	ld      r31, phrase1    ; load pointer to beginning of string
nextch:
	ld      r30, [r31]      ; load the character pointed at
	cmp     r30, $00        ; if null then done, so
	jr.z    main            ; repeat loop
	io1.on  r30             ; show on io port
	tx      r30             ; send to serial port
	io1.off r30             ; turn off io port
	add     r31, 1          ; move pointer
	jr      nextch          ; keep going


.lstdecoding
.lstprogress
.lstsymtable

.org $100
.struct next struct1
{
	buffer                  w 20    fill $a5
	formula_head            w 1     fill $32
	formula_ptr             w 1     fill 0
	generation              w 1     fill 0
}


.include "goldParser.asm"

.string message "This Micro Works Better", 0

.org $00000
start:

	; reset

	ldw     r20, 1100
	ld      r21, 11
	ld      r22, 0

	div     r20, r21, r22

	; LD_GPR_EXPR -> r20 is a pointer to message
	ldw     r20, message

	; LD_GPR_GPTR -> r21 gets character at pointer
	ld      r21, [r20]
	io      r21
	io-

	; LD_GPR_SPTR -> r22 gets value of variable
	ld      r22, [generation]

;	; SV_GPTR_GPR -> add one to variable

	; SV_SPTR_GPR
	add     r22, 1
	sv [generation], r22

;
;	; SV_GPTR_EXPR

	jr start


	ldw     r14, 1000
	shr     r14, 2
	shl     r14, 1

.end
// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
module regfile (
// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
	input wire clock,
	input wire wea,
	input wire wec,
	input wire [4:0] addra,
	input wire [4:0] addrb,
	input wire [4:0] addrc,
	input wire [17:0] dina,
	input wire [17:0] dinc,
	output reg [17:0] douta,
	output reg [17:0] doutb,
	output reg [17:0] doutc
);

reg [17:0] registers [0:31];

assign douta = registers[addra];
assign doutb = registers[addrb];
assign doutc = registers[addrc];

always @ ( posedge clock )
begin
	if (wea) registers[addra] <= dina; // else douta <= registers[addra];
	if (wec) registers[addrc] <= dinc; //else doutc <= registers[addrc];
end
endmodule

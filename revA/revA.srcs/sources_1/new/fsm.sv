`include "arch_defines.vh"

// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
module fsm (
// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
	input wire clock,
	input wire reset,
	input wire done,
	output wire [6:0] fsm_state,
	input wire waiting
);
	reg [6:0] state_nxt, state_cur, fsm_stateR;
	assign fsm_state = fsm_stateR;

	// assign next state
	always @ ( posedge clock )
	begin
		if (reset)
			state_cur <= `FSM_0;
		else
			state_cur <= state_nxt; // advance to next state
	end

	// decide next state
	always @ ( state_cur, done, waiting ) begin
		state_nxt = 'bx; 
		if (done)
			state_nxt = `FSM_LD_IW1_INIT;
		else
			if (waiting) // don't advance if waiting
				state_nxt = state_cur;
			else
				case (state_cur) // 
					`FSM_0: state_nxt = `FSM_LD_IW1_INIT; 
					`FSM_LD_IW1_INIT: state_nxt = `FSM_LD_IW1_WAIT;
					`FSM_LD_IW1_WAIT: state_nxt = `FSM_DECODE;
					`FSM_DECODE: state_nxt = 7'd4;
					7'd4: state_nxt = 7'd5;
					7'd5: state_nxt = 7'd6;
					7'd6: state_nxt = 7'd7;
					7'd7: state_nxt = 7'd8;
					7'd8: state_nxt = 7'd9;
					7'd9: state_nxt = 7'd10;
					7'd10: state_nxt = 7'd11;
					7'd11: state_nxt = 7'd12;
					7'd12: state_nxt = 7'd13;
					7'd13: state_nxt = 7'd14;
					7'd14: state_nxt = 7'd15;
					7'd15: state_nxt = 7'd16;
					7'd16: state_nxt = 7'd17;
					7'd17: state_nxt = 7'd18;
					7'd18: state_nxt = 7'd19;
					7'd19: state_nxt = 7'd20;
					7'd20: state_nxt = 7'd21;
					7'd21: state_nxt = 7'd22;
					7'd22: state_nxt = 7'd23;
					7'd23: state_nxt = 7'd24;
					7'd24: state_nxt = 7'd25;
					7'd25: state_nxt = 7'd26;
					7'd26: state_nxt = 7'd27;
					7'd27: state_nxt = 7'd28;
					7'd28: state_nxt = 7'd29;
					7'd29: state_nxt = 7'd30;
					7'd30: state_nxt = 7'd31;
					7'd31: state_nxt = 7'd32;
					7'd32: state_nxt = 7'd33;
					7'd33: state_nxt = 7'd34;
					7'd34: state_nxt = 7'd35;
					7'd35: state_nxt = 7'd36;
					7'd36: state_nxt = 7'd37;
					7'd37: state_nxt = 7'd38;
					7'd38: state_nxt = 7'd39;
					7'd39: state_nxt = 7'd40;
					7'd40: state_nxt = 7'd41;
					7'd41: state_nxt = 7'd42;
					7'd42: state_nxt = 7'd43;
					7'd43: state_nxt = 7'd44;
					7'd44: state_nxt = 7'd45;
					7'd45: state_nxt = 7'd46;
					7'd46: state_nxt = 7'd47;
					7'd47: state_nxt = 7'd48;
					7'd48: state_nxt = 7'd49;
					7'd49: state_nxt = 7'd50;
					7'd50: state_nxt = 7'd51;
					7'd51: state_nxt = 7'd52;
					7'd52: state_nxt = 7'd53;
					7'd53: state_nxt = 7'd54;
					7'd54: state_nxt = 7'd55;
					7'd55: state_nxt = 7'd56;
					7'd56: state_nxt = 7'd57;
					7'd57: state_nxt = 7'd58;
					7'd58: state_nxt = 7'd59;
					7'd59: state_nxt = 7'd60;
					7'd60: state_nxt = 7'd61;
					7'd61: state_nxt = 7'd62;
					7'd62: state_nxt = 7'd63;
					7'd63: state_nxt = 7'd64;
					7'd64: state_nxt = 7'd65;
					7'd65: state_nxt = 7'd66;
					7'd66: state_nxt = 7'd67;
					7'd67: state_nxt = 7'd68;
					default: state_nxt = 'bx;
				endcase
	end

	// export state to system to act on it
	always @ (posedge clock or posedge reset)
		if (reset) fsm_stateR <= 'bx; else fsm_stateR <= state_nxt;

endmodule

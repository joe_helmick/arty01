from AssemblerParser import *

from JMUAsmVisitorBase import JMUAsmVisitorBase
from JMUAsmVisitorBase import JMUEnvironment
from JMUInstruction import Instruction
from JMUArchitecture import Arch
from JMUCustomErrors import *

from JMUAsmProject import AsmProject


class JMUAsmVisitorPass2(JMUAsmVisitorBase):
    A = Arch()

    def __init__(self, ENV: JMUEnvironment, PRJ: AsmProject):
        super().__init__(ENV, PRJ)

    def visitAliasdef(self, ctx: AssemblerParser.AliasdefContext):
        regnumber = int(ctx.children[1].getText()[-2:])
        name = ctx.children[2].getText()
        self.ENV.aliases[regnumber] = name
        asm_line = self.get_asm_line(ctx)
        if self.ENV.dirPrintEachLine:
            print('.alias : line ' + str(asm_line.global_line_number) + ' using \'' + name + '\' as alias for r' + str(regnumber))
        return

    def visitBlockLabel(self, ctx: AssemblerParser.BlockLabelContext):
        name = ctx.children[0].getText()
        name = name.replace(':', '')
        pc = self.ENV.symbols.get(name).value
        self.PC = pc
        return

    def visitGpr(self, ctx: AssemblerParser.GprContext):

        if ctx.GPR():
            return int(ctx.children[0].getText()[-2:])

        if ctx.ALIASNAME():
            name = str(ctx.children[0])
            if name in self.ENV.aliases.values():
                regid = next(key for key, value in self.ENV.aliases.items() if value == name)
                return regid
            else:
                asm_line = self.get_asm_line(context=ctx)
                self.ENV.errors.append(SymbolNotDefinedError(asm_line.line_number, name))

    def visitGprptr(self, ctx: AssemblerParser.GprptrContext):
        return self.visit(ctx.children[1])

    def visitSymptr(self, ctx: AssemblerParser.SymptrContext):
        name = str(ctx.children[1])
        if name in self.ENV.symbols:
            v = self.ENV.symbols[name].value
            return v
        else:
            asm_line = self.get_asm_line(context=ctx)
            self.ENV.errors.append(SymbolNotDefinedError(asm_line.line_number, name))

    def visitIoconstant(self, ctx: AssemblerParser.IoconstantContext):
        if ctx.CHAR():
            t = ctx.children[0].getText()[1]
            t = ord(t)
            return t
        if ctx.SYMNAME():
            name = str(ctx.children[0])
            if name in self.ENV.symbols:
                v = self.ENV.symbols[name].value
                return v
            else:
                asm_line = self.get_asm_line(context=ctx)
                self.ENV.errors.append(SymbolNotDefinedError(asm_line.line_number, name))
        if ctx.number():
            return self.visit(ctx.children[0])

        # If we get here there's an unhandled type.
        asm_line = self.get_asm_line(context=ctx)
        self.ENV.errors.append(NotYetImpplementedError(asm_line.line_number))

    # ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
    # INSTRUCTION HELPER METHODS, COMMON CODE FOR DIFFERENT MODES AND OPCODES
    # ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■

    def finish_instruction(self, asm_line, instruction):
        asm_line.instruction = instruction
        asm_line.program_counter = instruction.pc
        asm_line.words = instruction.words
        self.PC += instruction.size

        if self.ENV.dirPrintEachLine:
            print(str(asm_line.global_line_number) + "  " + asm_line.source_code.rstrip())

    def do_ab_common(self, ctx, info: tuple):
        asm_line = self.get_asm_line(ctx)
        rega = self.visit(ctx.children[1])
        regb = self.visit(ctx.children[3])
        instruction = Instruction()
        instruction.make(lineno=asm_line.line_number,
                         source=asm_line.source_code,
                         pc=self.PC,
                         info=info,
                         a=rega,
                         b=regb)
        self.finish_instruction(asm_line, instruction)

    def do_at_common(self, ctx, info: tuple):
        asm_line = self.get_asm_line(ctx)
        instruction = Instruction()
        rega = self.visit(ctx.children[1])
        t = self.visit(ctx.children[3])
        if Arch().fits_in_t(t):
            instruction.make(lineno=asm_line.line_number,
                             source=asm_line.source_code,
                             pc=self.PC,
                             info=info,
                             a=rega,
                             t=t)
            self.finish_instruction(asm_line, instruction)
        else:
            self.ENV.errors.append(TinyValueOutOfRange(asm_line.line_number, t))

    # ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
    # INSTRUCTIONS
    # ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■

    def visitI_ld_gpr_expr(self, ctx: AssemblerParser.I_ld_gpr_exprContext):
        asm_line = self.get_asm_line(ctx)
        rega = self.visit(ctx.children[1])
        expr_value = self.visit(ctx.children[3])

        if not Arch.fits_in_word(expr_value):
            self.ENV.errors.append(IntegerOutOfRange(asm_line.line_number, expr_value))
            return

        instruction = Instruction()
        instruction.make(lineno=asm_line.line_number,
                         source=asm_line.source_code,
                         pc=self.PC,
                         info=Arch.d_op2['i_ld_gpr_expr'],
                         a=rega,
                         iw2=expr_value)
        self.finish_instruction(asm_line, instruction)

    def visitI_ld_gpr_t(self, ctx: AssemblerParser.I_ld_gpr_tContext):
        self.do_at_common(ctx, Arch.d_op2['i_ld_a_t'])

    def visitI_ld_gpr_gptr(self, ctx: AssemblerParser.I_ld_gpr_gptrContext):
        self.do_ab_common(ctx=ctx, info=Arch.d_op2['i_ld_gpr_gptr'])

    def visitI_ld_gpr_sptr(self, ctx: AssemblerParser.I_ld_gpr_sptrContext):
        asm_line = self.get_asm_line(ctx)
        rega = self.visit(ctx.children[1])
        expr_value = self.visit(ctx.children[3])

        if expr_value is None:
            txt = ctx.children[3].children[1].getText()
            self.ENV.errors.append(SymbolNotDefinedError(asm_line.line_number, txt))
            return

        if not Arch.fits_in_word(expr_value):
            self.ENV.errors.append(IntegerOutOfRange(asm_line.line_number, expr_value))
            return

        instruction = Instruction()
        instruction.make(lineno=asm_line.line_number,
                         source=asm_line.source_code,
                         pc=self.PC,
                         info=Arch.d_op2['i_ld_gpr_sptr'],
                         a=rega,
                         iw2=expr_value)
        self.finish_instruction(asm_line, instruction)

    def visitI_copy_gpr_gpr(self, ctx: AssemblerParser.I_copy_gpr_gprContext):
        self.do_ab_common(ctx, Arch.d_op2['i_copy_ab'])

    def visitI_ld_sp_expr(self, ctx: AssemblerParser.I_ld_sp_exprContext):
        asm_line = self.get_asm_line(ctx)
        instruction = Instruction()
        expr_value = self.visit(ctx.children[3])
        instruction.make(lineno=asm_line.line_number,
                         source=asm_line.source_code,
                         pc=self.PC,
                         info=Arch.d_op2['i_ld_sp'],
                         iw2=expr_value)
        self.finish_instruction(asm_line, instruction)

    def visitI_sv_expr_gpr(self, ctx: AssemblerParser.I_sv_expr_gprContext):
        asm_line = self.get_asm_line(ctx)
        instruction = Instruction()
        rega = self.visit(ctx.children[3])
        expr_value = self.visit(ctx.children[1])

        if not Arch.fits_in_word(expr_value):
            self.ENV.errors.append(IntegerOutOfRange(asm_line.line_number, expr_value))
            return

        instruction.make(lineno=asm_line.line_number,
                         source=asm_line.source_code,
                         pc=self.PC,
                         info=Arch.d_op2['i_sv_expr_gpr'],
                         a=rega,
                         iw2=expr_value)
        self.finish_instruction(asm_line, instruction)

    def visitI_sv_gptr_gpr(self, ctx: AssemblerParser.I_sv_gptr_gprContext):
        asm_line = self.get_asm_line(ctx)
        instruction = Instruction()
        rega = self.visit(ctx.children[1])
        regb = self.visit(ctx.children[3])
        instruction.make(lineno=asm_line.line_number,
                         source=asm_line.source_code,
                         pc=self.PC,
                         info=Arch.d_op2['i_sv_gptr_gpr'],
                         a=rega,
                         b=regb)
        self.finish_instruction(asm_line, instruction)

    def visitI_sv_sptr_gpr(self, ctx: AssemblerParser.I_sv_sptr_gprContext):
        asm_line = self.get_asm_line(ctx)
        instruction = Instruction()
        sptrvalue = self.visit(ctx.children[1])
        if sptrvalue is None:
            txt = ctx.children[1].children[1].getText()
            self.ENV.errors.append(SymbolNotDefinedError(asm_line.line_number, txt))
            return
        rega = self.visit(ctx.children[3])
        instruction.make(lineno=asm_line.line_number,
                         source=asm_line.source_code,
                         pc=self.PC,
                         info=Arch.d_op2['i_sv_sptr_gpr'],
                         a=rega,
                         iw2=sptrvalue)
        self.finish_instruction(asm_line, instruction)

    def visitI_sv_gptr_expr(self, ctx: AssemblerParser.I_sv_gptr_exprContext):
        asm_line = self.get_asm_line(ctx)
        instruction = Instruction()
        rega = self.visit(ctx.children[1])
        expr_value = self.visit(ctx.children[3])
        if not Arch.fits_in_word(expr_value):
            self.ENV.errors.append(IntegerOutOfRange(asm_line.line_number, expr_value))
            return

        instruction.make(lineno=asm_line.line_number,
                         source=asm_line.source_code,
                         pc=self.PC,
                         info=Arch.d_op2['i_sv_gptr_expr'],
                         a=rega,
                         iw2=expr_value)
        self.finish_instruction(asm_line, instruction)

    def visitI_call(self, ctx: AssemblerParser.I_callContext):
        asm_line = self.get_asm_line(ctx)
        symbol_name = ctx.children[1].getText()
        instruction = Instruction()

        if symbol_name not in self.ENV.symbols:
            self.ENV.errors.append(SymbolNotDefinedError(asm_line.line_number, symbol_name))
            return

        address = self.ENV.symbols[symbol_name].value
        if len(asm_line.words) == 1:
            s = address - (self.PC + 1)
            instruction.make(lineno=asm_line.line_number,
                             source=asm_line.source_code,
                             pc=self.PC,
                             info=Arch.d_op2['i_call_near'],
                             s=s)
        else:
            instruction.make(lineno=asm_line.line_number,
                             source=asm_line.source_code,
                             pc=self.PC,
                             info=Arch.d_op2['i_call_far'],
                             iw2=address)
        self.finish_instruction(asm_line, instruction)

    def visitI_add_gpr_t(self, ctx: AssemblerParser.I_add_gpr_tContext):
        self.do_at_common(ctx, Arch.d_op2['i_add_a_t'])

    def visitI_add_gpr_gpr(self, ctx: AssemblerParser.I_add_gpr_gprContext):
        self.do_ab_common(ctx, Arch.d_op2['i_add_ab'])

    def visitI_sub_gpr_t(self, ctx: AssemblerParser.I_sub_gpr_tContext):
        self.do_at_common(ctx, Arch.d_op2['i_sub_a_t'])

    def visitI_sub_gpr_gpr(self, ctx: AssemblerParser.I_sub_gpr_gprContext):
        self.do_ab_common(ctx, Arch.d_op2['i_sub_ab'])

    def visitI_cmp_gpr_t(self, ctx: AssemblerParser.I_cmp_gpr_tContext):
        self.do_at_common(ctx, Arch.d_op2['i_cmp_a_t'])

    def visitI_cmp_gpr_gpr(self, ctx: AssemblerParser.I_cmp_gpr_gprContext):
        self.do_ab_common(ctx, Arch.d_op2['i_cmp_ab'])

    def visitI_mul_gpr_t(self, ctx: AssemblerParser.I_mul_gpr_tContext):
        self.do_at_common(ctx, Arch.d_op2['i_mul_a_t'])

    def visitI_mul_gpr_gpr(self, ctx: AssemblerParser.I_mul_gpr_gprContext):
        self.do_ab_common(ctx, Arch.d_op2['i_mul_ab'])

    # def visitI_and_gpr_t(self, ctx: AssemblerParser.I_and_gpr_tContext):
    #     self.do_at_common(ctx, Arch.d_op2['i_and_a_t'])
    #
    # def visitI_and_gpr_gpr(self, ctx: AssemblerParser.I_and_gpr_gprContext):
    #     self.do_ab_common(ctx, Arch.d_op2['i_and_ab'])
    #
    # def visitI_or_gpr_t(self, ctx: AssemblerParser.I_or_gpr_tContext):
    #     self.do_at_common(ctx, Arch.d_op2['i_or_a_t'])
    #
    # def visitI_or_gpr_gpr(self, ctx: AssemblerParser.I_or_gpr_gprContext):
    #     self.do_ab_common(ctx, Arch.d_op2['i_or_ab'])
    #
    # def visitI_xor_gpr_t(self, ctx: AssemblerParser.I_xor_gpr_tContext):
    #     self.do_at_common(ctx, Arch.d_op2['i_xor_a_t'])
    #
    # def visitI_xor_gpr_gpr(self, ctx: AssemblerParser.I_xor_gpr_gprContext):
    #     self.do_ab_common(ctx, Arch.d_op2['i_xor_ab'])

    def visitI_div_gpr_gpr_gpr(self, ctx: AssemblerParser.I_div_gpr_gpr_gprContext):
        asm_line = self.get_asm_line(ctx)
        rega = self.visit(ctx.children[1])
        regb = self.visit(ctx.children[3])
        regc = self.visit(ctx.children[5])
        instruction = Instruction()
        instruction.make(lineno=asm_line.line_number,
                         source=asm_line.source_code,
                         pc=self.PC,
                         info=Arch.d_op2['i_div_abc'],
                         a=rega,
                         b=regb,
                         c=regc)
        self.finish_instruction(asm_line, instruction)

    def visitI_jr(self, ctx: AssemblerParser.I_jrContext):
        asm_line = self.get_asm_line(ctx)
        instruction = Instruction()
        dictentry = None
        if len(ctx.children) == 3:
            fc = ctx.children[1]
            label_text = ctx.children[2].getText()
            if isinstance(fc, AssemblerParser.Flagcond_zContext):
                dictentry = Arch.d_op2['i_jrz']
            if isinstance(fc, AssemblerParser.Flagcond_nzContext):
                dictentry = Arch.d_op2['i_jrnz']
            if isinstance(fc, AssemblerParser.Flagcond_sContext):
                dictentry = Arch.d_op2['i_jrs']
            if isinstance(fc, AssemblerParser.Flagcond_nsContext):
                dictentry = Arch.d_op2['i_jrns']
        else:
            label_text = ctx.children[1].getText()
            dictentry = Arch.d_op2['i_jr']
        sym = self.ENV.symbols.get(label_text)
        if sym is None:
            self.ENV.errors.append(SymbolNotDefinedError(asm_line.line_number, label_text))
            return
        target_address = sym.value
        delta = target_address - self.PC
        delta = delta - 1  # adjust for the PC movement during this instruction
        if not Arch().fits_in_s(delta):
            self.ENV.errors.append(IntegerOutOfRange(asm_line.line_number, delta))
            return
        instruction.make(lineno=asm_line.line_number,
                         source=asm_line.source_code,
                         pc=self.PC,
                         info=dictentry,
                         s=delta)
        self.finish_instruction(asm_line, instruction)

    def visitI_ja(self, ctx: AssemblerParser.I_jaContext):
        asm_line = self.get_asm_line(ctx)
        instruction = Instruction()
        dictentry = None
        if len(ctx.children) == 3:
            fc = ctx.children[1]
            label_text = ctx.children[2].getText()
            if isinstance(fc, AssemblerParser.Flagcond_zContext):
                dictentry = Arch.d_op2['i_jaz']
            if isinstance(fc, AssemblerParser.Flagcond_nzContext):
                dictentry = Arch.d_op2['i_janz']
            if isinstance(fc, AssemblerParser.Flagcond_sContext):
                dictentry = Arch.d_op2['i_jas']
            if isinstance(fc, AssemblerParser.Flagcond_nsContext):
                dictentry = Arch.d_op2['i_jans']
        else:
            label_text = ctx.children[1].getText()
            dictentry = Arch.d_op2['i_ja']

        sym = self.ENV.symbols.get(label_text)
        if sym is None:
            self.ENV.errors.append(SymbolNotDefinedError(asm_line.line_number, label_text))
            return
        address = sym.value
        instruction.make(lineno=asm_line.line_number,
                         source=asm_line.source_code,
                         pc=self.PC,
                         info=dictentry,
                         iw2=address)
        self.finish_instruction(asm_line, instruction)

    def visitI_ret(self, ctx: AssemblerParser.I_retContext):
        asm_line = self.get_asm_line(ctx)
        instruction = Instruction()
        dictentry = None
        if len(ctx.children) == 2:
            t1 = ctx.children[1]
            if isinstance(t1, AssemblerParser.Flagcond_zContext):
                dictentry = Arch.d_op2['i_retz']
            if isinstance(t1, AssemblerParser.Flagcond_nzContext):
                dictentry = Arch.d_op2['i_retnz']
            if isinstance(t1, AssemblerParser.Flagcond_sContext):
                dictentry = Arch.d_op2['i_rets']
            if isinstance(t1, AssemblerParser.Flagcond_nsContext):
                dictentry = Arch.d_op2['i_retns']
        else:
            dictentry = Arch.d_op2['i_ret']

        instruction.make(lineno=asm_line.line_number,
                         source=asm_line.source_code,
                         pc=self.PC,
                         info=dictentry)
        self.finish_instruction(asm_line, instruction)

    def visitI_push_flags(self, ctx: AssemblerParser.I_push_flagsContext):
        asm_line = self.get_asm_line(ctx)
        instruction = Instruction()
        instruction.make(lineno=asm_line.line_number,
                         source=asm_line.source_code,
                         pc=self.PC,
                         info=Arch.d_op2['i_push_flags'])
        self.finish_instruction(asm_line, instruction)

    def visitI_push_gpr(self, ctx: AssemblerParser.I_push_gprContext):
        asm_line = self.get_asm_line(ctx)
        instruction = Instruction()
        rega = int(ctx.children[1].getText()[-2:])
        instruction.make(lineno=asm_line.line_number,
                         source=asm_line.source_code,
                         pc=self.PC,
                         info=Arch.d_op2['i_push_reg'],
                         a=rega)
        self.finish_instruction(asm_line, instruction)

    def visitI_pop_flags(self, ctx: AssemblerParser.I_pop_flagsContext):
        asm_line = self.get_asm_line(ctx)
        instruction = Instruction()
        instruction.make(lineno=asm_line.line_number,
                         source=asm_line.source_code,
                         pc=self.PC,
                         info=Arch.d_op2['i_pop_flags'])
        self.finish_instruction(asm_line, instruction)

    def visitI_pop_gpr(self, ctx: AssemblerParser.I_pop_gprContext):
        asm_line = self.get_asm_line(ctx)
        instruction = Instruction()
        rega = int(ctx.children[1].getText()[-2:])
        instruction.make(lineno=asm_line.line_number,
                         source=asm_line.source_code,
                         pc=self.PC,
                         info=Arch.d_op2['i_pop_reg'],
                         a=rega)
        self.finish_instruction(asm_line, instruction)

    def visitI_tx_gpr(self, ctx: AssemblerParser.I_tx_gprContext):
        asm_line = self.get_asm_line(ctx)
        instruction = Instruction()
        rega = self.visit(ctx.children[1])
        instruction.make(lineno=asm_line.line_number,
                         source=asm_line.source_code,
                         pc=self.PC,
                         info=Arch.d_op2['i_tx_gpr'],
                         a=rega)
        self.finish_instruction(asm_line, instruction)

    def visitI_tx_t(self, ctx: AssemblerParser.I_tx_tContext):
        asm_line = self.get_asm_line(ctx)
        instruction = Instruction()
        t = self.visit(ctx.children[1])
        if Arch().fits_in_t(t):
            instruction.make(lineno=asm_line.line_number,
                             source=asm_line.source_code,
                             pc=self.PC,
                             info=Arch.d_op2['i_tx_t'],
                             t=t)
            self.finish_instruction(asm_line, instruction)
        else:
            self.ENV.errors.append(TinyValueOutOfRange(asm_line.line_number, t))

    def visitI_io_t(self, ctx: AssemblerParser.I_io_tContext):
        asm_line = self.get_asm_line(ctx)
        instruction = Instruction()
        t = self.visit(ctx.children[1])
        if Arch().fits_in_t(t):
            instruction.make(lineno=asm_line.line_number,
                             source=asm_line.source_code,
                             pc=self.PC,
                             info=Arch.d_op2['i_io_t'],
                             t=t)
            self.finish_instruction(asm_line, instruction)
        else:
            self.ENV.errors.append(TinyValueOutOfRange(asm_line.line_number, t))

    def visitI_io_gpr(self, ctx: AssemblerParser.I_io_gprContext):
        asm_line = self.get_asm_line(ctx)
        instruction = Instruction()
        rega = self.visit(ctx.children[1])
        instruction.make(lineno=asm_line.line_number,
                         source=asm_line.source_code,
                         pc=self.PC,
                         info=Arch.d_op2['i_io_gpr'],
                         a=rega)
        self.finish_instruction(asm_line, instruction)

    def visitI_io_off(self, ctx: AssemblerParser.I_io_offContext):
        asm_line = self.get_asm_line(ctx)
        instruction = Instruction()
        instruction.make(lineno=asm_line.line_number,
                         source=asm_line.source_code,
                         pc=self.PC,
                         info=Arch.d_op2['i_io_off'])
        self.finish_instruction(asm_line, instruction)

    def do_shift_common(self, ctx, info: tuple):
        asm_line = self.get_asm_line(ctx)
        instruction = Instruction()
        rega = self.visit(ctx.children[1])
        cnt = self.visit(ctx.children[3])
        # Make sure shift amount is a reasonable value.
        if not 1 <= cnt <= 18:
            self.ENV.errors.append(ImpossibleShift(asm_line.global_line_number, cnt))
            #  ^^^^^^^ TODO MAKE ALL LINE NUMBERS GLOBAL TO ACCOUNT FOR INCLUDE FILES!???
        instruction.make(lineno=asm_line.global_line_number,
                         source=asm_line.source_code,
                         pc=self.PC,
                         info=info,
                         a=rega,
                         b=cnt)
        self.finish_instruction(asm_line, instruction)

    def visitI_rx_gpr(self, ctx: AssemblerParser.I_rx_gprContext):
        asm_line = self.get_asm_line(ctx)
        instruction = Instruction()
        rega = self.visit(ctx.children[1])
        instruction.make(lineno=asm_line.line_number,
                         source=asm_line.source_code,
                         pc=self.PC,
                         info=Arch.d_op2['i_rx_gpr'],
                         a=rega)
        self.finish_instruction(asm_line, instruction)

    # def visitI_shl(self, ctx: AssemblerParser.I_shlContext):
    #     self.do_shift_common(ctx, Arch.d_op2['i_shl'])
    #
    # def visitI_shr(self, ctx: AssemblerParser.I_shrContext):
    #     self.do_shift_common(ctx, Arch.d_op2['i_shr'])



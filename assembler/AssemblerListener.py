# Generated from ..\Assembler.g4 by ANTLR 4.8
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .AssemblerParser import AssemblerParser
else:
    from AssemblerParser import AssemblerParser

# This class defines a complete listener for a parse tree produced by AssemblerParser.
class AssemblerListener(ParseTreeListener):

    # Enter a parse tree produced by AssemblerParser#program.
    def enterProgram(self, ctx:AssemblerParser.ProgramContext):
        pass

    # Exit a parse tree produced by AssemblerParser#program.
    def exitProgram(self, ctx:AssemblerParser.ProgramContext):
        pass


    # Enter a parse tree produced by AssemblerParser#blockDirective.
    def enterBlockDirective(self, ctx:AssemblerParser.BlockDirectiveContext):
        pass

    # Exit a parse tree produced by AssemblerParser#blockDirective.
    def exitBlockDirective(self, ctx:AssemblerParser.BlockDirectiveContext):
        pass


    # Enter a parse tree produced by AssemblerParser#blockLabel.
    def enterBlockLabel(self, ctx:AssemblerParser.BlockLabelContext):
        pass

    # Exit a parse tree produced by AssemblerParser#blockLabel.
    def exitBlockLabel(self, ctx:AssemblerParser.BlockLabelContext):
        pass


    # Enter a parse tree produced by AssemblerParser#blockInstruction.
    def enterBlockInstruction(self, ctx:AssemblerParser.BlockInstructionContext):
        pass

    # Exit a parse tree produced by AssemblerParser#blockInstruction.
    def exitBlockInstruction(self, ctx:AssemblerParser.BlockInstructionContext):
        pass


    # Enter a parse tree produced by AssemblerParser#directive.
    def enterDirective(self, ctx:AssemblerParser.DirectiveContext):
        pass

    # Exit a parse tree produced by AssemblerParser#directive.
    def exitDirective(self, ctx:AssemblerParser.DirectiveContext):
        pass


    # Enter a parse tree produced by AssemblerParser#equate.
    def enterEquate(self, ctx:AssemblerParser.EquateContext):
        pass

    # Exit a parse tree produced by AssemblerParser#equate.
    def exitEquate(self, ctx:AssemblerParser.EquateContext):
        pass


    # Enter a parse tree produced by AssemblerParser#org.
    def enterOrg(self, ctx:AssemblerParser.OrgContext):
        pass

    # Exit a parse tree produced by AssemblerParser#org.
    def exitOrg(self, ctx:AssemblerParser.OrgContext):
        pass


    # Enter a parse tree produced by AssemblerParser#struct.
    def enterStruct(self, ctx:AssemblerParser.StructContext):
        pass

    # Exit a parse tree produced by AssemblerParser#struct.
    def exitStruct(self, ctx:AssemblerParser.StructContext):
        pass


    # Enter a parse tree produced by AssemblerParser#structdecl.
    def enterStructdecl(self, ctx:AssemblerParser.StructdeclContext):
        pass

    # Exit a parse tree produced by AssemblerParser#structdecl.
    def exitStructdecl(self, ctx:AssemblerParser.StructdeclContext):
        pass


    # Enter a parse tree produced by AssemblerParser#defvarOriginNumber.
    def enterDefvarOriginNumber(self, ctx:AssemblerParser.DefvarOriginNumberContext):
        pass

    # Exit a parse tree produced by AssemblerParser#defvarOriginNumber.
    def exitDefvarOriginNumber(self, ctx:AssemblerParser.DefvarOriginNumberContext):
        pass


    # Enter a parse tree produced by AssemblerParser#defvarOriginSymbol.
    def enterDefvarOriginSymbol(self, ctx:AssemblerParser.DefvarOriginSymbolContext):
        pass

    # Exit a parse tree produced by AssemblerParser#defvarOriginSymbol.
    def exitDefvarOriginSymbol(self, ctx:AssemblerParser.DefvarOriginSymbolContext):
        pass


    # Enter a parse tree produced by AssemblerParser#defvarOriginNext.
    def enterDefvarOriginNext(self, ctx:AssemblerParser.DefvarOriginNextContext):
        pass

    # Exit a parse tree produced by AssemblerParser#defvarOriginNext.
    def exitDefvarOriginNext(self, ctx:AssemblerParser.DefvarOriginNextContext):
        pass


    # Enter a parse tree produced by AssemblerParser#structname.
    def enterStructname(self, ctx:AssemblerParser.StructnameContext):
        pass

    # Exit a parse tree produced by AssemblerParser#structname.
    def exitStructname(self, ctx:AssemblerParser.StructnameContext):
        pass


    # Enter a parse tree produced by AssemblerParser#vardecl.
    def enterVardecl(self, ctx:AssemblerParser.VardeclContext):
        pass

    # Exit a parse tree produced by AssemblerParser#vardecl.
    def exitVardecl(self, ctx:AssemblerParser.VardeclContext):
        pass


    # Enter a parse tree produced by AssemblerParser#include.
    def enterInclude(self, ctx:AssemblerParser.IncludeContext):
        pass

    # Exit a parse tree produced by AssemblerParser#include.
    def exitInclude(self, ctx:AssemblerParser.IncludeContext):
        pass


    # Enter a parse tree produced by AssemblerParser#defstring.
    def enterDefstring(self, ctx:AssemblerParser.DefstringContext):
        pass

    # Exit a parse tree produced by AssemblerParser#defstring.
    def exitDefstring(self, ctx:AssemblerParser.DefstringContext):
        pass


    # Enter a parse tree produced by AssemblerParser#stringpartcollection.
    def enterStringpartcollection(self, ctx:AssemblerParser.StringpartcollectionContext):
        pass

    # Exit a parse tree produced by AssemblerParser#stringpartcollection.
    def exitStringpartcollection(self, ctx:AssemblerParser.StringpartcollectionContext):
        pass


    # Enter a parse tree produced by AssemblerParser#stringPartExpr.
    def enterStringPartExpr(self, ctx:AssemblerParser.StringPartExprContext):
        pass

    # Exit a parse tree produced by AssemblerParser#stringPartExpr.
    def exitStringPartExpr(self, ctx:AssemblerParser.StringPartExprContext):
        pass


    # Enter a parse tree produced by AssemblerParser#stringPartString.
    def enterStringPartString(self, ctx:AssemblerParser.StringPartStringContext):
        pass

    # Exit a parse tree produced by AssemblerParser#stringPartString.
    def exitStringPartString(self, ctx:AssemblerParser.StringPartStringContext):
        pass


    # Enter a parse tree produced by AssemblerParser#stringPartChar.
    def enterStringPartChar(self, ctx:AssemblerParser.StringPartCharContext):
        pass

    # Exit a parse tree produced by AssemblerParser#stringPartChar.
    def exitStringPartChar(self, ctx:AssemblerParser.StringPartCharContext):
        pass


    # Enter a parse tree produced by AssemblerParser#i_halt.
    def enterI_halt(self, ctx:AssemblerParser.I_haltContext):
        pass

    # Exit a parse tree produced by AssemblerParser#i_halt.
    def exitI_halt(self, ctx:AssemblerParser.I_haltContext):
        pass


    # Enter a parse tree produced by AssemblerParser#i_out.
    def enterI_out(self, ctx:AssemblerParser.I_outContext):
        pass

    # Exit a parse tree produced by AssemblerParser#i_out.
    def exitI_out(self, ctx:AssemblerParser.I_outContext):
        pass


    # Enter a parse tree produced by AssemblerParser#i_add_gpr_gpr.
    def enterI_add_gpr_gpr(self, ctx:AssemblerParser.I_add_gpr_gprContext):
        pass

    # Exit a parse tree produced by AssemblerParser#i_add_gpr_gpr.
    def exitI_add_gpr_gpr(self, ctx:AssemblerParser.I_add_gpr_gprContext):
        pass


    # Enter a parse tree produced by AssemblerParser#i_sub_gpr_gpr.
    def enterI_sub_gpr_gpr(self, ctx:AssemblerParser.I_sub_gpr_gprContext):
        pass

    # Exit a parse tree produced by AssemblerParser#i_sub_gpr_gpr.
    def exitI_sub_gpr_gpr(self, ctx:AssemblerParser.I_sub_gpr_gprContext):
        pass


    # Enter a parse tree produced by AssemblerParser#i_cmp_gpr_gpr.
    def enterI_cmp_gpr_gpr(self, ctx:AssemblerParser.I_cmp_gpr_gprContext):
        pass

    # Exit a parse tree produced by AssemblerParser#i_cmp_gpr_gpr.
    def exitI_cmp_gpr_gpr(self, ctx:AssemblerParser.I_cmp_gpr_gprContext):
        pass


    # Enter a parse tree produced by AssemblerParser#i_jr_expr.
    def enterI_jr_expr(self, ctx:AssemblerParser.I_jr_exprContext):
        pass

    # Exit a parse tree produced by AssemblerParser#i_jr_expr.
    def exitI_jr_expr(self, ctx:AssemblerParser.I_jr_exprContext):
        pass


    # Enter a parse tree produced by AssemblerParser#i_jrz_expr.
    def enterI_jrz_expr(self, ctx:AssemblerParser.I_jrz_exprContext):
        pass

    # Exit a parse tree produced by AssemblerParser#i_jrz_expr.
    def exitI_jrz_expr(self, ctx:AssemblerParser.I_jrz_exprContext):
        pass


    # Enter a parse tree produced by AssemblerParser#i_jrnz_expr.
    def enterI_jrnz_expr(self, ctx:AssemblerParser.I_jrnz_exprContext):
        pass

    # Exit a parse tree produced by AssemblerParser#i_jrnz_expr.
    def exitI_jrnz_expr(self, ctx:AssemblerParser.I_jrnz_exprContext):
        pass


    # Enter a parse tree produced by AssemblerParser#i_ld_gpr_expr.
    def enterI_ld_gpr_expr(self, ctx:AssemblerParser.I_ld_gpr_exprContext):
        pass

    # Exit a parse tree produced by AssemblerParser#i_ld_gpr_expr.
    def exitI_ld_gpr_expr(self, ctx:AssemblerParser.I_ld_gpr_exprContext):
        pass


    # Enter a parse tree produced by AssemblerParser#i_add_gpr_expr.
    def enterI_add_gpr_expr(self, ctx:AssemblerParser.I_add_gpr_exprContext):
        pass

    # Exit a parse tree produced by AssemblerParser#i_add_gpr_expr.
    def exitI_add_gpr_expr(self, ctx:AssemblerParser.I_add_gpr_exprContext):
        pass


    # Enter a parse tree produced by AssemblerParser#i_sub_gpr_expr.
    def enterI_sub_gpr_expr(self, ctx:AssemblerParser.I_sub_gpr_exprContext):
        pass

    # Exit a parse tree produced by AssemblerParser#i_sub_gpr_expr.
    def exitI_sub_gpr_expr(self, ctx:AssemblerParser.I_sub_gpr_exprContext):
        pass


    # Enter a parse tree produced by AssemblerParser#i_cmp_gpr_expr.
    def enterI_cmp_gpr_expr(self, ctx:AssemblerParser.I_cmp_gpr_exprContext):
        pass

    # Exit a parse tree produced by AssemblerParser#i_cmp_gpr_expr.
    def exitI_cmp_gpr_expr(self, ctx:AssemblerParser.I_cmp_gpr_exprContext):
        pass


    # Enter a parse tree produced by AssemblerParser#gpio_state.
    def enterGpio_state(self, ctx:AssemblerParser.Gpio_stateContext):
        pass

    # Exit a parse tree produced by AssemblerParser#gpio_state.
    def exitGpio_state(self, ctx:AssemblerParser.Gpio_stateContext):
        pass


    # Enter a parse tree produced by AssemblerParser#bitnumber.
    def enterBitnumber(self, ctx:AssemblerParser.BitnumberContext):
        pass

    # Exit a parse tree produced by AssemblerParser#bitnumber.
    def exitBitnumber(self, ctx:AssemblerParser.BitnumberContext):
        pass


    # Enter a parse tree produced by AssemblerParser#bitop.
    def enterBitop(self, ctx:AssemblerParser.BitopContext):
        pass

    # Exit a parse tree produced by AssemblerParser#bitop.
    def exitBitop(self, ctx:AssemblerParser.BitopContext):
        pass


    # Enter a parse tree produced by AssemblerParser#powerExpr.
    def enterPowerExpr(self, ctx:AssemblerParser.PowerExprContext):
        pass

    # Exit a parse tree produced by AssemblerParser#powerExpr.
    def exitPowerExpr(self, ctx:AssemblerParser.PowerExprContext):
        pass


    # Enter a parse tree produced by AssemblerParser#numberExpr.
    def enterNumberExpr(self, ctx:AssemblerParser.NumberExprContext):
        pass

    # Exit a parse tree produced by AssemblerParser#numberExpr.
    def exitNumberExpr(self, ctx:AssemblerParser.NumberExprContext):
        pass


    # Enter a parse tree produced by AssemblerParser#plusMinusExpr.
    def enterPlusMinusExpr(self, ctx:AssemblerParser.PlusMinusExprContext):
        pass

    # Exit a parse tree produced by AssemblerParser#plusMinusExpr.
    def exitPlusMinusExpr(self, ctx:AssemblerParser.PlusMinusExprContext):
        pass


    # Enter a parse tree produced by AssemblerParser#multDivExpr.
    def enterMultDivExpr(self, ctx:AssemblerParser.MultDivExprContext):
        pass

    # Exit a parse tree produced by AssemblerParser#multDivExpr.
    def exitMultDivExpr(self, ctx:AssemblerParser.MultDivExprContext):
        pass


    # Enter a parse tree produced by AssemblerParser#symbolExpr.
    def enterSymbolExpr(self, ctx:AssemblerParser.SymbolExprContext):
        pass

    # Exit a parse tree produced by AssemblerParser#symbolExpr.
    def exitSymbolExpr(self, ctx:AssemblerParser.SymbolExprContext):
        pass


    # Enter a parse tree produced by AssemblerParser#parenExpr.
    def enterParenExpr(self, ctx:AssemblerParser.ParenExprContext):
        pass

    # Exit a parse tree produced by AssemblerParser#parenExpr.
    def exitParenExpr(self, ctx:AssemblerParser.ParenExprContext):
        pass


    # Enter a parse tree produced by AssemblerParser#number.
    def enterNumber(self, ctx:AssemblerParser.NumberContext):
        pass

    # Exit a parse tree produced by AssemblerParser#number.
    def exitNumber(self, ctx:AssemblerParser.NumberContext):
        pass


    # Enter a parse tree produced by AssemblerParser#decnum.
    def enterDecnum(self, ctx:AssemblerParser.DecnumContext):
        pass

    # Exit a parse tree produced by AssemblerParser#decnum.
    def exitDecnum(self, ctx:AssemblerParser.DecnumContext):
        pass


    # Enter a parse tree produced by AssemblerParser#hexnum.
    def enterHexnum(self, ctx:AssemblerParser.HexnumContext):
        pass

    # Exit a parse tree produced by AssemblerParser#hexnum.
    def exitHexnum(self, ctx:AssemblerParser.HexnumContext):
        pass


    # Enter a parse tree produced by AssemblerParser#binnum.
    def enterBinnum(self, ctx:AssemblerParser.BinnumContext):
        pass

    # Exit a parse tree produced by AssemblerParser#binnum.
    def exitBinnum(self, ctx:AssemblerParser.BinnumContext):
        pass



del AssemblerParser
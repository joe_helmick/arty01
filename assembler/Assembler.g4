grammar Assembler;

import Directives;

program
    : block* end EOF
    ;

end
    : END
    ;

block
    : directive    # blockDirective
    | LABEL        # blockLabel
    | instruction  # blockInstruction
    ;

instruction
    : LDW gpr SEP expr                  # i_ld_gpr_expr                 // OPC_LD_GPR_EXPR
    | LD gpr SEP expr                   # i_ld_gpr_t                    // OPC_LD_A_T

    | LD gpr SEP gprptr                 # i_ld_gpr_gptr                 // OPC_LD_GPR_GPTR
    | LD gpr SEP symptr                 # i_ld_gpr_sptr                 // OPC_LD_GPR_SPTR

    | COPY gpr SEP gpr                  # i_copy_gpr_gpr                // OPC_COPY_AB
    | LD STACKPOINTER SEP expr          # i_ld_sp_expr                  // OPC_LD_SP

    | SV expr SEP gpr                   # i_sv_expr_gpr                 // OPC_SV_EXPR_GPR
    | SV gprptr SEP gpr                 # i_sv_gptr_gpr                 // OPC_SV_GPTR_GPR
    | SV symptr SEP gpr                 # i_sv_sptr_gpr                 // OPC_SV_SPTR_GPR
    | SV gprptr SEP expr                # i_sv_gptr_expr                // OPC_SV_GPTR_EXPR

    | CALL SYMNAME                      # i_call                        // OPC_CALL_NEAR or OPC_CALL_FAR determined during pass 1

    | ADD gpr SEP expr                  # i_add_gpr_t                   // OPC_ADD_A T
    | ADD gpr SEP gpr                   # i_add_gpr_gpr                 // OPC_ADD_AB
    | SUB gpr SEP expr                  # i_sub_gpr_t                   // OPC_SUB_A_T
    | SUB gpr SEP gpr                   # i_sub_gpr_gpr                 // OPC_SUB_AB
    | CMP gpr SEP expr                  # i_cmp_gpr_t                   // OPC_CMP_A_T
    | CMP gpr SEP gpr                   # i_cmp_gpr_gpr                 // OPC_CMP_AB
    | MUL gpr SEP expr                  # i_mul_gpr_t                   // OPC_MUL_A_T
    | MUL gpr SEP gpr                   # i_mul_gpr_gpr                 // OPC_MUL_AB
//    | AND gpr SEP expr                  # i_and_gpr_t                   // OPC_AND_A_T
//    | AND gpr SEP gpr                   # i_and_gpr_gpr                 // OPC_AND_AB
//    | OR  gpr SEP expr                  # i_or_gpr_t                    // OPC_OR_A_T
//    | OR  gpr SEP gpr                   # i_or_gpr_gpr                  // OPC_OR_AB
//    | XOR gpr SEP expr                  # i_xor_gpr_t                   // OPC_XOR_A_T
//    | XOR gpr SEP gpr                   # i_xor_gpr_gpr                 // OPC_XOR_AB

    | DIV gpr SEP gpr SEP gpr           # i_div_gpr_gpr_gpr             // OPC_DIV_ABC

    | JR (flagcond)? SYMNAME            # i_jr                          // OPC_JR*  numerous opcodes because the S value takes up all the other bits
    | JA (flagcond)? SYMNAME            # i_ja                          // OPC_JA
    | RET (flagcond)?                   # i_ret                         // OPC_RET

    | PUSH FLAGS                        # i_push_flags                  // OPC_PUSH
    | PUSH gpr                          # i_push_gpr                    // OPC_PUSH
    | POP FLAGS                         # i_pop_flags                   // OPC_POP
    | POP gpr                           # i_pop_gpr                     // OPC_POP

    | TX gpr                            # i_tx_gpr                       // OPC_TX_GPR
    | IO gpr                            # i_io_gpr                       // OPC_IO_GPR
    | TX ioconstant                     # i_tx_t                         // OPC_TX_T
    | IO ioconstant                     # i_io_t                         // OPC_IO_T
    | IO_OFF                            # i_io_off                       // OPC_IO_GPR turns off any IO from the port

    | RX gpr                            # i_rx_gpr                       // OPC_RX_GPR

//    | SHL gpr SEP expr                  # i_shl                         // OPC_SHIFT
//    | SHR gpr SEP expr                  # i_shr                         // OPC_SHIFT

    ;

//// Post-increment and post-decrement gpr indicators
//postop
//    : POSTDEC
//    | POSTINC
//    ;

//  Stuff that the TX and IO instructions can emit
ioconstant
    : CHAR
    | SYMNAME
    | number
    ;

// Flag conditions for jumps and returns.
flagcond
    : COND_Z        # flagcond_z
    | COND_NZ       # flagcond_nz
    | COND_S        # flagcond_s
    | COND_NS       # flagcond_ns
    ;

// A pointer that is a gpr holding an address.  The GPR can be encoded and
// so instructions using this just use the 5-bit gpr designator.
gprptr
    : LBRACK gpr RBRACK
    ;

// A pointer that is a symbol representing an address.  Because this address
// can be anywhere in memory space, it takes an extra word to encode this.
symptr
    : LBRACK SYMNAME RBRACK
    ;

// A general-purpose register can be either in native format
// e.g. r02 or be represented by an alias that is in effect e.g. @byte_count
// Aliases are basically lightweight variable names.
gpr
    : GPR
    | ALIASNAME
    ;

expr
    : <assoc=right> expr POW expr       # powerExpr
    | expr (SPLAT | DIVIDE) expr        # multDivExpr
    | expr (PLUS | MINUS) expr          # plusMinusExpr
    | LPAREN expr RPAREN                # parenExpr
    | SYMNAME                           # symbolExpr
    | CHAR                              # charExpr
    | number                            # numberExpr
    ;

// Types of numbers supported
number
    : DECNUM
    | HEXNUM
    | BINNUM
    ;

//////////////////////////////////////////////////////////////////////////
//    LEXER RULES
//////////////////////////////////////////////////////////////////////////

// Native names for general purpose registers.
GPR
    : R '00' | R '01' | R '02' | R '03' | R '04' | R '05' | R '06' | R '07'
    | R '08' | R '09' | R '10' | R '11' | R '12' | R '13' | R '14' | R '15'
    | R '16' | R '17' | R '18' | R '19' | R '20' | R '21' | R '22' | R '23'
    | R '24' | R '25' | R '26' | R '27' | R '28' | R '29' | R '30' | R '31'
    ;

// Instructions
JR                 : J R ;
JA                 : J A ;
RET                : R E T ;
COND_Z             : DOT Z ;
COND_NZ            : DOT N Z ;
COND_S             : DOT S ;
COND_NS            : DOT N S ;
LD                 : L D ;
LDW                : L D W ;
SV                 : S V ;
COPY               : C O P Y ;
ADD                : A D D ;
SUB                : S U B ;
CMP                : C M P ;
AND                : A N D ;
OR                 : O R ;
XOR                : X O R ;
MUL                : M U L ;
DIV                : D I V ;
PUSH               : P U S H ;
POP                : P O P ;
CALL               : C A L L ;
TX                 : T X ;
RX                 : R X ;
IO                 : I O ;
IO_OFF             : I O MINUS ;
FLAGS              : F L A G S ;
SHR                : S H R ;
SHL                : S H L ;

// Directives
ORG                : DOT O R G ;
STRUCT             : DOT S T R U C T ;
EQUATE             : DOT E Q U ;
INCLUDE            : DOT I N C L U D E ;
STRINGDECL         : DOT S T R I N G ;
ALIAS              : DOT A L I A S ;
FILL               : F I L L ;
ORIGIN_NEXT        : N E X T ;
STACKPOINTER       : S P ;
DECLWORD           : W ;
LSTPROGRESS        : DOT L S T P R O G R E S S ;
LSTSYMTABLE        : DOT L S T S Y M T A B L E ;
LSTDECODING        : DOT L S T D E C O D I N G ;
BINDATA            : DOT B I N ;
END                : DOT E N D ;

// Symbols
POW                : '**' ;
SPLAT              : '*' ;
DIVIDE             : '/' ;
PLUS               : '+' ;
MINUS              : '-' ;
EQUALS             : '=' ;
RBRACE             : '}' ;
LBRACE             : '{' ;
RBRACK             : ']' ;
LBRACK             : '[';
RPAREN             : ')' ;
LPAREN             : '(' ;
SEP                : ',' ;
//POSTINC            : PLUS PLUS ;
//POSTDEC            : MINUS MINUS ;

// Other lexemes
NL                 : '\r'? '\n' -> channel(HIDDEN) ;
LINECOMMENT        : ';' ~[\r\n]* -> channel(HIDDEN) ;
SYMNAME            : [a-zA-Z][a-zA-Z_0-9]* ;
LABEL              : SYMNAME ':' ;
ALIASNAME          : '@' SYMNAME ;
HEXNUM             : '$' HEXDIGIT+ ;
BINNUM             : '#' BINDIGIT+ ;
DECNUM             :     DECDIGIT+ ;
DECDIGIT           : ('0'..'9') ;
HEXDIGIT           : ('0'..'9'|'A'..'F'|'a'..'f') ;
BINDIGIT           : ('0'..'1') ;

// This could use some work, it's not super flexible.
FILENAME           : DQ ('..')?[a-zA-Z\\/][a-zA-Z_0-9\\/]* '.asm' DQ;

// Used by tx and io instructions to output a single character directly.
CHAR               : '\'' ~["\\\r\n] '\'' ;

// From a Sam Harwell suggestion... can't even remember
// why this is included.
StringLiteral
  : UnterminatedStringLiteral DQ
  ;

UnterminatedStringLiteral
  : DQ (~["\\\r\n] | '\\' (. | EOF))*
  ;
DQ                 : '"' ;

// Toss away all unnecessary whitespace, it's not important.
WS                 : [ \t\f]+ -> channel(HIDDEN) ;

// From a Sam Harwell suggestion... can't even remember
// why this is included.
ERRORCHARACTER : . ;

// Enable mixed case for keywords with these fragments.
fragment DOT : '.' ;
fragment A : 'A' | 'a' ;
fragment B : 'B' | 'b' ;
fragment C : 'C' | 'c' ;
fragment D : 'D' | 'd' ;
fragment E : 'E' | 'e' ;
fragment F : 'F' | 'f' ;
fragment G : 'G' | 'g' ;
fragment H : 'H' | 'h' ;
fragment I : 'I' | 'i' ;
fragment J : 'J' | 'j' ;
fragment K : 'K' | 'k' ;
fragment L : 'L' | 'l' ;
fragment M : 'M' | 'm' ;
fragment N : 'N' | 'n' ;
fragment O : 'O' | 'o' ;
fragment P : 'P' | 'p' ;
fragment Q : 'Q' | 'q' ;
fragment R : 'R' | 'r' ;
fragment S : 'S' | 's' ;
fragment T : 'T' | 't' ;
fragment U : 'U' | 'u' ;
fragment V : 'V' | 'v' ;
fragment W : 'W' | 'w' ;
fragment X : 'X' | 'x' ;
fragment Y : 'Y' | 'y' ;
fragment Z : 'Z' | 'z' ;


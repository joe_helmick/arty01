from JMUArchitecture import Arch
# from JMUSymbol import Symbol
from JMUCustomErrors import *
# from antlr4.TokenStreamRewriter import *
# from sys import stderr


class JMUEnvironment:
    LISTING_WIDTH = 100
    HBAR_CHAR = '\u2508'
    VBAR_CHAR = '\u250A'
    BLOCK_CHAR = '\u25A0'

    symbols = None
    ram = None
    advisories = None
    # warnings = None
    errors = None
    parse_tree = None
    rewriter = None
    token_stream = None
    vlog_tst_pgm = None

    dirPrintEachLine = False
    dirPrintSymbolTable = False
    dirPrintDecoding = False

    def __init__(self):
        self.symbols = {}
        self.aliases = {}
        self.ram = [None] * Arch.MEMORY_DEPTH
        # self.warnings = []
        self.errors = []
        self.advisories = []
        self.rewriter = None
        self.token_stream = None

    def block_divider(self):
        t = ''.ljust(self.LISTING_WIDTH, self.BLOCK_CHAR)
        return t

    def header(self, text=''):
        if len(text) == 0:
            t = self.HBAR_CHAR * self.LISTING_WIDTH
        else:
            t = self.HBAR_CHAR * 5 + ' ' + text + ' '
            t = t.ljust(self.LISTING_WIDTH, self.HBAR_CHAR)  # + '\n'
        return t

    def program_header(self, pass_no: str):
        h1 = self.header('PROGRAM LISTING ' + pass_no ) + '\n'
        h2 = ' line   PC   iw1  iw2   ' + self.VBAR_CHAR + ' source code line\n'
        h3 = self.header('')  # + '\n'
        return h1 + h2 + h3

    def program_footer(self, pass_no: str):
        h1 = self.header('END ' + pass_no)
        return h1

    def print_symbol_dictionary(self):
        # first find longest symbol name
        max_length = 0
        for sym in self.symbols.keys():
            length = len(sym)
            if length > max_length:
                max_length = length

        # Now print the sorted list of keys and their locations.
        print()
        print(self.header('SYMBOL DICTIONARY'))
        for sym in sorted(self.symbols.keys()):
            padded_key = str(sym + ' ').ljust(max_length + 5, '-')
            value = self.symbols[sym].value
            if value is None:
                output = '{0} {1}'.format(padded_key, 'symbol unknown, check errors')
            else:
                output = '{0} ${1:05x} {2:8d}'.format(padded_key, value, value)
            print(output)
        print(self.header(''))
        return

    def show_assembly_result(self):
        e_count = len(self.errors)
        a_count = len(self.advisories)

        print()
        print(self.block_divider())
        # counts = "Errors:      {0}\nAdvisories:  {1}".format(e_count, a_count)

        if e_count == 0 and a_count == 0:
            result = "RESULT: SUCCESS"
        elif e_count == 0 and a_count > 0:
            result = "RESULT: SUCCESS (with " + str(a_count) + " advisories)"
        else:
            result = "RESULT: FAILURE DUE TO ERRORS"

        print(result)
        print(self.block_divider())

    def print_errors_and_warnings(self):
        if len(self.errors) == 0 and len(self.advisories) == 0:
            return

        print()
        char = '\u25A0'  # black square
        print(char * self.LISTING_WIDTH)

        index = 0
        if len(self.errors) > 0:
            print("ERRORS")
            for item in self.errors:
                index += 1
                message = '{0:03} {1}'.format(index, str(item))
                print(message)

        index = 0
        if len(self.advisories) > 0:
            print("ADVISORIES")
            for item in self.advisories:
                index += 1
                message = '{0:03} {1}'.format(index, str(item))
                print(message)

        print(char * self.LISTING_WIDTH)
        return

    def add_symbol(self, symbol):
        if symbol.name in self.symbols.keys():
            raise SymbolAlreadyDefinedError(symbol.line_defined_on, symbol.name)
        else:
            self.symbols[symbol.name] = symbol

    def ram_dump(self):
        # Work backwards and find the last memory location
        # with any value in it.
        last_filled = None
        for loc in range(Arch.MEMORY_DEPTH - 1, 0, -1):
            if self.ram[loc] is None:
                continue
            else:
                last_filled = loc
                break

        if last_filled is None:
            return

        file = open('outputs/ram_dump.txt', 'w')
        file.write('----- RAM DUMP -----------------------------------\n')
        file.write('--------------------------------------------------\n')
        # Now use that last-filled value as the limit to check.
        for n in range(last_filled + 1):
            o = self.ram[n]
            s = '$' + '{:05x}'.format(n) + ': '
            file.write(s)

            if o is None:
                file.write('empty')
            else:
                if isinstance(o, str):
                    file.write(o)
                else:
                    s = '$' + '{:05x}'.format(o) + ' '
                    file.write(s)
                    s = '#' + '{:018b}'.format(o) + ' '
                    file.write(s)
                    s = str(o)
                    file.write(s)
            file.write('\n')
        file.close()
        return

    # def write_verilog_test_program(self, listing: str):
    #     # Work backwards and find the last memory location
    #     # with any value in it.
    #     last_filled = None
    #     for loc in range(Arch.memory_depth - 1, 0, -1):
    #         if self.ram[loc] is None:
    #             continue
    #         else:
    #             last_filled = loc
    #             break
    #
    #     file = open(self.vlog_tst_pgm, mode='w', encoding='utf-8')
    #     # Now use that last-filled value as the limit to check.
    #
    #     file.write('\ttask ' + self.vlog_tst_pgm.replace('.v', '') + '();\n')
    #     file.write('\t\t' + 'real round_up;\n')
    #     file.write('\tbegin\n')
    #
    #     file.write('\t\t/*\n')
    #
    #     lines = listing.splitlines(keepends=True)
    #     for line in lines:
    #         file.write('\t\t' + line.rstrip() + '\n')
    #
    #     file.write('\t\t*/\n')
    #
    #     for n in range(last_filled + 1):
    #         o = self.ram[n]
    #         if o is None:
    #             continue
    #
    #         file.write('\t\tram_code_addr = \'d' + str(n) + '; ')
    #         file.write('ram_code_din = 18\'h' + '{:05x}'.format(o) + '; ')
    #         file.write('tickramclock_load(); \n')
    #
    #     file.write('\t\tround_up = 3.0 - load_ns;\n')
    #     file.write('\t\t# round_up;\n')
    #     file.write('\tend\n')
    #     file.write('\tendtask\n')
    #     file.close()
    #     return

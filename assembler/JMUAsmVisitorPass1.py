from JMUAsmVisitorBase import JMUAsmVisitorBase
from JMUAsmVisitorBase import JMUEnvironment
from JMUAsmProject import AsmProject
from AssemblerParser import *
from VarDefVarDecl import VarDef, VarDecl
from JMUCustomErrors import *
from JMUArchitecture import Arch
from JMUSymbol import *


# noinspection PyPep8Naming,PyTypeChecker
class JMUAsmVisitorPass1(JMUAsmVisitorBase):

    def __init__(self, ENV: JMUEnvironment, PRJ: AsmProject):
        super().__init__(ENV, PRJ)

    # ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
    # ASSEMBLER DIRECTIVES
    # ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■

    def visitPrintEachLine(self, ctx: AssemblerParser.PrintEachLineContext):
        self.ENV.dirPrintEachLine = True

    def visitPrintSymbolTable(self, ctx: AssemblerParser.PrintSymbolTableContext):
        self.ENV.dirPrintSymbolTable = True

    def visitPrintDecoding(self, ctx: AssemblerParser.PrintDecodingContext):
        self.ENV.dirPrintDecoding = True

    def visitBlockLabel(self, ctx: AssemblerParser.BlockLabelContext):
        #  These allocate no space, but do have an address.
        self.asm_line_pc(ctx, self.PC)
        name = ctx.children[0].getText()
        name = name.replace(":", "")
        value = self.PC
        line_defined_on = ctx.start.line
        new = Symbol(name, value, line_defined_on, symtype=SymbolType.pclabel)
        self.ENV.add_symbol(new)

    def visitEquate(self, ctx: AssemblerParser.EquateContext):
        # These allocate no space, but do have a value.
        name = ctx.children[1].getText()
        value = self.visit(ctx.children[2])
        line_defined_on = ctx.start.line
        new = Symbol(name, value, line_defined_on, symtype=SymbolType.equate)
        self.ENV.add_symbol(new)

    def visitInclude(self, ctx: AssemblerParser.IncludeContext):
        return

    def visitOrg(self, ctx: AssemblerParser.OrgContext):
        # asm_line = self.get_asm_line(ctx, self.PC)
        self.asm_line_pc(ctx, self.PC)
        value = self.visit(ctx.children[1])
        self.PC = value

    def visitEnd(self, ctx: AssemblerParser.EndContext):
        self.asm_line_pc(ctx, self.PC - 1)

    # ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
    # VARIABLE DEFINITION (vardef) BLOCK METHODS
    # ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■

    def visitStruct(self, ctx: AssemblerParser.StructContext):
        #  Create a new block object
        self.current_var_def = VarDef()
        self.current_block_size = 0
        for c in ctx.children:
            self.visit(c)
        # Add it to the list
        asm_line = self.asm_line_pc(ctx, self.current_var_def.address)
        asm_line.program_counter = self.current_var_def.address
        self.var_defs_list.append(self.current_var_def)
        size_symbol = Symbol('sizeof_' + self.current_block_name,
                             self.current_block_size,
                             self.current_block_line_defined_on,
                             symtype=SymbolType.sizeof)
        self.ENV.add_symbol(size_symbol)

    def visitStructname(self, ctx: AssemblerParser.StructnameContext):
        name = ctx.children[0].getText()
        value = self.PC
        line_defined_on = ctx.start.line
        new = Symbol(name, value, line_defined_on, symtype=SymbolType.datastruct)
        self.ENV.add_symbol(new)
        self.current_block_name = name
        return

    def visitDefvarOriginNumber(self, ctx: AssemblerParser.DefvarOriginNumberContext):
        address = self.visit(ctx.children[0])
        if address > Arch.MAX_UVALUE_REG:
            line = ctx.children[0].start.line
            self.ENV.errors.append(str(IntegerOutOfRange(line, address)))
        else:
            self.current_var_def.address = address
            self.PC = address

    def visitDefvarOriginNext(self, ctx: AssemblerParser.DefvarOriginNextContext):
        address = self.PC
        self.current_var_def.address = address

    def visitDefvarOriginSymbol(self, ctx: AssemblerParser.DefvarOriginSymbolContext):
        label_text = ctx.children[0].getText()
        sym = self.ENV.symbols.symbols.get(label_text)
        address = sym.value
        self.current_var_def.address = address
        self.PC = address

    def visitVardecl(self, ctx: AssemblerParser.VardeclContext):
        self.asm_line_pc(ctx, self.PC)
        name = ctx.children[0].getText()
        length = self.visit(ctx.children[2])
        decl = VarDecl(name, 'word', self.PC, length)
        self.current_var_def.decls.append(decl)
        line_defined_on = ctx.start.line
        new = Symbol(name, self.PC, line_defined_on, symtype=SymbolType.variable)
        self.ENV.add_symbol(new)
        self.current_block_size += length

        if len(ctx.children) < 5:
            # No fill value defined, so just move the program counter.
            self.PC += length
            return

        fill_value = self.visit(ctx.children[4])

        if isinstance(fill_value, int):
            for index in range(length):
                self.ENV.ram[self.PC] = fill_value
                self.PC += 1

        return

    # ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
    # BINARY DATA DECLARATION METHODS
    # ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■

    def visitBindata(self, ctx: AssemblerParser.BindataContext):
        self.asm_line_pc(ctx, self.PC)
        address = self.PC
        name = ctx.children[1].getText()
        line_defined_on = ctx.start.line
        new = Symbol(name, address, line_defined_on, symtype=SymbolType.variable)
        self.ENV.add_symbol(new)
        self.current_block_size = 0
        self.current_block_name = name
        self.current_block_line_defined_on = line_defined_on

        # .bin takes a list of lists, so visit each list in turn.
        for c in range(2, len(ctx.children)):
            self.visit(ctx.children[c])

        # # Add a 'sizeof' symbol to the symbol table also.
        # size_symbol = Symbol('sizeof_' + self.current_block_name,
        #                      self.current_block_size,
        #                      self.current_block_line_defined_on,
        #                      symtype=SymbolType.sizeof)
        # self.ENV.add_symbol(size_symbol)

    def visitBindataList(self, ctx: AssemblerParser.BindataListContext):
        self.asm_line_pc(ctx, self.PC)
        for bdlc in ctx.children:
            self.visit(bdlc)

    def visitBindataItemChar(self, ctx: AssemblerParser.BindataItemCharContext):
        part = ctx.children[0].getText()
        part = part.replace("'", "")
        self.ENV.ram[self.PC] = ord(part[0])
        self.current_block_size += 1
        self.PC += 1

    def visitBindataItemNumber(self, ctx: AssemblerParser.BindataItemNumberContext):
        part = ctx.children[0].getText()
        self.ENV.ram[self.PC] = int(part)
        self.current_block_size += 1
        self.PC += 1


    # ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
    # STRING DEFINITIONS METHODS
    # ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■

    def visitDefstring(self, ctx: AssemblerParser.DefstringContext):
        self.current_string_length = 0
        # asm_line = self.get_asm_line(ctx, self.PC)
        asm_line = self.asm_line_pc(ctx, self.PC)
        name = ctx.children[1].getText()
        name = name.replace(":", "")
        value = self.PC
        line_defined_on = asm_line.global_line_number
        new = Symbol(name, value, line_defined_on, symtype=SymbolType.variable)
        self.ENV.add_symbol(new)
        self.visit(ctx.children[2])
        size_symbol = Symbol('sizeof_' + name, self.current_string_length, line_defined_on, symtype=SymbolType.sizeof)
        self.ENV.add_symbol(size_symbol)

    def visitStringpartcollection(self, ctx: AssemblerParser.StringpartcollectionContext):
        for c in ctx.children:
            self.visit(c)

    def visitStringPartChar(self, ctx: AssemblerParser.StringPartCharContext):
        part = ctx.children[0].getText()
        part = part.replace("'", "")
        self.ENV.ram[self.PC] = ord(part[0])
        self.current_string_length += 1
        self.PC += 1

    def visitStringPartExpr(self, ctx: AssemblerParser.StringPartExprContext):
        part = self.visit(ctx.children[0])
        self.ENV.ram[self.PC] = part
        self.current_string_length += 1
        self.PC += 1

    def visitStringPartString(self, ctx: AssemblerParser.StringPartStringContext):
        part = ctx.children[0].getText()
        part = part.replace('"', '')
        for index in range(len(part)):
            self.ENV.ram[self.PC] = ord(part[index])
            self.current_string_length += 1
            self.PC += 1

    # ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
    # INSTRUCTIONS
    # ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■

    def visitI_ld_gpr_expr(self, ctx: AssemblerParser.I_ld_gpr_exprContext):
        self.asm_line_words_pc(ctx, self.PC, 2)
        # Will expression fit in T value?
        expr_value = self.visit(ctx.children[3])
        if expr_value is None:
            al = self.get_asm_line(ctx)
            self.ENV.errors.append(GeneralSyntaxError(al.line_number))
            self.PC += 2
            return
        if Arch.fits_in_t(expr_value):
            self.ENV.advisories.append(AdvisoryUseImmediate(ctx.start.line, expr_value))
        self.PC += 2
        return

    def visitI_ld_gpr_t(self, ctx: AssemblerParser.I_ld_gpr_tContext):
        self.asm_line_words_pc(ctx, self.PC, 1)
        self.PC += 1

    def visitI_ld_gpr_gptr(self, ctx: AssemblerParser.I_ld_gpr_gptrContext):
        self.asm_line_words_pc(ctx, self.PC, 1)
        self.PC += 1

    def visitI_ld_gpr_sptr(self, ctx: AssemblerParser.I_ld_gpr_sptrContext):
        self.asm_line_words_pc(ctx, self.PC, 2)
        self.PC += 2

    def visitI_copy_gpr_gpr(self, ctx: AssemblerParser.I_copy_gpr_gprContext):
        self.asm_line_words_pc(ctx, self.PC, 1)
        self.PC += 1

    def visitI_ld_sp_expr(self, ctx: AssemblerParser.I_ld_sp_exprContext):
        self.asm_line_words_pc(ctx, self.PC, 2)
        self.PC += 2

    def visitI_sv_expr_gpr(self, ctx: AssemblerParser.I_sv_expr_gprContext):
        self.asm_line_words_pc(ctx, self.PC, 1)
        self.PC += 2

    def visitI_sv_gptr_gpr(self, ctx: AssemblerParser.I_sv_gptr_gprContext):
        self.asm_line_words_pc(ctx, self.PC, 1)
        self.PC += 1

    def visitI_sv_sptr_gpr(self, ctx: AssemblerParser.I_sv_sptr_gprContext):
        self.asm_line_words_pc(ctx, self.PC, 2)
        self.PC += 2

    def visitI_sv_gptr_expr(self, ctx: AssemblerParser.I_sv_gptr_exprContext):
        self.asm_line_words_pc(ctx, self.PC, 2)
        self.PC += 2

    def visitI_call(self, ctx: AssemblerParser.I_callContext):
        asm_line = self.asm_line_words_pc(ctx, self.PC, 1)
        symname = ctx.children[1].getText()
        # We reference a symbol that hasn't been defined yet, so we have no choice
        # but to encode it as a two-word call.
        if symname not in self.ENV.symbols:
            self.asm_line_words_pc(ctx, self.PC, 2)
            self.PC += 2
        #    self.ENV.advisories.append(AdvisoryForwardCall(asm_line.line_number))
            return
        # If we get here, we know the symbol exists already , so it IS a backward
        # reference.  Next test is to see if it's a small enough delta
        # to be encoded in S.
        symvalue = self.ENV.symbols[symname].value
        s = symvalue - (self.PC + 1)
        if not Arch.fits_in_s(s):
            self.asm_line_words_pc(ctx, self.PC, 2)
            self.PC += 2
        #    self.ENV.advisories.append(AdvisoryFarCall(asm_line.line_number, s))
            return
        # If we get here, then we know the symbol exists (is a backward
        # reference, and we know the jump is small enough enough to be encoded
        # in S; so code as one-word S-type instruction.
        self.asm_line_words_pc(ctx, self.PC, 1)
        self.PC += 1
        return

    def visitI_add_gpr_t(self, ctx: AssemblerParser.I_add_gpr_tContext):
        self.asm_line_words_pc(ctx, self.PC, 1)
        self.PC += 1

    def visitI_add_gpr_gpr(self, ctx: AssemblerParser.I_add_gpr_gprContext):
        self.asm_line_words_pc(ctx, self.PC, 1)
        self.PC += 1

    def visitI_sub_gpr_t(self, ctx: AssemblerParser.I_sub_gpr_tContext):
        self.asm_line_words_pc(ctx, self.PC, 1)
        self.PC += 1

    def visitI_sub_gpr_gpr(self, ctx: AssemblerParser.I_sub_gpr_gprContext):
        self.asm_line_words_pc(ctx, self.PC, 1)
        self.PC += 1

    def visitI_cmp_gpr_t(self, ctx: AssemblerParser.I_cmp_gpr_tContext):
        self.asm_line_words_pc(ctx, self.PC, 1)
        self.PC += 1

    def visitI_cmp_gpr_gpr(self, ctx: AssemblerParser.I_cmp_gpr_gprContext):
        self.asm_line_words_pc(ctx, self.PC, 1)
        self.PC += 1

    def visitI_mul_gpr_t(self, ctx: AssemblerParser.I_mul_gpr_tContext):
        self.asm_line_words_pc(ctx, self.PC, 1)
        self.PC += 1

    def visitI_mul_gpr_gpr(self, ctx: AssemblerParser.I_mul_gpr_gprContext):
        self.asm_line_words_pc(ctx, self.PC, 1)
        self.PC += 1

    # def visitI_and_gpr_t(self, ctx: AssemblerParser.I_and_gpr_tContext):
    #     self.asm_line_words_pc(ctx, self.PC, 1)
    #     self.PC += 1
    #
    # def visitI_and_gpr_gpr(self, ctx: AssemblerParser.I_and_gpr_gprContext):
    #     self.asm_line_words_pc(ctx, self.PC, 1)
    #     self.PC += 1
    #
    # def visitI_or_gpr_t(self, ctx: AssemblerParser.I_or_gpr_tContext):
    #     self.asm_line_words_pc(ctx, self.PC, 1)
    #     self.PC += 1
    #
    # def visitI_or_gpr_gpr(self, ctx: AssemblerParser.I_or_gpr_gprContext):
    #     self.asm_line_words_pc(ctx, self.PC, 1)
    #     self.PC += 1
    #
    # def visitI_xor_gpr_t(self, ctx: AssemblerParser.I_xor_gpr_tContext):
    #     self.asm_line_words_pc(ctx, self.PC, 1)
    #     self.PC += 1
    #
    # def visitI_xor_gpr_gpr(self, ctx: AssemblerParser.I_xor_gpr_gprContext):
    #     self.asm_line_words_pc(ctx, self.PC, 1)
    #     self.PC += 1

    def visitI_div_gpr_gpr_gpr(self, ctx: AssemblerParser.I_div_gpr_gpr_gprContext):
        self.asm_line_words_pc(ctx, self.PC, 2)
        self.PC += 2

    def visitI_jr(self, ctx: AssemblerParser.I_jrContext):
        self.asm_line_words_pc(ctx, self.PC, 1)
        self.PC += 1

    def visitI_ja(self, ctx: AssemblerParser.I_jaContext):
        self.asm_line_words_pc(ctx, self.PC, 2)
        self.PC += 2

    def visitI_ret(self, ctx: AssemblerParser.I_retContext):
        self.asm_line_words_pc(ctx, self.PC, 1)
        self.PC += 1

    def visitI_push_flags(self, ctx: AssemblerParser.I_push_flagsContext):
        self.asm_line_words_pc(ctx, self.PC, 1)
        self.PC += 1

    def visitI_push_gpr(self, ctx: AssemblerParser.I_push_gprContext):
        self.asm_line_words_pc(ctx, self.PC, 1)
        self.PC += 1

    def visitI_pop_flags(self, ctx: AssemblerParser.I_pop_flagsContext):
        self.asm_line_words_pc(ctx, self.PC, 1)
        self.PC += 1

    def visitI_pop_gpr(self, ctx: AssemblerParser.I_pop_gprContext):
        self.asm_line_words_pc(ctx, self.PC, 1)
        self.PC += 1

    def visitI_tx_gpr(self, ctx: AssemblerParser.I_tx_gprContext):
        self.asm_line_words_pc(ctx, self.PC, 1)
        self.PC += 1

    def visitI_tx_t(self, ctx: AssemblerParser.I_tx_tContext):
        self.asm_line_words_pc(ctx, self.PC, 1)
        self.PC += 1

    def visitI_io_gpr(self, ctx: AssemblerParser.I_io_gprContext):
        self.asm_line_words_pc(ctx, self.PC, 1)
        self.PC += 1

    def visitI_io_t(self, ctx: AssemblerParser.I_io_tContext):
        self.asm_line_words_pc(ctx, self.PC, 1)
        self.PC += 1

    def visitI_io_off(self, ctx: AssemblerParser.I_io_offContext):
        self.asm_line_words_pc(ctx, self.PC, 1)
        self.PC += 1

    def visitI_rx_gpr(self, ctx: AssemblerParser.I_rx_gprContext):
        self.asm_line_words_pc(ctx, self.PC, 1)
        self.PC += 1

    # def visitI_shl(self, ctx: AssemblerParser.I_shlContext):
    #     self.asm_line_words_pc(ctx, self.PC, 1)
    #     self.PC += 1
    #
    # def visitI_shr(self, ctx: AssemblerParser.I_shrContext):
    #     self.asm_line_words_pc(ctx, self.PC, 1)
    #     self.PC += 1

.data $1000 numbufstruct
{
   ; $1000 1001 1002 1003 1004 1005 1006
    buffer  w   7
}

.org 0
    jr main                     ; go to main routine

;----------------------------------------------------------------------
bin2str:
;   input: r01 has value to convert to string
;   alters: r01, r02, r03, r04
;   output: <buffer> points at beginning of string
;----------------------------------------------------------------------
    ; set up buffer by filling with spaces and trailing null
    ld      r00, $20
    sv      (buffer + 0), r00
    sv      (buffer + 1), r00
    sv      (buffer + 2), r00
    sv      (buffer + 3), r00
    sv      (buffer + 4), r00
    sv      (buffer + 5), r00
    ld      r00, 0
    sv      (buffer + 6), r00
    ldw     r04, buffer + 5     ; point to the digits, ones position
    ld      r02, 10             ; r02 holds the divisor
divby10:
    div     r01, r02, r03       ; r03 gets remainder
    add     r03, $30            ; add $30 to remainder to get ASCII value
    sv      [r04], r03          ; save the remainder at current position
    dec     r04                 ; move left one place for next digit
    cmp     r01, 0              ; decide if we're done yet
    jr.nz   divby10             ; not done? repeat for next remaining value
    ret

;----------------------------------------------------------------------
io_number:
;   alters r02, r01
;----------------------------------------------------------------------
    ldw     r02, buffer         ; point at beginning of buffer
next_char:
    ld      r01, [r02]          ; load the value
    cmp     r01, 0              ; if terminating null,
    ret.z                       ; then return, done
    io1.on  r01                 ; io+
    io1.off r01                 ; io-
    inc     r02                 ; move to next position
    jr      next_char           ; repeat

;----------------------------------------------------------------------
main:
;----------------------------------------------------------------------
    ldw r01, 87654
    call bin2str
    call io_number
    jr main


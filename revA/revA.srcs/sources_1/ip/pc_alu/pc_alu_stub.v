// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (win64) Build 2708876 Wed Nov  6 21:40:23 MST 2019
// Date        : Wed Jun 10 16:02:40 2020
// Host        : DESKTOP-THNB40S running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top pc_alu -prefix
//               pc_alu_ pc_jumper_stub.v
// Design      : pc_jumper
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "c_addsub_v12_0_14,Vivado 2019.2" *)
module pc_alu(A, B, CE, S)
/* synthesis syn_black_box black_box_pad_pin="A[17:0],B[10:0],CE,S[17:0]" */;
  input [17:0]A;
  input [10:0]B;
  input CE;
  output [17:0]S;
endmodule

module pc_module(
	input wire clock,
	input wire reset,
	input wire enable,
	input wire step,
	input wire jr,
	input wire [10:0] offset,
	input wire ja,
	input wire [17:0] addr,
	output reg [17:0] pc
);

always @ (posedge clock)
begin
	if (reset)
	begin
		pc <= 0;
	end
	else
	begin
		if (enable)
			begin
			case ({step, jr, ja})
				3'b100 : pc <= pc + 1;
				3'b010 : pc <= pc + {{7{offset[10]}}, offset[10:0] };
				3'b001 : pc <= addr;
				default: ;
			endcase
			end
	end
end
endmodule

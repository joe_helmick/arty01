import antlr4
from AssemblerLexer import *


# noinspection PyPep8Naming,PyShadowingBuiltins
class JMULexer(AssemblerLexer):

    def __init__(self, errors: list, input=None, output: TextIO = sys.stdout):
        super().__init__(input, output)
        self.errors = errors

    def recover(self, re: NoViableAltException):
        message = "LEXICAL ERROR: Line {0} col {1}, symbol = {2}".format(self.line, self.column, self.text)
        self.errors.append(message)
        return





#▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
# BOARD PARAMETERS FOR PROGRAMMING/CONFIGURATION.
#▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
set_property CFGBVS VCCO [current_design]
set_property CONFIG_VOLTAGE 3.3 [current_design]
set_property CONFIG_MODE SPIx4 [current_design]

#▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
# PIN MAPPINGS
#▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
# Specify the hardware clock pin.
set_property -dict {PACKAGE_PIN E3 IOSTANDARD LVCMOS33} [get_ports brd_clk]
# Specify the hardware reset pin.  Signal is ACTIVE-LOW.
set_property -dict {PACKAGE_PIN C2 IOSTANDARD LVCMOS33} [get_ports brd_rst]
# GPIO-related pins.
set_property -dict {PACKAGE_PIN V15 IOSTANDARD LVCMOS33} [get_ports {io_byte[0]}]
set_property -dict {PACKAGE_PIN U16 IOSTANDARD LVCMOS33} [get_ports {io_byte[1]}]
set_property -dict {PACKAGE_PIN P14 IOSTANDARD LVCMOS33} [get_ports {io_byte[2]}]
set_property -dict {PACKAGE_PIN T11 IOSTANDARD LVCMOS33} [get_ports {io_byte[3]}]
set_property -dict {PACKAGE_PIN R12 IOSTANDARD LVCMOS33} [get_ports {io_byte[4]}]
set_property -dict {PACKAGE_PIN T14 IOSTANDARD LVCMOS33} [get_ports {io_byte[5]}]
set_property -dict {PACKAGE_PIN T15 IOSTANDARD LVCMOS33} [get_ports {io_byte[6]}]
set_property -dict {PACKAGE_PIN T16 IOSTANDARD LVCMOS33} [get_ports {io_byte[7]}]
set_property -dict {PACKAGE_PIN N15 IOSTANDARD LVCMOS33} [get_ports io_strobe]
# UART pins.
set_property -dict {PACKAGE_PIN M17 IOSTANDARD LVCMOS33} [get_ports tx]
set_property -dict {PACKAGE_PIN H5  IOSTANDARD LVCMOS33} [get_ports {txled}]; #IO_L24N_T3_35 Sch=led[4]


#▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
# TIMING CONSTRAINTS
#▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪

# Reset pin input delays.
set_input_delay -clock brd_clk -min -add_delay 0.000 [get_ports brd_rst]
set_input_delay -clock brd_clk -max -add_delay -10.000 [get_ports brd_rst]

# IO pins output delays.
set_output_delay -clock brd_clk -min -add_delay 0.000 [get_ports {io_byte[*]}]
set_output_delay -clock brd_clk -max -add_delay -30.000 [get_ports {io_byte[*]}]

# IO strobe pin output delays.
set_output_delay -clock brd_clk -min -add_delay 0.000 [get_ports io_strobe]
set_output_delay -clock brd_clk -max -add_delay -30.000 [get_ports io_strobe]

# TX pin output delays
set_output_delay -clock brd_clk -min -add_delay 0.000 [get_ports tx]
set_output_delay -clock brd_clk -max -add_delay -30.000 [get_ports tx]
set_property SLEW SLOW [get_ports tx]

# TX LED indicator output delays
set_output_delay -clock brd_clk -min -add_delay 0.0 [get_ports {txled}];
set_output_delay -clock brd_clk -max -add_delay -30.0 [get_ports {txled}];


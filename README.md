Demonstrable version of custom-build FPGA-based RISC microprocessor.

This project has several goals:
    1. Create a soft-core microprocessor using the Xilinx Artix-7 part XC7A100TCSG324-1.
    2. Create a RISC instruction set for the microprocessor.
    3. Create an assembler for the instruction set.
    4. Implement the instruction set on the microprocessor.
    5. Demonstrate the processor using small test programs.
    6. Demonstrate the processor using a version of Conway's Game of Life.

All goals were achieved.

The microprocessor was coded in Verilog and SystemVerilog using Xilinx Vivado 2020.1.

The RISC instruction set is described in the .g4 file (the lexer and parser rules for Antlr4).

The assembler is written in Python.  It is is two-pass assembler that implements the grammar defined in the Antlr4 files.

The GUI-based simulator/debugger is written in Python using tkinter.


Key Files:
	The implementation logfile is at:
		D:\dev-attic\microprocessors\Arty01\revA\revA.runs\impl_3\runme.log

To program the device, the "Generate Bitstream" file is located at:
		D:\dev-attic\microprocessors\Arty01\revA\revA.runs\impl_3\system.bit (CORRUPT TODAY 2020-10-24)
		
		H:\dev-attic\microprocessors\Arty01\revA\revA.runs\impl_3\system.bit is last working one from backup.
	
	
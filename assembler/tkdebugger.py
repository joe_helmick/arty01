from tkinter import *
from tkSourceLine import SourceLine
from tkDebuggerMainWindow import ui_main_window
from tkKonstants import tkkuistandards as tkk
from JMUArchitecture import *


BASE_PATH = 'D:\\dev-attic\\microprocessors\\Arty01\\assembler\\'


def load_memory_image():
    MEMSIZE = Arch.MEMORY_DEPTH
    memory = [0] * MEMSIZE

    # Read the RAM dump file and fill in as necessary.
    ramfile = open(BASE_PATH + r'\outputs\ram_dump.txt', 'r')
    # Read and discard two header lines
    ramfile.readline()
    ramfile.readline()
    # Read rest of lines.
    while True:
        line = ramfile.readline()
        # Bail if done.
        if not line:
            break
        # Skip this one if listed as empty, default value left in place.
        if 'empty' in line:
            continue
        # Otherwise parse the address in hex, the value in hex, and fill in RAM.
        addr = int(line[1:6], 16)
        value = int(line[9:14], 16)

        if addr <= len(memory) - 1:
            memory[addr] = value
    ramfile.close()
    return memory


def load_listing():
    # Read entire listing into array of lines.
    f = open(BASE_PATH + '__main.lst', 'r', encoding='utf-8')
    alllines = f.readlines()
    f.close()
    # Create dummy line zero which will never get used.
    # This list will get filled.
    lines = [SourceLine(0, '')]
    # Now add real lines, one-based.
    lineNo = 1
    for _ in alllines:
        line = SourceLine(lineNo, alllines[lineNo - 1].rstrip())
        lines.append(line)
        lineNo += 1
    return lines


def main():
    # Load the __main.lst listing file.
    listinglines = load_listing()

    # Load the RAM image file
    memory = load_memory_image()

    # Create a main window.
    root = Tk()
    root.configure(bg=tkk.COLOR_MAIN_WINDOW)
    root.resizable(width=False, height=False)
    root.title("JMU Assembler Simulator / Debugger")
    root.geometry('{0}x{1}+{2}+{3}'.format(tkk.WID_MAIN_WINDOW, tkk.HEI_MAIN_WINDOW, 0, 0))

    # Create the main UI window and start the event loop.
    ui_main_window(root, listinglines, memory)

    # Start the TCL main UI loop.
    root.mainloop()


if __name__ == '__main__':
    main()


.org 0

start:
    ld      r00, 1      ; 1
    add     r00, 2      ; 3
    sub     r00, 1      ; 2
    inc     r00         ; 3
    dec     r00         ; 2
    copy    r01, r00    ; r01 <= 2

    jr      start

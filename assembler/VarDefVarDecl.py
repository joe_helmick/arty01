class VarDecl:
    name = None
    unit = None
    length = None
    address = None

    def __init__(self, name, unit, length, address):
        self.name = name
        self.unit = unit
        self.length = length
        self.address = address


class VarDef:
    name = None
    address = None
    decls = []

    def __init__(self):
        self.name = None
        self.address = None

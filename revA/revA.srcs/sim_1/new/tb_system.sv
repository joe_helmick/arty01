`timescale 1ns / 1ps


module tb_system();
	// Inputs to system.
	reg brd_rst;
	reg brd_clk;
	// Real outputs from system.
	wire [7:0] io_byte;
	wire io_strobe;
	wire tx;
	wire txled;

	// Test-only outputs from system.
	wire t_clk_sys;
	wire t_sys_rst;
	wire [6:0] t_state;
	wire [17:0] t_iw1;
	wire [17:0] t_iw2;
	wire [2:0] t_flags;
	wire [6:0] t_opcode;
	wire [4:0] t_a;
	wire [4:0] t_b;
	wire [4:0] t_c;
	wire [17:0] t_dina;
	wire [17:0] t_dinc;
	wire [17:0] t_douta;
	wire [17:0] t_doutb;
	wire [17:0] t_doutc;
	wire t_wea;
	wire t_wec;
	wire signed [10:0] t_s;
	wire [7:0] t_t;
	wire t_opt5;
	wire [1:0] t_flagid;
	wire t_flagsc;
	wire [17:0] t_pc;
	wire signed [10:0] t_pc_delta;
	wire t_pc_alu_en;
	wire t_alu_en;
	wire [17:0] t_alu_a;
	wire [17:0] t_alu_b;
	wire [17:0] t_alu_result;
	wire [17:0] t_alu_md_result;
	wire [17:0] t_alu_md_remainder;
	wire t_muldiv_start;
	wire t_muldiv_done;
	wire [17:0] t_sp;
	wire t_sp_alu_en;
	wire t_sp_add;
	wire t_fault;
	wire [17:0] t_ram_data_addr;
	wire [17:0] t_ram_data_dout;
	wire [17:0] t_ram_code_dout;
	wire t_txbusy;
	
	// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
	//  CLOCK SIMULATOR 
	// ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪
	initial begin
		$timeformat(-9, 2, " ns", 10);
		// Simulation of real-life inbound signals
		brd_rst = 0; // physical board reset signal is active low so start as 0
		brd_clk = 0;
		// Start the simulated board clock at 100 Mhz
		 	forever
		 		# 5.000 brd_clk = ~brd_clk; // 100 MHz
	end

	// Emerge from reset.
	initial begin
		# 100 brd_rst = 1;
	end

	// Instantiate the system as the device under test.
	system SOC (
		// Inputs.
		.brd_rst(brd_rst),
		.brd_clk(brd_clk),
		// Real outputs.
		.io_byte(io_byte),
		.io_strobe(io_strobe),
		.tx(tx),
		.txled(txled),
		// Test-only outputs.
		.t_clk_sys(t_clk_sys),
		.t_sys_rst(t_sys_rst),
		.t_state(t_state),
		.t_iw1(t_iw1),
		.t_iw2(t_iw2),
		.t_flags(t_flags),
		.t_opcode(t_opcode),
		.t_a(t_a),
		.t_b(t_b),
		.t_c(t_c),
		.t_dina(t_dina),
		.t_dinc(t_dinc),
		.t_douta(t_douta),
		.t_doutb(t_doutb),
		.t_doutc(t_doutc),
		.t_wea(t_wea),
		.t_wec(t_wec),
		.t_s(t_s),
		.t_t(t_t),
		.t_opt5(t_opt5),
		.t_flagid(t_flagid),
		.t_flagsc(t_flagsc),
		.t_pc(t_pc),
		.t_pc_delta(t_pc_delta),
		.t_pc_alu_en(t_pc_alu_en),
		.t_alu_en(t_alu_en),
		.t_alu_a(t_alu_a),
		.t_alu_b(t_alu_b),
		.t_alu_result(t_alu_result),
		.t_alu_md_result(t_alu_md_result),
		.t_alu_md_remainder(t_alu_md_remainder),
		.t_muldiv_start(t_muldiv_start),
		.t_muldiv_done(t_muldiv_done),
		.t_sp(t_sp),
		.t_sp_alu_en(t_sp_alu_en),
		.t_sp_add(t_sp_add),
		.t_fault(t_fault),
		.t_ram_data_addr(t_ram_data_addr),
		.t_ram_data_dout(t_ram_data_dout),
		.t_ram_code_dout(t_ram_code_dout),
		.t_txbusy(t_txbusy)
	);
endmodule


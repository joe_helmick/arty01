from JMUArchitecture import Arch
from enum import Enum
from bitstring import BitArray


class InstructionMode(Enum):
    ModeUnknown = -1
    ModeRegular = 1
    ModeAAT = 2


class InstructionDecode:
    a, b, c, s, t = None, None, None, None, None
    flagid, flagsc = None, None
    opt5, opt10 = None, None
    mode = None
    opcode = None
    iw1, iw2 = None, None

    def __init__(self,
                 iw1,
                 iw2=None,
                 a=None,
                 b=None,
                 c=None,
                 s=None,
                 t=None,
                 flagid=None,
                 flagsc=None,
                 opt5=None,
                 opt10=None,
                 mode=None,
                 opcode=None):
        self.iw1 = iw1
        self.iw2 = iw2
        self.a = a
        self.b = b
        self.c = c
        self.s = s
        self.t = t
        self.flagid = flagid
        self.flagsc = flagsc
        self.opt5 = opt5
        self.opt10 = opt10
        self.mode = mode
        self.opcode = opcode
        return


class Decoder:
    iw1 = None
    iw2 = None
    mode = InstructionMode.ModeUnknown
    a = None
    b = None
    c = None
    flagid = None
    flagsc = None
    s = None
    t = None
    opt5 = None
    opt10 = None
    opcode = None

    # @formatter:off
    mask_a          = 0b000000000000011111
    mask_b          = 0b000000001111100000
    mask_c          = 0b000000000000011111
    mask_s          = 0b000000011111111111
    mask_t          = 0b000001111111100000
    mask_at_msb     = 0b100000000000000000
    mask_at_opcode  = 0b011110000000000000
    mask_opcode     = 0b011111100000000000
    mask_flagid     = 0b000000000000001110
    mask_flagsc     = 0b000000000000000001
    mask_opt5       = 0b100000000000100000
    mask_opt10      = 0b000000010000000000

    # # Special bitarray for AT instructions.
    # ba_at = None

    def __init__(self):
        self.iw1 = None
        # self.ba_at = BitArray(bin='100')

    def decodeIw1(self, iw1=None):
        self.iw1 = iw1
        if iw1 is None:
            return

        # Decode the registers.
        self.a = (self.iw1 & self.mask_a)
        self.b = (self.iw1 & self.mask_b) >> 5

        # Decode s and subtract if negative.
        self.s = (self.iw1 & self.mask_s)
        if self.s > Arch.MAX_VALUE_S:
            self.s = self.s - Arch.S_TWOS_SUBTRACTION

        # Decode t.
        self.t = (self.iw1 & self.mask_t) >> 5

        # Decode the flags stuff.
        self.flagid = (self.iw1 & self.mask_flagid) >> 1
        self.flagsc = (self.iw1 & self.mask_flagsc)

        # Decode the two options.
        self.opt5 = (self.iw1 & self.mask_opt5) >> 5
        self.opt10 = (self.iw1 & self.mask_opt10) >> 10

        # Determine if it's an A/AT instruction or regular instruction,
        # then create, decode and shift the opcode.
        if (self.iw1 & self.mask_at_msb) == self.mask_at_msb:
            self.mode = InstructionMode.ModeAAT
            self.opcode = (self.iw1 & self.mask_at_opcode) >> 13
            bx = BitArray(bin='100')
            # bx = self.ba_at.copy()
            bx.append(BitArray(uint=self.opcode, length=4))
            self.opcode = int(bx.bin, 2)
        else:
            self.mode = InstructionMode.ModeRegular
            self.opcode = (self.iw1 & self.mask_opcode) >> 11

    def decodeIw2(self, iw2):
        self.iw2 = iw2
        if self.iw2 is not None:
            self.c = (self.iw2 & self.mask_c)

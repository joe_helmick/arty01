// arch_defines.vh
`ifndef _arch_defines_vh
`define _arch_defines_vh

//`define SIMULATION	// comment out to do synthesis/implementation

// ALU selector values.
`define ALU_SELECTOR_LOGICAL			2'd0
`define ALU_SELECTOR_ADDSUB         2'd1
`define ALU_SELECTOR_MULDIV         2'd2

// Early, common FSM states. 
`define FSM_0								7'd0
`define FSM_LD_IW1_INIT					7'd1
`define FSM_LD_IW1_WAIT					7'd2
`define FSM_DECODE						7'd3

// ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//  OPCODES
// ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
	`define OPCODE_LD_GPR_EXPR      	7'd0
	`define OPCODE_LD_GPR_GPTR			7'd1
	//`define OPCODE_LD_GPR_SPTR			7'd2
	`define OPCODE_COPY_AB				7'd3
	`define OPCODE_LD_SP					7'd4
	`define OPCODE_SV_EXPR_GPR			7'd5
	`define OPCODE_SV_GPTR_GPR			7'd6
	//`define OPCODE_SV_SPTR_GPR			7'd7
	//`define OPCODE_SV_GPTR_EXPR		7'd8
	`define OPCODE_CALL_FAR				7'd9
	`define OPCODE_CALL_NEAR			7'd10
	`define OPCODE_ADD_AB           	7'd11
	`define OPCODE_SUB_AB           	7'd12
	`define OPCODE_CMP_AB           	7'd13
	`define OPCODE_MUL_AB           	7'd14
	`define OPCODE_AND_AB           	7'd15
	`define OPCODE_OR_AB            	7'd16
	`define OPCODE_XOR_AB           	7'd17
	`define OPCODE_DIV_ABC          	7'd18

	`define OPCODE_JR						7'd22
	`define OPCODE_JRZ					7'd23
	`define OPCODE_JRNZ					7'd24
	`define OPCODE_JRS					7'd25
	`define OPCODE_JRNS					7'd26
	`define OPCODE_JA						7'd27
	`define OPCODE_RET              	7'd28
	`define OPCODE_PUSH					7'd29
	`define OPCODE_POP              	7'd30
	`define OPCODE_IO_GPR				7'd31
	`define OPCODE_TX_GPR				7'd32
	`define OPCODE_RX_GPR				7'd33

	`define OPCODE_LD_AT					7'd64
	`define OPCODE_ADD_AT				7'd65
	`define OPCODE_SUB_AT				7'd66
	`define OPCODE_CMP_AT           	7'd67
	`define OPCODE_MUL_AT           	7'd68
	`define OPCODE_AND_AT           	7'd69
	`define OPCODE_OR_AT            	7'd70
	`define OPCODE_XOR_AT           	7'd71
	`define OPCODE_TX_T					7'd72
	`define OPCODE_IO_T					7'd73

	// Flag definitions.
	`define FLAG_ID_NOFLAG          	2'd0
	`define FLAG_ID_Z           		2'd1
	`define FLAG_ID_S 	     			2'd2

	// Mnemonics for basic settings.
	`define SET     						1'd1
	`define CLR			    	    		1'd0
	`define YES 							1'b1
	`define NO								1'b0

// ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
// ALU OPERATIONS
// ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
	`define OP_ADD 						4'd1
	`define OP_SUB 			 			4'd2
	`define OP_CMP 			 			4'd3
	`define OP_AND 			 			4'd4
	`define OP_OR 			 				4'd5
	`define OP_XOR 						4'd6

	`define USE_ALU_GENERAL				1'b1
	`define USE_ALU_MULTDIV				1'b0

// // ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
// // Other operations
// // ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
// 	//`define OP_RETI 			6'd21 
// 	//`define OP_EI 			6'd22
// 	//`define OP_DI 			6'd23
// 	//`define OP_NOP	 		6'd24
// 	//`define OP_CLR_FLAG		6'd43
// 	//`define OP_NONE  			6'd0
// 	//`define OP_HALT 			6'd63

`endif
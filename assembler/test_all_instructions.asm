.lstdecoding
.lstprogress
.lstsymtable

.equ    CARRIAGE_RETURN         $d
.equ    LINE_FEED               $a

.org $1000
.data next structure1
{
	buffer  w $100 fill $20
	buf2    w $20 fill $00
}
.string msg1 "Joe rulez!",0

.org 0
.alias r31      @reg31
.alias r30      @reg30

near_call:
	ret

start:
	;ld      r01, 400000                    ; PASS tries to use OPC_LD_A_T but immediate value too large
	;ldw     r01, 400000                    ; PASS tries to use OPC_LD_GPR_EXPR but number too large, gives error
	;ldw     r01, (600000/2)                ; PASS gives correct error message
	;ldw     r01, @reg31                    ; PASS cannot have an alias here
	;sv      500000, r01                    ; PASS gives proper error messsage
	;sv      -1, r01                        ; PASS gives proper error message, can't handle negative value
	;add     r01, 316                       ; PASS gives proper error message

	ldw     r01, 4096                       ; PASS
	ld      r01, 1+1                        ; PASS

	ld      r02, [r03]                      ; PASS
	ld      r02, [msg1]

	sv      buffer, r01                     ; PASS
	sv      [r03], r01
	sv      [msg1], r01
	sv      [r04], $f0 + $03

	sv      (500000/2), r01                 ; PASS expression within bounds *after* evaluation
	sv      [r31], buffer                   ; PASS

	jr      start

	copy    r22, r21                        ; PASS

	ld      SP, structure1                  ; PASS
	ld      SP, $3000                       ; PASS

	call    far_call                        ; PASS uses far call because forward call
	call    near_call                       ; PASS uses near call properly

	add     r01, 16                         ; PASS
	add     r03, r02                        ; PASS
	sub     r01, 16                         ; PASS
	sub     r03, r02                        ; PASS
	cmp     r01, 16                         ; PASS
	cmp     r03, r02                        ; PASS
	MUL     r01, 4                          ; PASS
	MUL     r03, r02                        ; PASS
;	and     r01, 16                         ; PASS
;	and     r03, r02                        ; PASS
;	or      r01, 16                         ; PASS
;	or      r03, r02                        ; PASS
;	xor     r01, 16                         ; PASS
;	xor     r03, r02                        ; PASS
	div     r03, r02, r01                   ; PASS

;	incinc  r01, r02                        ; PASS
;	incdec  r03, r04                        ; PASS
;	decdec  r03, r05                        ; PASS

	jr      near_call                       ; PASS
	jr.z    near_call                       ; PASS
	jr.nz   near_call                       ; PASS
	jr.s    near_call                       ; PASS
	jr.ns   near_call                       ; PASS

	ja      start                           ; PASS
	ja.z    start                           ; PASS
	ja.nz   start                           ; PASS
	ja.s    start                           ; PASS
	ja.ns   start                           ; PASS

	push    flags                           ; PASS
	push    r04                             ; PASS
	pop     r04                             ; PASS
	pop     flags                           ; PASS

	tx       r20                            ; PASS
	tx       'X'                            ; PASS
	tx       @reg31                         ; PASS
	tx       CARRIAGE_RETURN                ; PASS
	tx       27                             ; PASS

	io       r20                            ; PASS
	io       'X'                            ; PASS
	io       @reg31                         ; PASS
	io       CARRIAGE_RETURN                ; PASS
	io       27                             ; PASS

	io-

	ldw     @reg31, $3000                   ; PASS
	ld      @reg31, $ff                     ; PASS
	ld      @reg31, [r20]                   ; PASS
	ld      @reg31, [buffer]
	ldw     @reg31, buf2                    ; PASS
	copy    @reg31, @reg30                  ; PASS

	sv      [@reg31], r20
	sv      [r22], r20
	sv      [buf2], r20
	sv      [buf2], @reg31

far_call:
	ret
	ret.z
	ret.nz
	ret.s
	ret.ns
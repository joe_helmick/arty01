from JMUEnvironment import JMUEnvironment
from JMUAsmProject import AsmProject
from JMUCustomErrors import *
from AssemblerVisitor import AssemblerVisitor
from AssemblerParser import *
from JMUSymbol import *
from JMUArchitecture import Arch


# noinspection PyPep8Naming,PyTypeChecker
class JMUAsmVisitorBase(AssemblerVisitor):
    PC = 0
    current_var_def = None
    var_defs_list = []
    current_string_length = None
    current_block_size = None
    current_block_name = None
    current_block_line_defined_on = None

    def __init__(self, ENV: JMUEnvironment, PRJ: AsmProject):
        self.ENV = ENV
        self.PRJ = PRJ

    def visitProgram(self, ctx: AssemblerParser.ProgramContext):
        for c in ctx.children:
            self.visit(c)
        return

    def asm_line_words_pc(self, context, pc: int, words: int):
        cur_asm_file = self.PRJ.main_file
        asm_line = cur_asm_file.asm_lines[context.start.line - 1]
        asm_line.program_counter = pc
        asm_line.words = [None] * words
        return asm_line

    def get_asm_line(self, context):
        cur_asm_file = self.PRJ.main_file
        asm_line = cur_asm_file.asm_lines[context.start.line - 1]
        return asm_line

    def asm_line_pc(self, context, pc: int):
        cur_asm_file = self.PRJ.main_file
        asm_line = cur_asm_file.asm_lines[context.start.line - 1]
        asm_line.program_counter = pc
        return asm_line

    def visitOrg(self, ctx: AssemblerParser.OrgContext):
        value = self.visit(ctx.children[1])
        self.PC = value
        self.asm_line_pc(ctx, self.PC)
        # self.get_asm_line(ctx, self.PC)

    def visitPowerExpr(self, ctx: AssemblerParser.PowerExprContext):
        v1 = self.visit(ctx.children[0])
        v2 = self.visit(ctx.children[2])
        return v1 ** v2

    def visitMultDivExpr(self, ctx: AssemblerParser.MultDivExprContext):
        v1 = self.visit(ctx.children[0])
        v2 = self.visit(ctx.children[2])
        if ctx.children[1].getText() == '*':
            return int(v1 * v2)
        else:
            return int(v1 / v2)

    def visitPlusMinusExpr(self, ctx: AssemblerParser.PlusMinusExprContext):
        v1 = self.visit(ctx.children[0])
        v2 = self.visit(ctx.children[2])
        if ctx.children[1].getText() == '+':
            return v1 + v2
        else:
            return v1 - v2

    def visitParenExpr(self, ctx: AssemblerParser.ParenExprContext):
        return self.visit(ctx.children[1])

    def visitSymbolExpr(self, ctx: AssemblerParser.SymbolExprContext):
        name = ctx.children[0].getText()
        line_found_on = ctx.start.line
        # Look the name up in the symbol dictionary
        if name in self.ENV.symbols:
            sym = self.ENV.symbols.get(name)
            value = sym.value
            return value
        raise SymbolNotDefinedError(line_found_on, name)

    def visitNumber(self, ctx: AssemblerParser.NumberContext):
        if ctx.BINNUM():
            s1 = str(ctx.BINNUM())
            s2 = s1.replace('#', '')
            v1 = int(s2, 2)
            v = int(str(v1))
            return v
        if ctx.DECNUM():
            v = int(str(ctx.DECNUM()))
            return v
        if ctx.HEXNUM():
            s1 = str(ctx.HEXNUM())
            s2 = s1.replace('$', '')
            v1 = int(s2, 16)
            v = int(str(v1))
            return v
        self.ENV.errors.append(NotImplementedError)

    def visitNumberExpr(self, ctx: AssemblerParser.NumberExprContext):
        x = self.visit(ctx.children[0])
        return x
